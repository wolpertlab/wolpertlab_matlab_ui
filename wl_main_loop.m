function S=wl_main_loop(obj, length_of_test, predict)
%WL_MAIN_LOOP runs for the duration of the experiment processing the states
% implicitly and updating the screen, audio and other IO based on the
% state.
% Returns timing statistics for the graphics rendering cycle.
%
%   S=WL_MAIN_LOOP(OBJ) takes in a pointer to the experiment object/class
%   (note pointer to the experiment object class refferes to an
%   ExperimentBase instance).
%
%   Note this function loops for the duration of the experiment
%   calling the core functions required to display and advance the state
%   machine in the experiments (i.w. WL.display_func, WL.idle, ...).
%
%   Calling WL.main_loop automatically hanndles passing of the OBJ parameter
%   WL.main_loop == wl_main_loop(WL).
%
%   S=WL_MAIN_LOOP(OBJ, l) takes in an additional argument l which
%   terminates the main loop after l seconds have passed.
    
    % Set the mouse to the middle of the screen (if mac)
    if ismac
        SetMouse(obj.Screen.cent(1), obj.Screen.cent(2), obj.Screen.window);
    end
    
    if nargin < 2
        length_of_test = inf; % no testing, early ending
    end
    
    if nargin < 3
        predict = true;
    end
    
    if ~isfield(obj.GW, 'sigma')
        obj.GW.sigma = 2;
    end
        
    topPriorityLevel = MaxPriority(obj.Screen.window);
    Priority(topPriorityLevel);
    
    obj.GW.TrialCount  = rows(obj.TrialData);
    obj.TrialNumber = 1;
    wl_trial_setup(obj);
    
    [id, names, ~] = GetKeyboardIndices();

    Nb = 20000; % buffer length for pscyhtoolbox time stats
    delta_flip_vbl = zeros(1, Nb);
    delta_flip_getsecs = zeros(1, Nb);
    vbl_diff = zeros(1, Nb);
    flip_diff = zeros(1, Nb);
    vbl_flip_diff = zeros(1, Nb);
    flip_vbl_diff = zeros(1, Nb);
    desired_flip_2_display_func_buffer = zeros(1, Nb);
    desired_buffer_count = 0;
    
    delta_count = 1;
    circ = @(count) mod(count - 1, Nb) + 1;

    if exist('kbd_default.mat','file')
        load kbd_default;
        fprintf(1,'Loaded keyboard id =%i from file \n', key_id); %#ok<NODEF>
    else
        key_id = id(1);
 
        for k=1:length(names)
            if strfind(names{k},'Apple Keyboard') == 1
                key_id = id(k);
            end
        end
    end
    KbQueueCreate(key_id);
    KbQueueStart(key_id);
    
    obj.Screen.flipped = 0;
     
    desired_flip_request_2_flip = 0.0025;
    desired_idle_func_freq = 1000; 
    disp_pred = 0.004 ; % Initial value prior to adapting
    
    Exit = 0;
    vbl = Screen('Flip',  obj.Screen.window);
    start = GetSecs();
    while ~Exit
        obj.Timer.System.idle_loop.Loop;
        
        if wl_everyhz(desired_idle_func_freq)
            obj.Timer.System.idle_func_2_idle_func.Loop;
            
            obj.Timer.System.idle_func.Tic;
            obj.idle_func();
            obj.Timer.System.idle_func.Toc;
            
            obj.State.From = 0;
            obj.State.To = 0;
            
            [pressed, firstPress, ~, ~, ~] = KbQueueCheck(key_id);
            [~, ~, MouseButton] = GetMouse(obj.Screen.window);
            
            if  obj.GW.ExitFlag || (pressed && firstPress(KbName('ESCAPE'))) ...
               || MouseButton(3) || (GetSecs() - start > length_of_test)
                Exit=1;
            end
            
        end
        
        desired_buffer_count = desired_buffer_count + 1;
        if ~predict
            disp_pred = 0; % set disp_pred to 0 if prediction dissabelled
        end
        desired_flip_2_display_func = (obj.Screen.ifi - desired_flip_request_2_flip) - disp_pred;
        % sprintf('desired = %f = %f - (0.0025 + %f)', desired_flip_2_display_func, obj.Screen.ifi, disp_pred);
        % sprintf('disp_pred = %f', disp_pred);
        desired_flip_2_display_func_buffer(circ(desired_buffer_count)) = desired_flip_2_display_func;
                
        if obj.Screen.flipped == 1 && (GetSecs() - vbl > desired_flip_2_display_func)
            obj.Timer.System.flip_2_display_func.Toc;
            if wl_is_error(obj)
                Screen('BeginOpenGL', obj.Screen.window);
                glClear;
                glClearColor(1,0,0,0); % Red screen.
                Screen('EndOpenGL', obj.Screen.window);
                wl_draw_text(obj, obj.GW.error_msg, 'center', 'center', 'fontsize', 40, 'fliph', 1);
                
            elseif obj.State.Current == obj.State.REST
                % wl_draw_teapot(obj);
            else
                obj.Timer.System.display_func.Tic();
                obj.display_func()
                obj.Timer.System.display_func.Toc();

                disp_res = obj.Timer.System.display_func.GetSummary().toc.dt;
                if length(disp_res) > 10
                    disp_pred = mean(disp_res);
                    disp_pred = disp_pred +  obj.GW.sigma * std(disp_res);
                end
            end
            
            obj.Timer.System.flip_request.Tic();
            Screen('AsyncFlipBegin', obj.Screen.window);
            obj.Timer.System.flip_request.Toc();
            
            obj.Timer.System.flip_request_2_flip.Tic();
            obj.Screen.flipped = 0;
            for timer = wl_flip_timer.get_instances()
                timer.Trigger;
            end
            
        end
            
        
        %test whether screen has flipped
        if  obj.Screen.flipped == 0

            obj.Timer.System.flip_check.Tic();
            [vbl, ~, FlipTimestamp] = Screen('AsyncFlipCheckEnd', obj.Screen.window);
            obj.Timer.System.flip_check.Toc();

            if vbl > 0
                delta_flip_vbl(circ(delta_count)) = vbl;
                delta_flip_getsecs(circ(delta_count)) = FlipTimestamp;
                
                % collecting diff timestamps
                if delta_count > 1
                    vbl_diff(circ(delta_count)) = delta_flip_vbl(circ(delta_count)) - delta_flip_vbl(circ(delta_count-1));
                    flip_diff(circ(delta_count)) = delta_flip_getsecs(circ(delta_count)) - delta_flip_getsecs(circ(delta_count-1));
                    vbl_flip_diff(circ(delta_count)) = delta_flip_getsecs(circ(delta_count)) - delta_flip_vbl(circ(delta_count-1));
                    flip_vbl_diff(circ(delta_count)) = delta_flip_vbl(circ(delta_count)) - delta_flip_getsecs(circ(delta_count-1));
                end
                
                delta_count = delta_count + 1;

                obj.Screen.flipped = 1;

                for timer = wl_flip_timer.get_instances()
                    timer.Loop();
                end

                obj.Timer.System.flip_request_2_flip.Toc();
                obj.Timer.System.flip_2_display_func.Tic();

                obj.Timer.System.flip_2_flip.Loop();
            end
        end
    end
    
    ShowCursor();
    
    wl_timer_results(obj);
    KbQueueStop(key_id);
    sca;
    
    obj.trial_close(); % saves code and trialdata to mat file
    
    if isfield(obj.cfg, 'Debug') && obj.cfg.Debug
        diary off
    end
    
    % shorten array if samples less than buffer else make sure
    % samples are in temporal order y pivoting the buffer around
    % delta_count
    if delta_count <= Nb
        Nb = delta_count;
        inds = 1:(Nb - 1);
    else
        inds = [1:circ(delta_count) circ(delta_count + 1):(Nb - 1)];
    end
    
    % Time statistics in mileseconds
    S.delta_flip_FlipTime = 1000 * delta_flip_getsecs(inds);
    S.delta_flip_vbl = 1000 * delta_flip_vbl(inds);    
    S.delta_flip = 1000 * (delta_flip_getsecs(inds) - delta_flip_vbl(inds));
    S.vbl_diff = 1000 * vbl_diff(inds);
    S.vbl_flip_diff = 1000 * vbl_flip_diff(inds);
    S.flip_diff = 1000 * flip_diff(inds);
    S.flip_vbl_diff = 1000 * flip_vbl_diff(inds);
    S.disp = obj.Timer.System.display_func.GetSummary();
    S.req2flip = obj.Timer.System.flip_request_2_flip.GetSummary();
    S.fip2disp = obj.Timer.System.flip_2_display_func.GetSummary();
    S.desired_flip_2_display_func = 1000 * desired_flip_2_display_func_buffer;
    S.Nb = Nb - 1;
    obj.GW.S = S;
    
end


