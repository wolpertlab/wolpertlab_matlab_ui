classdef wl_mouse<handle
    %WL_MOUSE is a class intended to use for debugging purposes instead of
    % WL_ROBOT. It mimics/simmulates the WL_ROBOT functionality allowing to
    % debug experimental paradigms from a mac or pc without needing connection
    % to a rig.
    %
    % Most methods in this class are void and do not do anything they just
    % exist so that any code that runs with wl_robot will run with wl_mouse
    
    properties
        FrameVariableList = { 'TimeStamp','State','StateTime','Position','Velocity','Speed' };
        FrameVariableSize = [ 1,1,1,3,3,1];
        FrameVariableCount = 0;
        FrameData
        
        Data
        StationaryTimer
        DataCount = 0;
        TimeStamp
        State
        StartTime
        StateTime
        LastTime = 0;
        Collecting = false;
        CircBuffer
        RobotCount
        FieldType = [ 0 0 ];
        Simulate=true;
        Position;
        Velocity = [ 0 0 0 ];
        buttons = [ 0 0 0 ];
        oldbuttons;
        Speed = 0;
        bcount = 0;
        k1
        k2
        Active = [ 1 1 ];
        bsize = 50;
        N = 3000;
        ActualType = 'mouse';
        obj;
    end
    
    methods

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function this = wl_mouse(obj, Robot1,Robot2)
            %WL_MOUSE constructor takes in a pointer to the ExperimentBase
            % object (WL) and has optional placeolder arguments Robot1 and
            % Robot2 (in order to match WL_ROBOT) which are not used.
            
            % Data collection setup.
            this.StationaryTimer = wl_timer();
            this.FrameVariableCount = length(this.FrameVariableList);
            this.Data = zeros(this.N,sum(this.FrameVariableSize));
            this.CircBuffer = zeros(this.bsize,4); %for pos and time
            this.StartTime = GetSecs;
            this.k1 = cumsum([0 this.FrameVariableSize(1:end-1)])+1;
            this.k2 = cumsum(this.FrameVariableSize);
            this.RobotCount = nargin - 1;
            % Class composition with Experiment Base to handle passing refferences
            % TODO: renaame obj to WL (possibly rename all obj instances to
            % WL).
            this.obj = obj;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = GetLatest(this)
            % Updates all the data attributes of WL_MOUSE with the lastest data
            % read from the mouse. (i.e. this.Speed , this.Position,
            % this.Velocity, this.State ...)

            this.oldbuttons = this.buttons;
            [pos(1,1), pos(2,1), this.buttons] = GetMouse(this.obj.Screen.window);
            
            pos = wl_pix2cm(this.obj, pos);
            pos(3,1) = 0;
            
            this.TimeStamp = GetSecs-this.StartTime;
            this.State = this.obj.State.Current;
            this.StateTime = this.obj.State.Timer.GetTime;
            this.Position = [pos];
            
            this.Active = ones(1, this.RobotCount);
            
            if this.TimeStamp - this.LastTime > 0.001
                this.LastTime = this.TimeStamp;
                this.bcount = this.bcount + 1;
                this.CircBuffer(circ_buffer(this.bcount, this.bsize),:) = [this.Position' this.TimeStamp];
                tmp = this.CircBuffer(circ_buffer(this.bcount, this.bsize),:) - this.CircBuffer(circ_buffer(this.bcount - this.bsize+1, this.bsize),:);
                this.Velocity = tmp(1:3) / tmp(4);
                
                this.Speed = norm(this.Velocity);
                
                if this.Collecting
                    this.DataCount = this.DataCount + 1;
                    for k=1:this.FrameVariableCount
                        this.Data(this.DataCount,this.k1(k):this.k2(k)) = this.(this.FrameVariableList{k});
                    end
                end
                
                if this.RobotCount == 2
                    this.Velocity = [this.Velocity  this.Velocity];
                    this.Speed = [this.Speed  this.Speed];
                end
                
            end

            if this.RobotCount == 2
                this.Position = [this.Position  [-1 0 0;  0 -1 0 ; 0 0 1] * this.Position];
            end
            ok = 1;
          
            %this.Velocity
            %this.Speed
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = DataStart(this)
            %DATASTART Initialises data collection variable to defaults
            this.Data = zeros(this.N,sum(this.FrameVariableSize));
            this.StartTime = GetSecs;
            this.DataCount = 0;
            this.Collecting = 1;
            this.LastTime = 0;
            
            ok = 1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = DataStop(this)
            %DATASTOP simmulates data stop and sets relevant flags to false
            this.Collecting = 0;
            ok = 1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ ok, FrameData ] = DataGet(this)
            %DATAGET Retrives frame data from the mouse.
            FrameData.Frames = this.DataCount;
            
            for i=1:this.FrameVariableCount
                FrameData.(this.FrameVariableList{i}) = this.Data(:, this.k1(i):this.k2(i));
            end
            
            ok = 1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = Start(this)
            %START Initialise wl_mouse speed varaibles to defaults
            ok = 1;
            this.Velocity = zeros(3, this.RobotCount);
            this.Speed = zeros(1, this.RobotCount);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %        ALL METHODS FROM THIS POINT ONWARDS DONT DO ANYTHING            %
        %            CHECK WL_MOUSE FOR DOCUMENTATION ON THESE                   %              
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = Stop(this)
            ok = 1;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = StateSet(varargin)
            ok =1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = RampUp(this)
            ok = 1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = RampDown(this)
            ok =1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldNull(this)
            ok = 1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldViscous(varargin)
            ok = 1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldSpring(varargin)
            ok = 1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldChannel(varargin)
            ok = 1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldPMove(varargin)
            ok =1;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok =FieldBimanualSpring(varargin)
            ok =1;
        end
    end
    
    
end


function k=circ_buffer(count, N)
    % Circular buffer, counts modulo N within an array of size N
    %
    %   count - (postive integer index potentially out of bounds)
    %   N     - size of the array/buffer
    %
    % this method is placed in a rather awkward location, and re-written
    % multiple times. Potentially worth adding it to functools or something
    k = mod(count-1, N) + 1;
end
