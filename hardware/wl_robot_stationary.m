function out = wl_robot_stationary(obj)
%WL_ROBOT_STATIONARY returns true if robot is stationary
% OUT = WL_ROBOT_STATIONARY(OBJ) takes in a pointer to the experiment
% object OBJ returning true in OUT if the robot is stationary false 
% otherwise
  %max(obj.Robot.Speed) > obj.cfg.StationarySpeed
  if obj.cfg.StationarySpeed == 0
    out = true;
    return;
  elseif max(obj.Robot.Speed) > obj.cfg.StationarySpeed
    obj.Robot.StationaryTimer.Reset;
  end

  out = obj.Robot.StationaryTimer.GetTime > obj.cfg.StationaryTime;
end
