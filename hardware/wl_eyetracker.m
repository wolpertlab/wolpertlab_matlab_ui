classdef wl_eyetracker<handle
    % MATLAB interface to the eyelink functionality in the MexHardwareFunc
    
    properties
        % Function Codes for mexHardwareFunction
        EYELINK_START     = 15;
        EYELINK_STOP      = 16;
        EYELINK_LATEST    = 17;
        EYELINK_SET_CALIB = 18;
        
        ActualType = 'eyelink';
        
        % Latest data.
        Frame;
        TimeStamp;
        EyeXY;
        PupilSize;
    end
    
    methods
        function this=wl_eyetracker
            %WL_EYETRACKER Constructor does not do anything (required by matlab)
            % wl_hardware carries out the startup procedure required for
            % wl_eyetracker to work
        end
        
        function ok = Start(this)
            %START enables the eyelink for the data collection loop in the
            % mexhardware function
            ok = mexHardwareFunc(this.EYELINK_START);
        end
        
        function ok = Stop(this)
            %STOP stops the data collection for the eyelink
            ok = mexHardwareFunc(this.EYELINK_STOP);
        end
        
        function ok = SetCalib(this, CalibMatrix)
            %SETCALIB sets the calibtration matrix for the eyelink
            ok = mexHardwareFunc(this.EYELINK_SET_CALIB,CalibMatrix);
        end
        
        function ok = GetLatest(this)
            %GETLATEST Collects the latest frame of data from the eyelink
            [ ok,FN,TS,EP,PS ] = mexHardwareFunc(this.EYELINK_LATEST);
            this.Frame = FN;
            this.TimeStamp = TS;
            this.EyeXY = EP;
            this.PupilSize = PS;
        end
    end
end
