classdef wl_robot < handle
%WL_ROBOT Is a matlab interface class to the MexHardwareFunc that allows to
% query and send data to the robot in a user friendly manner.
    
    properties
        % Function Codes for mexHardwareFunc()
        ROBOT_START                 = 0;  % Code for starting robot
        ROBOT_STOP                  = 1;  % Code for stopping robot
        ROBOT_GET_LATEST            = 2;  % Code for getting latest data
        ROBOT_RAMP_UP               = 7;  % Code for applying ramp up
        ROBOT_RAMP_DOWN             = 8;  % Code for applyying ramp down
        ROBOT_FIELD_NONE            = 9;  % Code for no robot field
        ROBOT_FIELD_VISCOUS         = 10; % Code for viscous field
        ROBOT_FIELD_SPRING          = 11; % Code for spring field
        ROBOT_FIELD_CHANNEL         = 12; % Code for channel field
        ROBOT_FIELD_PMOVE           = 13; % Code for pmove field
        ROBOT_FIELD_BIMANUAL_SPRING = 14; % Code for Bimuanual Spring field
        
        % Latest data.
        RobotCount;           % Variable storing the number of robots either 1 or 2
        StationaryTimer;      % wl_timer class instance 
        Active;               % Array indicating which robots are active
        RampValue;            % Array indicating which the ramp value of each robot
        FieldRampValue;       % Array indicating the field ramp value for each robot
        FieldType;            % Array indicating the field type of each robot
        PMoveFinished;        % Array indicating if each robot has finished
        Position;             % Array containing position vector for each robot
        Velocity;             % Array containing veclocity vector for each robot
        Speed;                % Array containing speed for each robot
        Forces;               % Array containing the force vector for each robot       
        ActualType = 'robot'; % String with the class type to differentiate from wl_mouse (seems unnecesary)
        RobotLeft;            % String value with name of left robot
        RobotRight;           % string value with name of right robot
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods

        function this=wl_robot(RobotLeft, RobotRight)
            %WL_ROBOT Takes in one or two string arguments with the identifier and sets up the class.
            %
            %  RobotLeft    - String with the name of the left robot (rig
            %                 dependant)
            %  RobotRight   - String with the name of the right robot (rig
            %                 dependant)

            this.StationaryTimer = wl_timer;
            this.RobotLeft = RobotLeft;
            if( nargin == 1 )
                this.RobotRight = '';
                this.RobotCount = 1;
            else
                this.RobotRight = RobotRight;
                this.RobotCount = 2;
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = Start(this)
            %START initiates the communication process with the robot
            %
            % Relevant class arguments Arguments to mexHardWareFunc in Start:
            %
            % this.ROBOT_START  - Integer code for the mexfunc to start the
            %                     robots
            % this.RobotLeft    - String correspoding to the name of the
            %                     left robot
            % this.RobotRight   - String corresponding to the name of the
            %                     right robot
            %
            % return values:
            %
            % ok                - boolean value 1 indicates start failed 0
            %                     indicates start was succesful

            ok = mexHardwareFunc(this.ROBOT_START, this.RobotLeft, this.RobotRight); % Starts robot;
            
            fprintf(1,'RobotCount=%d, RobotName=[%s],[%s]\n', this.RobotCount, this.RobotLeft, this.RobotRight);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = Stop(this)
            %STOP stops all connections with the robots
            
            % Stops all robot related processes passing the ROBOT_STOP
            % integer code to the mexHardwareFunc
            ok = mexHardwareFunc(this.ROBOT_STOP);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = GetLatest(this)
            % Updates all the data attributes of the robot with the lastest data

            [ ok,RS,RP,RV,RF ] = mexHardwareFunc(this.ROBOT_GET_LATEST);
            this.Active         = RS(1,:);
            this.RampValue      = RS(2,:);
            this.FieldRampValue = RS(3,:);
            this.FieldType      = RS(4,:);
            this.PMoveFinished  = RS(5,:);
            this.Position       = RP;
            this.Velocity       = RV;
            this.Forces         = RF;
            
            for robot=1:this.RobotCount
                this.Speed(robot) = norm(RV(:,robot));
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = RampUp(this, RobotIndex)
            %RAMPUP given a selected robot it applies a ramp up force
            % (linearly increasing)to the specified robot
            %
            %   RobotIndex    -  Integer valued 0 or 1 index corresponding to
            %                    the left or right robot
            ok = false;
            
            if( this.RobotCount == 2 )
                if( nargin == 2 )
                    ok = mexHardwareFunc(this.ROBOT_RAMP_UP, RobotIndex);
                else
                    ok = mexHardwareFunc(this.ROBOT_RAMP_UP, 0) && mexHardwareFunc(this.ROBOT_RAMP_UP, 1);
                end
            elseif( this.RobotCount == 1 )
                ok = mexHardwareFunc(this.ROBOT_RAMP_UP);
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = RampDown(this, RobotIndex)
            %RAMPDOWN given a selected robot it applies a ramp down force
            % (linearly decreasing)to the specified robot
            %
            %   RobotIndex    -  Integer valued 0 or 1 index corresponding to
            %                    the left or right robot
            ok = false;
            
            if( this.RobotCount == 2 )
                if( nargin == 2 )
                    ok = mexHardwareFunc(this.ROBOT_RAMP_DOWN,RobotIndex);
                else
                    ok = mexHardwareFunc(this.ROBOT_RAMP_DOWN, 0);
                    ok = mexHardwareFunc(this.ROBOT_RAMP_DOWN, 1) && ok;
                end
            elseif( this.RobotCount == 1 )
                ok = mexHardwareFunc(this.ROBOT_RAMP_DOWN);
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldNull(this, RobotIndex)
            %FIELDNULL given a selected robot it applies a null field
            % (stops all forces)
            %
            %   RobotIndex    -  Integer valued 0 or 1 index corresponding to
            %                    the left or right robot

            if( this.RobotCount == 2 )
                if( nargin == 2 )
                    ok = mexHardwareFunc(this.ROBOT_FIELD_NONE, RobotIndex);
                else
                    ok = mexHardwareFunc(this.ROBOT_FIELD_NONE, 0);
                    ok = mexHardwareFunc(this.ROBOT_FIELD_NONE, 1) && ok;
                end
            elseif( this.RobotCount == 1 )
                ok = mexHardwareFunc(this.ROBOT_FIELD_NONE);
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldViscous(this, ViscousMatrix, RobotIndex)
            %FIELDVISCOUS given a selected robot and a specified viscocity matrixc
            % it applies a velocity dependant force field to the selected
            % robot :
            %               F(r') = ViscousMatrix * r'
            % Where r' is a velocity vector [x', y', z'] (x' = d x(t) / dt)
            %
            %   ViscousMatrix -  3x3 (usually negative integer valued)
            %                    matrix that specifies the viscocity
            %                    constants and coupling
            %
            %   RobotIndex    -  Integer valued 0 or 1 index corresponding to
            %                    the left or right robot

            if( this.RobotCount == 2 )
                ok = mexHardwareFunc(this.ROBOT_FIELD_VISCOUS, RobotIndex, ViscousMatrix);
            else
                ok = mexHardwareFunc(this.ROBOT_FIELD_VISCOUS, ViscousMatrix);
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldSpring(this, SpringHome, SpringConstant, DampingConstant, RobotIndex)
            %FIELDSPRING given a selected robot and a specified Spring
            % Constant, Location and Damping constant it applies a spring
            % simmulated force to the specified robot :
            %
            %  F(r, r') = SpringConstant * (r - SpringHome) + DampingConstant * r'
            %
            % Where r' is a velocity vector [x', y', z'] and r is a position vector [x, y, z]
            %
            %   SpringHome      -  3x1 Vector specifying the equilibrium (start position of the spring) 
            %
            %   SpringConstant  -  Negative valued constant of
            %                      proportionality (see Hookes law)
            %
            %   DampingConstant -  Friction constant (proportionality to
            %                      velocity) usually negative.
            %                            
            %   RobotIndex      -  Integer valued 0 or 1 index corresponding to
            %                      the left or right robot
            if( this.RobotCount == 2 )
                ok = mexHardwareFunc(this.ROBOT_FIELD_SPRING, RobotIndex, SpringHome, SpringConstant, DampingConstant);
            else
                ok = mexHardwareFunc(this.ROBOT_FIELD_SPRING, SpringHome, SpringConstant, DampingConstant);
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldChannel(this, ChannelTarget, SpringConstant, DampingConstant, RobotIndex)
            %FieldChannel given a selected robot and a specified Spring
            % Constant, and Damping constant it applies a spring on either
            % side of a channel/line towards ChannelTarget.
            %
            %   ChannelTarget   -  Position target of the channel Trial
            %
            %   SpringConstant  -  Negative valued constant of
            %                      proportionality (see Hookes law)
            %
            %   DampingConstant -  Friction constant (proportionality to
            %                      velocity) usually negative.
            %                            
            %   RobotIndex      -  Integer valued 0 or 1 index corresponding to
            %                      the left or right robot
            if( this.RobotCount == 2 )
                ok = mexHardwareFunc(this.ROBOT_FIELD_CHANNEL, RobotIndex, ChannelTarget, SpringConstant, DampingConstant);
            else
                ok = mexHardwareFunc(this.ROBOT_FIELD_CHANNEL, ChannelTarget, SpringConstant, DampingConstant);
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldPMove(this, MoveTarget, MoveTime, HoldTime, RobotIndex)
            %FIELDPMOVE applies the passive return movement towards MoveTarget for 
            % the duration of MoveTime at a given RobotIndex
            %
            % TODO: DETERMINE WHAT THE HOLDTIME PARAM DOES, seems to be the
            % time before ramping down the force whilst move 
            if( this.RobotCount == 2 )
                ok = mexHardwareFunc(this.ROBOT_FIELD_PMOVE, RobotIndex, MoveTarget, MoveTime, HoldTime);
            else
                ok = mexHardwareFunc(this.ROBOT_FIELD_PMOVE, MoveTarget, MoveTime, HoldTime);
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = FieldBimanualSpring(this, BimanualSpringType, SpringConstant, DampingConstant)
            %FIELDBIMANUALSPRING Creates a spring using both robotic manipulands centered at intersrection between handles
            ok = mexHardwareFunc(this.ROBOT_FIELD_BIMANUAL_SPRING, BimanualSpringType, SpringConstant, DampingConstant);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
end