function wl_choose_keyboard()
%WL_CHOSE_KEYBOARD method to select the keyboard from a list of keyboards
%   WL_CHOSE_KEYBOARD called with no arguments , press esc key of the
%   desired keyboard and this function will save its ID for later usage
    clear all
    
    [id names info] = GetKeyboardIndices();
    key_id=id(1);
    n=length(id);
    k=1;
    Exit=0;
    fprintf(1,'Hit the Escape key multiple times\n')
    while ~Exit
        if wl_everyhz(2)
            k=mod(k+1,n)+1;
            key_id=id(k);
            
            KbQueueCreate(key_id);
            KbQueueStart(key_id);
        end
        [pressed, firstPress, firstRelease, lastPress, lastRelease] = KbQueueCheck(key_id);
        
        if pressed && firstPress(KbName('ESCAPE'))
            Exit=1;
        end
    end
    KbQueueStop(key_id)
    fprintf(1,'keyboard id =%i\n',key_id)
    
    save kbd_default key_id;
end