classdef wl_hardware<handle
    %WL_HARDWARE is an interface that controls the multiple hardware
    % objects associated with the sensorimotor experiments, such as the
    % robot, liberty, eyetracker , etc .. 
    
    properties
        % Hardware function codes for mexHardwareFunc()
        HARDWARE_DATA_START     = 3;
        HARDWARE_DATA_STOP      = 4;
        HARDWARE_DATA_GET       = 5;
        HARDWARE_STATE_SET      = 6;
        
        % Hardware data field types
        HARDWARE_DATA_GENERAL   = 100;
        HARDWARE_DATA_ROBOT     = 200;
        HARDWARE_DATA_EYELINK   = 300;
        HARDWARE_DATA_LIBERTY   = 400;
        
        GeneralVariableList = { 'TimeStamp','State','StateTime' };
        GeneralVariableSize = [ 1,1,1 ];
        GeneralVariableCount;
        
        RobotVariableList = { 'RobotActive','RobotRamp','RobotFieldRamp','RobotFieldType','RobotPosition','RobotVelocity','RobotForces' };
        RobotVariableSize = [ 1,1,1,1,3,3,3 ];
        RobotVariableCount;
        
        EyeLinkVariableList = { 'EyeLinkFrame','EyeLinkTimeStamp','EyeLinkEyeXY','EyeLinkPupilSize' };
        EyeLinkVariableSize = [ 1,1,2,1 ];
        EyeLinkVariableCount;
        
        LibertyVariableList = { 'LibertyFrame','LibertyTimeStamp' };
        LibertyVariableSize = [ 1,1 ];
        LibertyVariableCount;
        
        LibertyVariableSensorList = { 'LibertyPosition','LibertyOrientation','LibertyDistortion' };
        LibertyVariableSensorSize = [ 3,3,1 ];
        LibertyVariableSensorCount;
        
        RobotCount=0;
        Robot;
        
        MouseCount=0;
        Mouse;
        
        EyeLinkCount=0;
        EyeLink;
        
        EyeCount=0;
        Eye;
        
        LibertyCount=0;
        Liberty;
        
        Hardware=0;
        
        k01;
        k02;
        k03;
        k04;
        k05;
        k06;
        k07;
        k08;
        k09;
        k10;
    end
    
    methods
        function this = wl_hardware(varargin)
            for k=1:length(varargin)
                if strcmp(varargin{k}.ActualType,'robot')
                    this.Robot = varargin{k};
                    this.RobotCount = this.Robot.RobotCount;
                elseif strcmp(varargin{k}.ActualType,'mouse')
                    this.Mouse = varargin{k};
                    this.MouseCount = 1;
                elseif strcmp(varargin{k}.ActualType,'eyelink')
                    this.EyeLink = varargin{k};
                    this.EyeLinkCount = 1;
                elseif strcmp(varargin{k}.ActualType,'eye')
                    this.Eye = varargin{k};
                    this.EyeCount = 1;
                elseif strcmp(varargin{k}.ActualType,'liberty')
                    this.Liberty = varargin{k};
                    this.LibertyCount = 1;
                end
            end
            
            if (this.RobotCount > 0) || (this.EyeLinkCount > 0) || (this.LibertyCount > 0)
                this.Hardware = true;
            end
            
            this.GeneralVariableCount = length(this.GeneralVariableList);
            this.RobotVariableCount = length(this.RobotVariableList);
            this.EyeLinkVariableCount = length(this.EyeLinkVariableList);
            this.LibertyVariableCount = length(this.LibertyVariableList);
            this.LibertyVariableSensorCount = length(this.LibertyVariableSensorList);
            
            % Cumulative offsets for hardware variable lists.
            this.k01 = cumsum([ 0 this.GeneralVariableSize(1:end-1) ]) + 1;
            this.k02 = cumsum(this.GeneralVariableSize);
            
            this.k03 = cumsum([ 0 this.RobotVariableSize(1:end-1) ]) + 1;
            this.k04 = cumsum(this.RobotVariableSize);
            
            this.k05 = cumsum([ 0 this.EyeLinkVariableSize(1:end-1) ]) + 1;
            this.k06 = cumsum(this.EyeLinkVariableSize);
            
            this.k07 = cumsum([ 0 this.LibertyVariableSize(1:end-1) ]) + 1;
            this.k08 = cumsum(this.LibertyVariableSize);
            
            this.k09 = cumsum([ 0 this.LibertyVariableSensorSize(1:end-1) ]) + 1;
            this.k10 = cumsum(this.LibertyVariableSensorSize);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = Start(this)
            %START instantiates the robot, mouse, eyetracker and liberty
            %objects
            ok = true;
            
            if( ok && (this.RobotCount > 0) )
                ok = this.Robot.Start;
            end
            
            if( ok && (this.MouseCount > 0) )
                ok = this.Mouse.Start;
            end
            
            if( ok && (this.EyeLinkCount > 0) )
                ok = this.EyeLink.Start;
            end
            
            if( ok && (this.EyeCount > 0) )
                ok = this.Eye.Start;
            end
            
            if( ok && (this.LibertyCount > 0) )
                ok = this.Liberty.Start;
            end
            
            fprintf('Hardware Start=%d\n',ok);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = Stop(this)
            %STOP stops the robot, mouse, eyetracker and liberty
            %objects
            ok = true;
            
            if( ok && (this.RobotCount > 0) )
                ok = this.Robot.Stop;
            end
            
            if( ok && (this.MouseCount > 0) )
                ok = this.Mouse.Stop;
            end
            
            if( ok && (this.EyeLinkCount > 0) )
                ok = this.EyeLink.Stop;
            end
            
            if( ok && (this.EyeCount > 0) )
                ok = this.Eye.Stop;
            end
            
            if( ok && (this.LibertyCount > 0) )
                ok = this.Liberty.Stop;
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = GetLatest(this, obj)
            %GETLATEST this method updates the data collection matrix of
            %every hardware device in the experiment by adding a row with
            %the latest data at the end of the matri/table.
            ok = true;
            
            if( ok && (this.RobotCount > 0) )
                ok = this.Robot.GetLatest;
            end
            
            if( ok && (this.MouseCount > 0) )
                ok = this.Mouse.GetLatest();
            end
            
            if( ok && (this.EyeLinkCount > 0) )
                ok = this.EyeLink.GetLatest;
            end
            
            if( ok && (this.EyeCount > 0) )
                ok = this.Eye.GetLatest;
            end
            
            if( ok && (this.LibertyCount > 0) )
                ok = this.Liberty.GetLatest;
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = DataStart(this)
            %DATASTART Initiates the mexhardware function data collection
            %loop with the robot and other hardware components in the
            %experiment. This loop updates the the data collection matrix
            %at a particular sample rate
            ok = false;
            
            if this.Hardware
                ok = mexHardwareFunc(this.HARDWARE_DATA_START);
            end
            
            if this.MouseCount
                ok = this.Mouse.DataStart();
            end
            
            if this.EyeCount
                ok = this.Eye.DataStart();
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = DataStop(this)
            %DATASTOP instructs the mexhardware function to terminate the
            %data collection loop 
            ok = false;
            
            if this.Hardware
                ok = mexHardwareFunc(this.HARDWARE_DATA_STOP);
            end
            
            if this.MouseCount > 0
                ok = this.Mouse.DataStop();
            end
            
            if this.EyeCount > 0
                ok = this.Eye.DataStop();
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ ok,FrameData,names ] = DataGet(this)
            %DATAGET signals the mexHardWareFunc to collect the most recent
            %data frame from the datamatrix and return it in a cell array
            %structured format
            ok = false;
            FrameData = [ ];
            names = { };
            
            k = 0;
            
            if this.Hardware
                [ ok,Data ] = mexHardwareFunc(this.HARDWARE_DATA_GET);
                
                % Process data only if some was returned.
                if( ok && ~isempty(Data) )
                    k = k + 1;
                    names{k} = '';
                    FrameData{k}.Frames = size(Data,2)-1;
                    
                    % What fields are present in the frame data?
                    FieldList = Data(:,1);
                    FieldGeneral = any((FieldList >= this.HARDWARE_DATA_GENERAL) & (FieldList < this.HARDWARE_DATA_ROBOT));
                    FieldRobot = any((FieldList >= this.HARDWARE_DATA_ROBOT) & (FieldList < this.HARDWARE_DATA_EYELINK));
                    FieldEyeLink = any((FieldList >= this.HARDWARE_DATA_EYELINK) & (FieldList < this.HARDWARE_DATA_LIBERTY));
                    FieldLiberty = any(FieldList >= this.HARDWARE_DATA_LIBERTY);
                    
                    offset = 0;
                    
                    if( FieldGeneral )
                        for i=1:this.GeneralVariableCount
                            FrameData{k}.(this.GeneralVariableList{i}) = Data(offset+(this.k01(i):this.k02(i)),2:end);
                        end
                        
                        offset = offset + this.k02(end);
                    end
                    
                    if( FieldRobot )
                        for RobotIndex=1:this.RobotCount
                            for i=1:this.RobotVariableCount
                                if( this.RobotCount == 2 )
                                    if( this.RobotVariableSize(i) == 1 )
                                        FrameData{k}.(this.RobotVariableList{i})(RobotIndex,:) = Data(offset+(this.k03(i):this.k04(i)),2:end);
                                    else
                                        FrameData{k}.(this.RobotVariableList{i})(RobotIndex,:,:) = Data(offset+(this.k03(i):this.k04(i)),2:end);
                                    end
                                else
                                    FrameData{k}.(this.RobotVariableList{i}) = Data(offset+(this.k03(i):this.k04(i)),2:end);
                                end
                            end
                            
                            offset = offset + this.k04(end);
                        end
                    end
                    
                    if( FieldEyeLink )
                        for i=1:this.EyeLinkVariableCount
                            FrameData{k}.(this.EyeLinkVariableList{i}) = Data(offset+(this.k05(i):this.k06(i)),2:end);
                        end
                        
                        offset = offset + this.k06(end);
                    end
                    
                    if( FieldLiberty )
                        for i=1:this.LibertyVariableCount
                            FrameData{k}.(this.LibertyVariableList{i}) = Data(offset+(this.k07(i):this.k08(i)),2:end);
                        end
                        
                        offset = offset + this.k08(end);
                        
                        for j=1:this.Liberty.SensorCount
                            for i=1:this.LibertyVariableSensorCount
                                FrameData{k}.(this.LibertyVariableSensorList{i})(j,:,:) = Data(offset+(this.k09(i):this.k10(i)),2:end);
                            end
                            
                            offset = offset + this.k10(end);
                        end
                    end
                end
            end
            
            if this.MouseCount
                k = k + 1;
                [ ok,FrameData{k} ] = this.Mouse.DataGet();
                names{k} = 'mouse_';
            end
            
            if this.EyeCount
                k = k + 1;
                [ ok,FrameData{k} ] = this.Eye.DataGet();
                names{k} = 'eye_';
            end
        end
        
        function ok = StateSet(this,State,StateTime)
            %STATESET sets a particular data state for the frames in the
            %data matrix
            if this.Hardware
                ok = mexHardwareFunc(this.HARDWARE_STATE_SET,State,StateTime);
            else
                ok = true;
            end
        end
    end
end


