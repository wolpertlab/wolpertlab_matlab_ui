classdef wl_liberty<handle
    % Summary of wl_liberty class
    
    properties
        % Function Codes for mexHardwareFunc()
        LIBERTY_START                 = 19;
        LIBERTY_STOP                  = 20;
        LIBERTY_GET_LATEST            = 21;
        
        % Latest data.
        LibertyConfig;
        FrameCount;
        FrameTime;
        SensorCount=0;
        SensorPosition;
        SensorOrientation;
        SensorDistortion;
        
        ActualType='liberty';
        
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        function this = wl_liberty(cfg)
            %loads the liberty config cfg required by motor.lib
            this.LibertyConfig = cfg;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = Start(this)
            %START signals the mexhardware function to start the liberty
            %data collection
            [ ok,SC ] = mexHardwareFunc(this.LIBERTY_START,this.LibertyConfig);
            
            if( ok )
                fprintf(1,'Liberty started with %d sensor(s).\n',SC);
                this.SensorCount = SC;
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = Stop(this)
            %STOP stops the data collection for the liberty
            ok = mexHardwareFunc(this.LIBERTY_STOP);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = GetLatest(this)
            %GETLATEST signals the mexfunction to obtain the latest frame
            %from the liberty data
            [ ok,FC,FT,SP,SO,SD ] = mexHardwareFunc(this.LIBERTY_GET_LATEST);

            if( ok )
                this.FrameCount         = FC;
                this.FrameTime          = FT;
                this.SensorPosition     = SP;
                this.SensorOrientation  = SO;
                this.SensorDistortion   = SD;
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
end