function [ok, TrialData]=wl_trial_save(obj)
%WL_TRIAL_SAVE updates TrialData matrix in the experiment with the results
%of the latest trial and saves data to a mat file if flag is enabled.
%   ok = WL_TRIAL_SAVE(OBJ) takes in a pointer to the experiment object
%   OBJ and returns a boolean flag which is true if the save was
%   successful.
%   if OBJ.cfg.trial_save is set to true then a mat file will be saved
%   everytime this method is called. TODO: remove redundant return D
%   TODO: I think the D parameter is potentially set by one of the loads

    if isfield(obj.cfg, 'Debug')  && obj.cfg.Debug
        memory
    end

    obj.Timer.System.TrialSave.Tic;

    %%%%%%%%%%% resave line of the table %%%%%%%%%%%
    fn = fieldnames(obj.TrialData);  %fieldnames in TrialData
    obj.cfg.TrialData_Single = table();   % empty table (temprorary variable)
    for k=1:cols(obj.TrialData)       %add current items to trial data by reading from obj.Trial to  create a single line of table data
        if min(size(obj.Trial.(fn{k}))) > 1 % is field multi-dimensional matrix? (not a scalar or a vector)
            obj.cfg.TrialData_Single.(fn{k}){1} = obj.Trial.(fn{k}); % this will create a single line of table data
        elseif ischar(obj.Trial.(fn{k})) % is field a character or string array?
            obj.cfg.TrialData_Single.(fn{k}) = cellstr(obj.Trial.(fn{k})); % this will create a single line of table data
        else % numerical scalar or vector
            obj.cfg.TrialData_Single.(fn{k}) = shiftdim(obj.Trial.(fn{k}))'; % this will create a single line of table data
        end
    end

    % First, add new row to the "WL" version of the table for current trial number.
    obj.TrialData(obj.TrialNumber,:) = obj.cfg.TrialData_Single;

% Looks like this doesn't do anything.    
%     if exist(obj.GW.table_file, 'file')
%         load(obj.GW.table_file);
%         % trial=rows(TrialData)+1; JH CHANGE
%         trial = obj.TrialNumber;
%         TrialData(trial,:) = obj.cfg.TrialData_Single;
%     else
%         trial = 1;
%         TrialData = obj.cfg.TrialData_Single;
%     end

    %%%%%%%%%%% save TabledData to separate file %%%%%%%
    TrialData = obj.TrialData; % completely ignore above ???
    if obj.cfg.trial_save
        save(obj.GW.table_file, 'TrialData');
    end

    %%%%%%%%%%% get trial data %%%%%%%

    %%%%%%%%%%% write out all data %%%%%%%
    if obj.cfg.trial_save
        m = matfile(obj.GW.save_file, 'Writable', true); %moved up ?OK
    end
    
    [ok, obj.GW.FrameData, names] = obj.Hardware.DataGet(); %get frame data
    fprintf(1,'wl_trial_save: ok=%d\n',ok);

    if ok && obj.cfg.trial_save
        N = obj.GW.FrameData{1}.Frames;
        fprintf(1,'%d frames of data.\n', N);

        % FrameData is a cell array of different data types.
        for j=1:length(obj.GW.FrameData)  % this can be different hardware
            obj.GW.FrameData{j}.Trial = obj.TrialNumber; % trial number
            vars_to_save = fieldnames(obj.GW.FrameData{j});

            % loop over variables
            for k=1:length(vars_to_save)
                % load variables and remove redundant dimensions
                tmp = shiftdim(obj.GW.FrameData{j}.(vars_to_save{k}));
                vname = ([names{j} vars_to_save{k}]);

                d = ndims(tmp); %dimensions -  currently allow up to trials x 3 dims (i.e. 4) but can extend to more
                s = size(tmp);

                if isvector(tmp)
                    m.(vname)(obj.TrialNumber,1:s(1)) = tmp';
                elseif ismatrix(tmp)  % 2-d matrix
                    m.(vname)(obj.TrialNumber,1:s(1),1:s(2)) = reshape(tmp,[1 s]);
                elseif d==3 %N-D array of size 3
                    m.(vname)(obj.TrialNumber,1:s(1),1:s(2),1:s(3)) = reshape(tmp,[1 s]);
                end
            end
        end
    end

    m.Time(obj.TrialNumber,1) = GetSecs(); %same actual time
    clear m; % close the file

    ok = true;
    obj.Timer.System.TrialSave.Toc;
end
