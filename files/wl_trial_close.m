function wl_trial_close(obj)
% Saves TrialData and matlab Code to the save file specified by
% obj.GW.save_file
%
%  OBJ:  wl_experiment pointer
if exist(obj.GW.table_file,'file')
    load(obj.GW.table_file);
    save(obj.GW.save_file, '-append', 'TrialData');
    delete(obj.GW.table_file);
end

if( (obj.TrialNumber >= 10) && obj.cfg.trial_save )
    d = dbstack;
    fname = d(end).file(1:(end-2));
    
    Code = wl_export_mfiles(fname);
    
    m = matfile(obj.GW.save_file, 'Writable', true);
    m.Code = Code;
    clear m; % close the file
end

end