function D = wl_load_data(file)
%WL_LOAD_DATA loads in a saved matfile with experiment results as a table
%   D = WL_LOAD_DATA(FILE) takes as input a path FILE and loads struct +
%   experiment results table (D.FrameData) into D

    D = load(file);

    D.TotalTrials = length(D.Trial);
    D.MissTrials = sum(D.TrialData.MissTrial);
    D.GoodTrials = D.TotalTrials - D.MissTrials;
    
    D.FrameData.Frames = D.Frames;
    D.FrameData.TimeStamp = D.TimeStamp;
    D.FrameData.State = D.State;
    D.FrameData.StateTime = D.StateTime;
    D.FrameData.RobotActive = D.RobotActive;
    D.FrameData.RobotRamp = D.RobotRamp;
    D.FrameData.RobotFieldRamp = D.RobotFieldRamp;
    D.FrameData.RobotFieldType = D.RobotFieldType;
    D.FrameData.RobotForces = D.RobotForces;
    D.FrameData.RobotPosition = D.RobotPosition;
    D.FrameData.RobotVelocity = D.RobotVelocity;
    
    D = rmfield(D,'Frames');
    D = rmfield(D,'TimeStamp');
    D = rmfield(D,'State');
    D = rmfield(D,'StateTime');
    D = rmfield(D,'RobotActive');
    D = rmfield(D,'RobotRamp');
    D = rmfield(D,'RobotFieldRamp');
    D = rmfield(D,'RobotFieldType');
    D = rmfield(D,'RobotForces');
    D = rmfield(D,'RobotPosition');
    D = rmfield(D,'RobotVelocity');
end
