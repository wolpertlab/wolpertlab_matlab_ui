function T = wl_export_mfiles(funcname)
%WL_EXPORT_MFILES - Creates a ZIP file containing all dependencies of a function
%
% zipfilename = WL_EXPORT_MFILES(funcname)
% zipfilename = WL_EXPORT_MFILES(funcname,zipfilename)
%
% All M-files which are required by the specified function(s) and which are
% not inside MATLAB's "toolbox" directory.
% funcname is one or more function names, as a string or cell array of strings.
%
% If no zipfilename is specified, the ZIP file is created in the current
% working directory with name X.zip where X is the first function name in
% the supplied list.  The file names in the ZIP file are relative to the
% common root directory directory of all the required files.  If no
% common root directory can be established (e.g. if files are on
% different drives) an error is thrown.
%
  
  % Copyright 2006-2010 The MathWorks, Inc.
  
  if ~iscell(funcname)
    funcname = {funcname};
  end
  
  if isempty(funcname)
    error('No function names specified');
  end
  
  if ~iscellstr(funcname)
    error('Function names must be strings');
  end
  
  if nargin<2
    zipfilename = fullfile(pwd,[ funcname{1} '.zip' ]);
  end
  
  req = cell(size(funcname));
  for i=1:numel(funcname)
    req{i} = matlab.codetools.requiredFilesAndProducts(funcname{i},'toponly');
  end
  req = vertcat(req{:}); % cell arrays of full file names
  req = unique(req);
  count=0;
  
  for i=1:numel(req)
    %  XXX NEED FIXING
    if isempty(strfind(upper(req{i}),'APPLICATION')) & isempty(strfind(upper(req{i}),'MEX'))
      count=count+1;
      out{count} = req{i}; % step over last character (which is the separator)
    end
  end
  
  req=out;
  odir=cd;
  
  % Find the common root directory
  % Calculate relative paths for all required files.
  for i=1:numel(req)
    [d fname] = parse_path(req{i});
    
    % This is the bit that can't be vectorised
    
    cd(d);
   
    [dd,NAME,EXT] = fileparts(fname);
    
    T.files.(NAME)=fileread(fname);
  end
  
  cd(odir);

  % save date matlab version and hostname - would like to save user
  % ?possble fro looking at u drive
  T.files.date=datestr(clock);
  T.files.version=version('-release');
  [~, T.files.system] = system('hostname');
  T.files.arch=computer('arch');
end

%%%%%%%%%%%%%%%%%%%
function [dd fname] = parse_path(d)
  % Identifies the parent directory, including a trailing separator

  % Include trailing separator in all comparisons so we don't assume that
  % file C:\tempX\file.txt is in directory C:\temp
  [dd,NAME,EXT] = fileparts(d);
  fname=[  NAME  EXT];
end

