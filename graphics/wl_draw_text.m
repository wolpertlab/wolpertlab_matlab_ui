function wl_draw_text (obj, txt, x, y, varargin)
%WL_DRAW_TEXT draws text in a position of a psychtoolbox screen
%   WL_DRAW_TEXT(OBJ,TXT,X,Y) draws TXT at position X,Y on screen using
%   the default window settings specified by the experiment pointer OBJ
%
%   WL_DRAW_TEXT(OBJ,TXT,X,Y,COL,FLIPH,FLIPV,FONTSIZE) customizes the
%   drawing of the TXT to color COL (Specified by an RGB array [ 1 1 1] is
%   white ) , fontsize FONTSIZE which is an integer and boolean parameters
%   FLIPV , FLIPH which specify weather to flip the txt along an axis

  pSet = inputParser;
  addParameter(pSet, 'col', [1 1 1]);  %
  addParameter(pSet, 'fliph', 0);  %
  addParameter(pSet, 'flipv', 0);  %
  addParameter(pSet, 'fontsize', 24);  %
  
  
  parse(pSet,varargin{:});
  v2struct(pSet.Results);
  
  
  Screen('TextFont',obj.Screen.window, 'Arial');
  Screen('TextSize',obj.Screen.window, fontsize);
  Screen('TextStyle', obj.Screen.window, 1);
  Screen('TextColor', obj.Screen.window, col);
  
  DrawFormattedText(obj.Screen.window, txt, x, y, col, [], flipv, fliph);
end
