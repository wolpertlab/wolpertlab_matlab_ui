function wl_draw_sphere(pos, radius, col, varargin)
%WL_DRAW_SPHERE draws a sphere on the active psychtoolbox screen
%   WL_DRAW_SPHERE(POS,RADIUS,COL) draws a sphere at POS ([X Y Z]) of
%   scalar RADIUS and color COL ([R G B])
%
%   WL_DRAW_SPHERE(POS,RADIUS,COL,ALPHA,SLICES,STACKS) Draws a sphere as
%   per above with aditional customisation parameters such as the
%   transparency level ALPHA (goes from 0 to 1) and the Number of SLICES
%   and STACKS used to construct the sphere , set by default to 20 (this
%   alters the resolution of the sphere)
global GL

pSet = inputParser;
addParameter(pSet, 'Alpha', 1);
addParameter(pSet, 'Slices', 20);
addParameter(pSet, 'Stacks', 20);
addParameter(pSet, 'wire', false);
parse(pSet,varargin{:});
v2struct(pSet.Results);

%glLoadIdentity;
glPushMatrix();

% Draw the cursor
glTranslatef(pos(1), pos(2), pos(3));
glMaterialfv(GL.FRONT_AND_BACK, GL.AMBIENT, [col Alpha]);
glMaterialfv(GL.FRONT_AND_BACK, GL.DIFFUSE, [col Alpha]);

if wire
    glutWireSphere(radius, Slices, Stacks);
else
    glutSolidSphere(radius, Slices, Stacks);
end

glPopMatrix();
