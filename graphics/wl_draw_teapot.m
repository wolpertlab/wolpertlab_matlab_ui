function wl_draw_teapot(obj)
%WL_DRAW_TEAPOT draws the classic open GL rotating teapot example at
% hardcoded positions and settings in the screen
%   WL_DRAW_TEAPOT(OBJ) takes in a pointer to the experiment object in
%   order to draw on the correct screen

global GL

TeapotSize=10.0;
TeapotRotateSpeed=40;
Screen('BeginOpenGL', obj.Screen.window);

glClearColor(0, 0, 0.5, 0);
glClear;
glLoadIdentity();
glTranslatef(0,0,-100);
glRotated(obj.GW.STATE.Timer.GetTime * TeapotRotateSpeed,1.0,1.0,1.0);
glLineWidth(1.0);
glMaterialfv(GL.FRONT_AND_BACK,GL.AMBIENT, [1 1 1 1]);
glMaterialfv(GL.FRONT_AND_BACK,GL.DIFFUSE, [1 1 1 1]);
glutWireTeapot(TeapotSize * obj.GW.RestBreakRemainPercent);
Screen('EndOpenGL',obj.Screen.window)
