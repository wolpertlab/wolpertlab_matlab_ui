function wl_start_screen(obj, w)
%WL_START_SCREEN draws psychtoolbox experiment screeen and sets up with
%initial text and graphics.
%   WL_START_SCREEN(OBJ) Draws screen in the third monitor at full screen
%   where OBJ is the pointer to the Experiment class
%
%   WL_START_SCREEN(OBJ, W) Draws screen at position and size specified by W
%   which takes the form [X, Y, XDELTA, YDELTA] this is used for debugging
%   mode when OBJ.cfg.MouseFlag is true.

global GL
% Here we call some default settings for setting up Psychtoolbox

% Skip sync tests for this demo in case people are using a defective
% system. This is for demo purposes only.

% Find the screen to use for display

PsychDefaultSetup(2);
InitializeMatlabOpenGL(2);

Screen('Preference', 'SkipSyncTests', 2);

screenid = max(Screen('Screens'));
% Initialise OpenGL
if isfield(obj.cfg, 'SmallScreen') && obj.cfg.SmallScreen
    w = get(0, 'ScreenSize') .* [0 0 0.5 0.5];
end

% w specifies position across monitors for the screen placement
if obj.cfg.MouseFlag && nargin == 2
    [window, windowRect] = PsychImaging('OpenWindow', 0, 0.5*[1 1 1], w, 32, 2);
else
    [window, windowRect] = PsychImaging('OpenWindow', screenid, 0.5*[1 1 1]);
end

obj.Screen.window = window;
obj.Screen.windowRect = windowRect;
obj.Screen.cent(1,1) = windowRect(3) / 2;
obj.Screen.cent(2,1) = windowRect(4) / 2;
obj.Screen.cent(3,1) = 0;

Screen('ColorRange', obj.Screen.window, 1);
obj.Screen.ifi = Screen('GetFlipInterval', obj.Screen.window);
obj.Screen.ar = windowRect(3) / windowRect(4);



if ~isfield(obj.Screen,'xcm') || ~isfield(obj.Screen,'ycm')
    obj.cfg.graphics_config = MexReadConfig('GRAPHICS');
    obj.Screen.xcm = obj.cfg.graphics_config.Xmin_Xmax;
    obj.Screen.ycm = obj.cfg.graphics_config.Ymin_Ymax;
end

%HideCursor(screenid);
Screen('BlendFunction', obj.Screen.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
Screen('BeginOpenGL', obj.Screen.window);

% Enable lighting
glEnable(GL.LIGHTING);

% Define a local light source
glEnable(GL.LIGHT0);

% Enable proper occlusion handling via depth tests
glEnable(GL.DEPTH_TEST);

glEnable(GL.BLEND);
glBlendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);

% Lets set up a projection matrix, the projection matrix defines how images
% in our 3D simulated scene are projected to the images on our 2D monitor
glMatrixMode(GL.PROJECTION);
glLoadIdentity;
glOrtho(obj.Screen.xcm(1), obj.Screen.xcm(2), obj.Screen.ycm(1),obj.Screen.ycm(2), -10, 200);

% Setup modelview matrix: This defines the position, orientation and
% looking direction of the virtual camera that will be look at our scene.
glMatrixMode(GL.MODELVIEW);

% Our point lightsource is at position (x,y,z) == (1,2,3)
glLightfv(GL.LIGHT0, GL.POSITION, [1 2 3 0]);

% Location of the camera is at the origin
cam = [0 0 -50];

% Set our camera to be looking directly down the Z axis (depth) of our
% coordinate system
fix = [0 0 -100];

% Define "up"
up = [0 1 0];

% Here we set up the attributes of our camera using the variables we have
% defined in the last three lines of code
gluLookAt(cam(1), cam(2), cam(3), fix(1), fix(2), fix(3), up(1), up(2), up(3));

% Set background color to 'black' (the 'clear' color)
glClearColor(0, 0, 0.5, 0);

% Clear out the backbuffer
glClear;

glLoadIdentity;
% End the OpenGL context now that we have finished setting things up
Screen('EndOpenGL', obj.Screen.window);

% Get a time stamp with a flip
Screen('AsyncFlipBegin', obj.Screen.window);
