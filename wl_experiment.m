classdef (Abstract) wl_experiment < handle
% The Experiment Base is an abstract class (template/skeleton) that
% should be extended every time an experiment is written. The experiment
% class holds wrappers to all (almost all, TODO: Wrap all) wl functions, wl

    properties
        Timer; % Attribute used to store timers (i.e. wl_timer and wl_flip_timer)
        cfg;   % cfg is the attribute which holds the data specified in the config files
        Robot; % Attribute that contains a robot or robot symmulation object (wl_robot, wl_mouse)
        State; % Keeps track of current, next and previous state and general state information
        Hardware; % Attribute that encapsulates the wl_hardware object
        Screen;  % Screen data from psychtoolbox(window, rect, etc )
        Trial; % Current Trial (Current trial row from TrialData, TODO: Double check)
        TrialNumber; % Current Trial Number
        TrialData; % Table specifying each Trial and relevant per trial variables 
        GUI; % Place holder for the GUI
        GW; % Place to store miscellanous variables for the experiment paradigm being implemented
        StimulusTime;
        cmd_line; % Comand line flag (might be removed), set to false to ignore GUI
    end
    
    methods
    % here is where the wrapper methods to WL functions go
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function this  = wl_experiment
        % Constructor setting up path and initialisation
            if ispc
                % TODO: Migrate this to an installation script
                addpath('U:\wolpert\catstruct');
                addpath(genpath('S:\matlab\myfunc'));
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function out=initialise(this)
            out = wl_initialise(this);
        end
        
        function main_loop(this, length)
            if nargin < 2
                length = inf;
            end
            wl_main_loop(this, length);
        end

        function hware = hardware(this, robot)
            hware = wl_hardware(this, robot);
        end
        
        function out=cm2pix(this,in)
            out = wl_cm2pix(this, in);
        end
        
        function out=pix2cm(this, in)
            out = wl_pix2cm(this ,in);
        end
        
        function error_resume(this)
            wl_error_resume(this);
        end
        
        function error_state(this,msg, state)
            wl_error_state(this, msg, state)
        end

        function out = is_error(this)
            out = wl_is_error(this);
        end
        
        function out = trial_save(this)
            out = wl_trial_save(this);
        end
        
        function draw_tea_pot(this)
            wl_draw_tea_pot(this);
        end
        
        function draw_text(this, txt, x, y, varargin)
            wl_draw_text(this, txt, x, y, varargin{:});
        end
        
        function start_screen(this, w)
            wl_start_screen(this, w);
        end
        
        function mouse_ = mouse(this, robot1, robot2)
            if nargin < 3
                mouse_ = wl_mouse(this, robot1);
            else
                mouse_ = wl_mouse(this, robot1, robot2);
            end
        end
        
        function out = robot_stationary(this)
            out = wl_robot_stationary(this);
        end
        
        function P = load_audio(this, fname)
            P = wl_load_audio(this, fname);
        end
        
        function play_sound(this,P,vol)
            if nargin==2
                vol=this.cfg.vol;
            end
            wl_play_sound(this,P,vol);
        end
        
        function state_init(this, varargin)
            wl_state_init(this, varargin{:});
        end
        
        function state_next(this, state)
            wl_state_next(this, state);
        end
        
        function timer_results(this)
            wl_timer_results(this);
        end
        
        function R = parse_trials(this, X)
            R = wl_parse_trials(this, X, inputname(2));
        end
        
        function trial_setup(this)
            wl_trial_setup(this);
        end
        
        function trial_abort(this)
            wl_trial_abort(this);
        end
        
        function out=trial_next(this)
            out = wl_trial_next(this);
        end
        
        function trial_start(this)
            trial_start(this);
        end
        
        function trial_stop(this)
            wl_trial_stop(this)
        end
        
        function trial_close(this)
            wl_trial_close(this);
        end
        
        function overide_cfg_defaults(this) % TODO: override, TODO: think about placement
            % This method overrides the fields that overlap between
            % this.cfg and this.GW. 
            % Paramters that are set by the GUI will be overriden in the
            % cfg and GW attributes after this method is called.
            k1 = fields(this.cfg);
            k2 = fields(this.GW);
            inter = intersect(k1, k2);
            for i=1:length(inter)
                key = inter{i};
                this.cfg.(key) = this.GW.(key); 
            end
        end
        
    end

    methods (Static)
        
        function robot_ = robot(robot1, robot2)
            if nargin < 2
                robot_ = wl_robot(robot1);
            else
                robot_ = wl_robot(robot1, robot2);
            end
        end
        
        function out=load_beeps(freqs, dur)
            out= wl_load_beeps(freqs, dur);
        end
    end

    methods (Abstract)
    % Minimal methods required for an experiemnt that the Student must
    % implement in order to have the most minimal experiment going

        run(this); % Method used to setup and run the experiment
        myinitialise(this); % usually for setting up the FSM
        idle_func(this); % Function which is called at 1kHz in wl_main_loop
        display_func(this); % Function with gl and pychtoolbox drawing instruction
        StateProcess(this); % Function which implments the FSM and advances to the next state
        TrialStart(this); % sets up before each trial
        MissTrial(this); % procedure to be called when a trial is missed

    end

end