

This is the most complex config arrangement I have. Part of the reason it is so complex is because I didn't solve how to implement it very well, so it actually isn't as fully randomised as would have been ideal. Attached are the configs - look to FT-LeadinDecay.cfg first to understand how it is all pieced together.

The files live: /hrs40/public/Experiments/MemoryDecay/CombinedLeadin/Balance

The experiment is composed of the following phases in order:

1. Null phase
2. Exposure 1 phase (particular trial structure)
3. Exposure 2 phase (a different particular trial structure)
4. Decay phase
5. Null phase

The decay phase is complicated. It is composed of 6 different probe block conditions 'FT-ProbeBlock1x', 'FT-ProbeBlock2x', 'FT-ProbeBlock3x' etc. Each of these different probe block conditions have 4 different versions, because we wanted to make sure that the order of the two trials at the start and end of each ProbeBlock condition had a permuted order (one version will have trial order ABAB, another will have ABBA, another BAAB, another BABA). We needed to make sure that each of these versions occur for each condition. The version of each probe block condition is indicated by a letter after the probe block number in its title e.g. FT-ProbeBlockXa, FT-ProbeBlockXb etc. 

Because the decay phase is long and stretched over several rest breaks, I wanted to make sure that each of the 6 probe block conditions were experienced in-between each rest break, but in a random order. It did not matter that each of the versions for each condition were experienced next to each other in the experiment (in fact it would have been preferable to have the versions mixed randomly across the decay phase, but I didn't know how to do this part of the randomisation, so I kept the version fixed in between each rest). 

Between each probe block there is a set of re-exposure trials which varies slightly in length to stop it being predictable in duration each time it happens. These are the FT-Exposure26.cfg, etc files where the number at the end of the file name describes the number of trials experienced. 

Hope that is clear. Any questions come ask me!

Hannah




