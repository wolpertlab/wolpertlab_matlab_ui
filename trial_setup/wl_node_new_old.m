classdef wl_node < handle & matlab.mixin.Heterogeneous % mixin.Heterogeneous allows for subclass polymorphism (Placing objects with a common super type in the same array/iterable)
    properties (Access = public)
        
        children; % vector list of children of type wl_node whch can be either a node or a leaf
        visited_flag; % boolean as whether wl_node has been visited
        % this is initialsed to false and resets to false when all children
        % have been visited if reset_flag is true - only used by sample
        
        mode; %  fixed or sample
        % fixed visits the children in the fixed order left to right and
        % uses fixed_child_index to know which one to visit next
        %
        % sample visits stochastically with or without replacement and has an
        % associated  probability distribution for sampling which is
        % uniform by default.
        %
        % It can also have a maximum number of visits to each child
        % specified by sample_max_count (which is by default infinite)
        % After exhauting all max_count visits to the children it can reset
        % or not (depending on reset_flag) and if it does not reset it will
        % no longer visit the children and therefore not return leaves.
        
        sample_distribution; % Discrete probability distribution for sampling children
        % This can be specified as any positive numbers as will be normalised to sum to 1
        % Has same number of elements as number of children
        
        sample_replace_flag; % boolean variable for sampling with or without replacements (default false)
        
        sample_exaust_flag; % boolean flag indicating that once you sample one of this node's children
        % you need to visit all its children until exhausted before sampling the next child
        sample_exaust_index; % current child that need to be exausted before moving one
        
        sample_max_count; % List of the number of times that each child should be visited (see mode)
        sample_max_count_tmp; % temporary variable to implement max_count - this will be decremented each visit
        
        fixed_all_flag; % boolean flag (default false) which is true means you need to  visit each child once
        fixed_child_index; % index as to which child to visit next
        
        reset_flag; % boolean flag indicating once the direct children of a node have each been visited
        % i.e. once for fixed, or sample_max_count for% sampling with/without replacement,
        %or all exhausted for sampling without replacement
    end
    
    methods (Access = public)
        function this = wl_node(children, sample_distribution, ...
                mode, thresholds)
            % WL_NODE constructor for wl_node object used to  generate trials in an experiment by arranging nodes and leaves
            % children              list of childen specified as an array of nodes
            % sample_distribution   list of probabilities for each child if sampling
            % mode                  'sample' or 'fixed'
            % thresholds            integer list of how many times we sample each child
            %
            % A wl_node has a list of children which themselves
            % are also of type wl_node.
            %
            % A special sort of node is a leaf which has no children and has
            % its own constructor
            %
            % This function is not used directly but the end user
            % A totally minimal usage would be:
            %
            % A = wl_leaf(table({'pre'}), [], [], 0);
            % B = wl_leaf(table({'exposure'}), [], [], 0);
            % C = wl_node([A, B], [], 'sample');
            %
            %             yields the tree:
            %                    C
            %                   / \
            %                  A   B
            %
            
            % set defaults for other parameters of the node
            
            this.children = children;
            this.fixed_child_index = 1;
            this.reset_flag = true;
            this.mode = mode;
            this.sample_exaust_flag = false;
            this.sample_exaust_index = -1;
            this.visited_flag = false;
            this.sample_replace_flag = false;
            this.fixed_all_flag = false;
            
            if isempty(this.sample_distribution)
                this.sample_distribution = ones(1, length(children))./length(children);
            else   %normalise the sample_distribution to sum to 1
                this.sample_distribution = sample_distribution ./ sum(sample_distribution);
            end
            
            if nargin < 5  % allow as many call as you like
                thresholds = inf(1,length(children));
            end
            
            this.sample_max_count = thresholds;
            this.sample_max_count_tmp = thresholds;
        end
    end
    
    methods ( Access = public)
        
        function new_obj = plus(obj1, obj2)
            %PLUS operator (+) adds two wl_nodes (trees) together
            % For example:
            %
            % If A and B are leaves and C is a sample node
            % i.e  C = wl_node.sample([A, B]);
            %     then D = C + A  gives
            %
            %                    C          A             D
            %                   / \    +        =        / \
            %                  A   B                    C   A
            %                                          / \
            %                                         A   B
            %
            % When adding 'fixed' wl_nodes the plus operatore flattens the children:
            %
            % C = A + B;
            % D = C + A
            %
            %            C + A   =       D
            %           / \            / | \
            %          A   B          A  B  A
            %
            % Rest breaks is specfied by adding the character 'r'
            % this creates a new leaf:
            %
            %        F = A + 'r' =     F
            %                        /   \
            %                       A    'r'
            %
            
            if lower(obj1) == 'r'
                % Rest break is an empty leaf.
                new_obj = plus(wl_leaf([],[],[],0), obj2);
                return;
            end
            if lower(obj2) == 'r'
                % Rest break is an empty leaf.
                new_obj = plus(obj1, wl_leaf([],[],[],0));
                return;
            end
            
            % flatten and combine children into new list
            childrn = [wl_node.flatten_node(obj1) wl_node.flatten_node(obj2)];
            
            % creat new node
            new_obj = wl_node(childrn, [], 'fixed');
        end
        
        function obj_list = times(c,obj)
            %TIMES overrides the .* opeator to repeat a node c times 
            % B = 3.*A = [A A A];
            % not this returns a list of nodes which can then be made fixed or
            % sampled
            if ~isfloat(c)
                error('First argument of .* operator must be a scalar');
            end
            
            obj_list = [];
            for i=1:c
                obj_list = [obj_list obj];
            end
        end
        
        function new_obj=mtimes(c, obj)
            %MTIMES overrides * operator. 
            % This adds a wl_node to itself c times
            % where one of the arguments is a
            % constant and the other is a wl_node object:
            % B= 3*A = A + A + A
            %     3 * A =    B
            %              / | \
            %             A  A  A
            
            if ~isfloat(c)
                error('First argument of operatr * must be a scalar');
            end
            
            new_obj = obj;
            for i=1:(c-1)
                new_obj = new_obj + obj;
            end
        end
        
        function resolved_tree=parse_tree(this)
            %PARSE_TREE parses a wl_node algebraic expression to generate a
            % trialdata table which has a row for each trial
            % 
            % This is generated by fully parsing the hierarchical structure
            % of the wl_node expresion
            %
            % PARSE_TREE loops over all its direct children and extracts
            % one leaf from each child (except that if any visited node has
            % the fixed_all_flag true then all its children are visited)
            %
            % For PARSE_TREE to work correctly the terminal nodes should be
            % leaves with a non empty table
            %
            % a = wl_leaf(table({'pre'}), [], [], 0);
            % b = wl_leaf(table({'adapt'}), [], [], 0);
            % c = wl_leaf(table({'post'}), [], [], 0);
            % t = parse_tree( 3*a + 2*b + c)
            %
            % block_count    block_index    block_index_count    block_trial    block_original_trial     Var1  
            % ___________    ___________    _________________    ___________    ____________________    _______
            % 1              1              1                    1              1                       'pre'  
            % 2              1              2                    1              1                       'pre'  
            % 3              1              3                    1              1                       'pre'  
            % 4              2              1                    1              1                       'adapt'
            % 5              2              2                    1              1                       'adapt'
            % 6              3              1                    1              1                       'post' 
            %
            % the trialdata will always start with block information:
            % 
            % block_count           - this increments with every block
            % block_index           - this is the block identifier
            % block_index_count     - this counts how many time the particular
            %                           block (specified by block_index) has been
            %                           called
            % block_trial           - this is the trial count within a block
            % block_original_trial  - this is the orginal location of the
            %                           trial within a block before any permuting
            %
            % the remaining columns are specified by trial_data in the leaves are sorted alphabetically  

            n = length(this.children);
            resolved_tree = table;
            
            block_index_count = containers.Map('KeyType', 'double', 'ValueType', 'double'); % this counts block_index_count
            block_index = containers.Map('KeyType', 'double', 'ValueType', 'double'); % this counts block_index
            block_index_count_count = 1;
            rest_break = 0;
           
            % Loop over all of 'this' children
            for i=1:n
                leaves = this.dfs_leaves(); % if there is a fixed_all_flag wl_node multiple leaves come out
                for j=1:length(leaves) 
                    leaf = leaves(j);
                    if isempty(leaf.trial_data)
                        resolved_tree(end,:).RestFlag = 1;
                        rest_break = rest_break + 1;
                        continue
                    end
                    
                    if leaf.permute
                        new_indices = wl_constrained_permute(height(leaf.trial_data), ...
                            leaf.position, ...
                            leaf.adjacency);
                    else
                        new_indices = 1:height(leaf.trial_data);
                    end
                    
                    if ~isKey(block_index, leaf.id) 
                        block_index_count(leaf.id) = 1;
                        block_index(leaf.id) = block_index_count_count;
                        block_index_count_count = block_index_count_count + 1;
                    else
                        block_index_count(leaf.id) = block_index_count(leaf.id) + 1;
                    end
                    
                    % block_index is the block identifier 
                    leaf.table = table();
                    leaf.table.block_count = repmat(i + (j - 1) - rest_break, height(leaf.trial_data), 1);
                    leaf.table.block_index = repmat(block_index(leaf.id), height(leaf.trial_data), 1);
                    
                    % block_index_count=n  means this is the nth occurence of this block
                    leaf.table.block_index_count = repmat(block_index_count(leaf.id) ,height(leaf.trial_data), 1);
                    
                    leaf.table.block_trial = (1:height(leaf.trial_data))'; % trial number within a block
                    
                    leaf.table.block_original_trial = new_indices';
                    
                    leaf.table = [leaf.table leaf.trial_data(new_indices, :)];
                    resolved_tree = [resolved_tree; leaf.table];
                end
            end
        end
    end
    
    
    methods( Access = public, Static )
        % The public static methods are the only functions the user 
        %  should need in additon to the operators +, .*, *
        
        function nodes=copy(obj)
            %COPY is a recursive method that takes in a list of wl_node
            % objects and it de-references them recursively (makes a
            % completely new set of wl_nodes with no pointers to the old ones)
            
            if isempty(obj)
                nodes = [];
                return;
            end
            
            for i=1:length(obj)
                str = class(obj(i));
                
                if strcmp(str, 'wl_node')
                    nodes(i) = wl_node(wl_node.copy(obj(i).children), ... % recurive call in wl_node.copy(...)
                                       obj(i).sample_distribution, obj(i).mode, ...
                                       obj(i).sample_max_count);
                    nodes(i).sample_replace_flag = obj(i).sample_replace_flag;
                    nodes(i).sample_exaust_flag = obj(i).sample_exaust_flag;
                else
                    nodes(i) = wl_leaf(obj(i).trial_data,...
                                       obj(i).position,...
                                       obj(i).adjacency, obj(i).permute);
                end
            end
        end
        
        function obj = sample(children, varargin)
            %SAMPLE creates a node from which a child will
            % be sampled randomly according to a specified
            % sample_distribution every time this node is visited
            %
            % 
            % D = wl_node.SAMPLE(children_list, 'prob', dist, 'max', ...
            %                      max_num, 'replace_flag', bool);
            %
            % creates a sample node with N children in children_list
            % 'prob'           sets the relative probablity of sampling the children
            %                     to dist (length N). By default is uniform sampling
            % 'max'            sets the maximum number of times (length N) each child can be
            %                     visited. Deafult is infinite
            % 'replace_flag'   sample with or without replacment
            %
            % If A, B and C are leaves 
            % D = wl_node.SAMPLE([A B C], 'prob', [1 1 1], 'max', ...
            %                    [Inf Inf Inf], 'replace_flag', true);
            %
            %                D  (sample)
            %              / | \
            %             B  C  A  (orders change stochastically upon visit)
            %
            % creates a wl_node object with 3 children A, B and C which will
            % be sampled at random with a uniform disitrbution [1 1 1] by
            % an equal number of proportions specified by 'max' , [Inf Inf Inf]
            % and the sampling will occur with sample_replacement

            pSet = inputParser;
            addParameter(pSet, 'prob', ones( 1, length(children)));
            addParameter(pSet,'max', ones( 1, length(children)) * inf);
            addParameter(pSet,'replace', true);
            addParameter(pSet,'exhaust', false);

            parse(pSet,varargin{:});
            sample_distribution = pSet.Results.prob / sum(pSet.Results.prob);
            sample_max_count = pSet.Results.max;
            sample_replace_flag = pSet.Results.replace;
            exhaust = pSet.Results.exhaust;
            if exhaust
                children = wl_node.copy(children);
            end
            obj = wl_node(children, sample_distribution, 'sample', sample_max_count);
            obj.sample_replace_flag = sample_replace_flag;
            obj.sample_exaust_flag = exhaust;
        end
        
        function obj = fixed(children)
            %FIXED creates a fixed wl_node. (Children wil be visited in the
            % order given by the children list.) :
            %
            % D = wl_node.FIXED(children_list);
            %
            % creates a fixed node with N children in children_list 
            %
            % If A, B and C are leaves 
            % D = wl_node.FIXED([A B C]);
            %
            %                D  (fixed)
            %              / | \
            %             A  B  C
            obj = wl_node(children, [], 'fixed');
        end
        
        function obj= fixed_all(children)
            %FIXED_ALL creates a fixed wl_node that when visited will return
            % a leaf for each one of its children, similar to how the root
            % wl_node visit is executed by default.
            %
            % D = wl_node.FIXED_ALL(children_list);
            %
            % creates a fixed node with N children in children_list and
            % fixed_all_flag = true
            %
            % If A, B and C are leaves 
            % D = wl_node.FIXED_ALL([A B C]);
            %
            %                D  (fixed,  fixed_all_flag = true)
            %              / | \
            %             A  B  C
            % D will always return 3 leaves when visited one from A, B and
            % C
            obj = wl_node(children, [], 'fixed');
            obj.fixed_all_flag = true;
        end
        
        function obj = permute(children)
            %PERMUTE creates a node that permutes children by sampling
            % without replacement every time the node is visited
            %
            % D = wl_node.PERMUTE(children_list);
            %
            % creates a sample node with N children in children_list and 
            % sample_replace_flag = true
            %
            % If A, B and C are leaves 
            % D = wl_node.PERMUTE([A B C]);
            %
            %                D  (sample, sample_replace_flag = true)
            %              / | \
            %             B  A  C
            obj = wl_node.sample(children, 'replace', false);
        end
        
    end
    
    
    methods( Access = private )
        
        function reset_tree(this)
            %RESET_TREE Sets fixed_all visited_flag flags to false from
            % this wl_node onwards
            if isempty(this) || ~(this.reset_flag); return; end
            this.visited_flag = false;
            for i=1:length(this.children)
                this.children(i).reset_tree() % recursive call
            end
        end
        
        function index= get_index(this)
            %GET_INDEX returns the index of the next child to visit if
            % there is one. This method checks the mode and updates the
            % state of the wl_nodes it visits accordingly to its mode
            
            index = 0; % returns invalid index if no children left to visit ad reset = false
            if strcmp(this.mode, 'fixed') && ...
                    this.fixed_child_index <= length(this.children) % if fixed
                index = this.fixed_child_index;
                this.fixed_child_index = this.fixed_child_index + 1;
                if  this.fixed_child_index > length(this.children) && this.reset_flag
                    this.fixed_child_index = 1;
                end
                return; % done with 'fixed'
            end
                
            if strcmp(this.mode, 'sample') % else sample
                % regular sample (sample_exaust_flag = false)
                if ~this.sample_exaust_flag
                    alive_children = find(this.sample_max_count_tmp > 0);
                    alive_dist = this.sample_distribution(alive_children);
                    
                    if ~isempty(alive_children)
                        index = this.discrete_sample(alive_children, alive_dist);
                    elseif this.reset_flag
                        this.sample_max_count_tmp = this.sample_max_count;
                        index = this.discrete_sample(1:length(this.children),...
                            this.sample_distribution);
                    end
                    
                    this.sample_max_count_tmp(index) = this.sample_max_count_tmp(index) - 1;
                    if ~this.sample_replace_flag
                        this.sample_max_count_tmp(index) = 0;
                    end
                    return; % done with 'regular sample' 
                end
                
                % If this section is reached it means we are exhaustively
                % sampling (sample_exaust_flag = true)
                if this.sample_exaust_index > 0  && ...
                        ~wl_node.all_visited_list(this.children(this.sample_exaust_index))
                    index = this.sample_exaust_index;
                elseif ~wl_node.all_visited_list(this)
                    vis = wl_node.all_visited_list(this.children);
                    alive_indices = find(~vis); % find un-visited children
                    alive_dist = this.sample_distribution(alive_indices);
                    index = this.discrete_sample(alive_indices, alive_dist);
                else
                    this.reset_tree();
                    index = this.discrete_sample(1:length(this.children),...
                                                 this.sample_distribution);
                end
                this.sample_exaust_index = index;
                
                return; % done with 'exhuastive sample' 
            end

            % If we get here, it's a mode we don't know about.
            error('%s is not a valid mode for wl_node', this.mode);
        end
        
      
        
        function resolved_leaf = dfs_leaves(this)
            %DFS_LEAVES performs a single iteration of an in-order like
            % traveral on the wl_node (this) returning one  single leaf uppon
            % each cfixed_all
            % DFS = "Depth First Search", returns the leaves of the deepest node
            resolved_leaf = [];
            cur_node = this;
            while ~isempty(cur_node.children)
                
                % Quick condition check for fixed_all nodes
                if cur_node.fixed_all_flag % this is very ugly TODO: fix somehow
                    % loop over children of fixed all node visiting each once
                    for i=1:length(cur_node.children)
                        if  isa(cur_node.children(i), 'wl_leaf')
                            leaf = cur_node.children(i);
                        else
                            leaf = dfs_leaves(cur_node.children(i));
                        end
                        resolved_leaf = [resolved_leaf leaf]; %#ok<AGROW>
                        resolved_leaf(i).visited_flag = true;
                    end
                    return;
                end
                
                cur_node.visited_flag = true;
                child_index = cur_node.get_index(); % get index ustilises wl_node criterion to select child
                % if index is not valid (out of bounds) return nothing (only happens with reset_flag = false)
                if child_index == 0 
                    break; 
                end 
                
                cur_node = cur_node.children(child_index);
                
                if isa(cur_node, 'wl_leaf') % This stopping condition is weaker than checking for empty list
                    resolved_leaf = cur_node;
                    resolved_leaf.visited_flag = true;
                end
            end
        end
        
    end
    
    methods( Static, Access = private)
        % Private Auxiliary/helper methods
        
        function out = all_visited_list(lst)
            %ALL_VISITED used by sample only when sample_exhaust_flag = true
            % this method takes in a list (LST) of nodes and returns a list
            % of boolean values corresponding to each node. For a given
            % node the return value is true if every node below it has been
            % visited, in other words it returns true if a node has been
            % exhausted.
            if isempty(lst)
                out = true;
                return;
            end
            
            out = [];
            for j=1:length(lst)
                if ~lst(j).visited_flag
                    out(j) = false;
                    continue;
                end

                bools=[];
                for i=1:length(lst(j).children)
                    bools(i) = wl_node.all_visited_list(lst(j).children(i)); % recursive call
                end
                out(j) = all(bools);
            end
        end

        function childrn=flatten_node(obj)
            %FLATTEN_NODE auxiliary static method which 'flattens' a wl_node
            % if it is fixed otherwise it just returns obj. By flattening a
            % wl_node it means it returns a list of its children (which is
            % somewhat like removing that wl_node as a hierarchy).
            if strcmp(obj.mode, 'fixed') && ~isempty(obj.children)
                childrn = obj.children;
            else
                childrn = obj;
            end
        end
              
        function samp =  discrete_sample(A, p)
            %DISCRETE_SAMPLE takes in a vector and a corresponding vector
            % of probabilities returns a sample from A with a categorical 
            % distribution on p. 
            p = p / sum(p);
            samp = A(find(mnrnd(1, p))); %#ok<FNDSB>
            samp = samp(1);
        end
        
    end
    
end