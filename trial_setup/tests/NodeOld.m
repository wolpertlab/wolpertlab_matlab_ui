classdef NodeOld < handle
    properties (Access = public)
        children;
        n;
        data;
        order;
        distribution;
        isVisited;
        child_pointer;
        mode;
        returns;
        q;
        max_;
        max_tmp;
        reset;
    end
    
    methods (Access = public)
        function this = NodeOld(children, data, order, distribution, mode)
            this.children = children;
            this.child_pointer = 1;
            this.n = length(children);
            this.data = data;
            this.order = order;
            this.distribution = distribution;
            this.isVisited = false;
            this.mode = mode;

            if strcmp(this.mode, 'permutation')
                this.children = this.children(randperm(length(this.children)));
            end
            this.q = DoubleLinkedList(); % Making the deque an attribute to
                                         % preserve state
            this.q.insert_tail(this);
        end
        
        function r=plus(obj1, obj2)
            %
            childrn = [obj1, obj2]; % It may not be a good idea to override
                                    % concatenation I need to be able to
                                    % append objects to a list of children
            r = NodeOld(childrn, 'X', 'all', [], 'fixed');
        end
        
        function reset(this)
            if strcmp(this.mode, 'permutation')
                this.children = this.children(randperm(length(this.children)));
            end
            for i=1:length(this.children)
                this.children(i).reset()
            end
        end
        
        function resolved_tree=parse_tree(this)
            resolved_tree = [];
            iteration = 1;
            while this.returns ~= 0
                resolved_tree = [resolved_tree this.bfs_intercalated2(iteration)];
                iteration = iteration + 1;
                if this.q.is_empty()
                    % Reseting
                    this.q.insert_tail(this);
                    this.reset();
                end
            end
            resolved_tree
        end
        
        function resolved_tree = bfs_intercalated2(this, iteration)
            
            resolved_tree = [];
            while ~this.q.is_empty() && this.returns ~= 0
                
                cur_node = this.q.pop_head();
                if strcmp(cur_node.order, 'one') % add more cases for other iters
                    if cur_node.child_pointer <= length(cur_node.children) * iteration
                        cur_child = cur_node.children(mod(cur_node.child_pointer - 1,length(cur_node.children) ) + 1);
                        cur_node.child_pointer = cur_node.child_pointer + 1;
                        cur_child.isVisited = true;
                        this.q.insert_tail(cur_node);
                        this.q.insert_head(cur_child);
                    end
                elseif strcmp(cur_node.order, 'all')
                    for i=1:length(cur_node.children)
                        child = cur_node.children(i);
                        if ~child.isVisited
                            this.q.insert_tail(child);
                        end
                    end
                end
                
                if isempty(cur_node.children)
                    resolved_tree = [ resolved_tree cur_node.data];
                    this.returns = this.returns - 1;
                end
            end
        end
        
        function index= get_index(child)
            if isempty(strfind(child.mode, 'sample'))
                index = mod(cur_node.child_pointer - 1,length(cur_node.children) ) + 1;
            else
                index = 0; % sample here
            end
        end
        
        function resolved_leaf = bfs_one(this, iteration)
            
            resolved_leaf = [];
            cur_node = this;
            while true % Danger. But a leaf shoud always be reached

                if cur_node.child_pointer <= length(cur_node.children) * iteration
                    child_index = this.get_index(cur_node);
                    cur_child = cur_node.children(child_index); % get index ustilises node criterion to select child
                    
                    cur_node.child_pointer = cur_node.child_pointer + 1;
                    cur_node = cur_child;
                end
                
                if isempty(cur_node.children)
                    resolved_leaf = cur_node.data;
                    break;
                end
            end
        end
        
        function bfs_intercalated(this)
            q = DoubleLinkedList();
            q.insert_tail(this);
            resolved_tree = [];
            while ~q.is_empty()
                
                cur_node = q.pop_head().data;
                if ~strcmp(cur_node.order, 'fixed') % add more cases for other iters
                    if cur_node.child_pointer <= length(cur_node.children)
                        cur_child = cur_node.children(cur_node.child_pointer);
                        cur_node.child_pointer = cur_node.child_pointer + 1;
                        cur_child.isVisited = true;
                        q.insert_tail(cur_node);
                        q.insert_tail(cur_child);
                    end
                else
                    for i=1:length(cur_node.children)
                        child = cur_node.children(i);
                        if ~child.isVisited
                            q.insert_tail(child);
                        end
                    end
                end
                
                if isempty(cur_node.children)
                    resolved_tree = [ resolved_tree cur_node.data];
                end
            end
            resolved_tree
        end
        
        function bfs(this)
            q = DoubleLinkedList();
            q.insert_tail(this);
            while ~q.is_empty()
                cur_node = q.pop_head();
                cur_node_children = cur_node.children;
                for i=1:length(cur_node_children)
                    child = cur_node_children(i);
                    q.insert_tail(child);
                    if isempty(child.children)
                        child.data
                    end
                end
            end
        end
        
    end
end