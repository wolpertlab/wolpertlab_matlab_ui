obj = dynamics;
cfg_name = 'trial_parse_test';

obj.cfg.TargetDistance=10;

obj.cfg.HomePosition = [0 0 0]';

%%
% specify items that can change across trials
% as indexed structure or indexed cell array

% Null field.
Field{1}.Name		= 'null';
Field{1}.FieldType		= 0;
Field{1}.FieldConstants	= [0 0];
Field{1}.FieldAngle	=	0.0;
Field{1}.FieldMatrix	=	eye(3);

% Viscous curl field
Field{2}.Name		= 'curl';
Field{2}.FieldType		= 1;
Field{2}.FieldConstants	= [0.2 0];
Field{2}.FieldAngle	=	90.0;
Field{2}.FieldMatrix	=	eye(3);

% Channel trial
Field{3}.Name		= 'channel';
Field{3}.FieldType		= 2;
Field{3}.FieldConstants	= [-30.000  -0.05];
Field{3}.FieldAngle	=	0.0;
Field{3}.FieldMatrix	=	eye(3);

TargetAngle=num2cell(linspace(0,2*pi,5));

Test{1}=eye(3);
Test{2}=eye(3);

%add these variables to the global workspace

obj.cfg.Field=Field;
obj.cfg.TargetAngle=TargetAngle;
obj.cfg.Test=Test;

%%

% create a block based on the indices above - name must match 
% Trial sub-structure are variables that change within a block
% Block sub-structure are variables that are fixed within a block


PreExposure.Trial.Field =		    [ 1 ];
PreExposure.Trial.TargetAngle =		[ 1 ];
PreExposure.Trial.Test =		    [ 1 ];

PreExposure.Block.Phase = 5;
PreExposure.Permute = true; %whether to permute within block - default is no

PreExposure.Location = sparse(1,1);

PreExposure.Adjacency = sparse(1,1);

Exposure.Trial.Field =		        [2];
Exposure.Trial.TargetAngle =		[1];
Exposure.Permute = false;
Exposure.Block.Phase = 2;
Exposure.Trial.Test =		[1];
PostExposure.Trial.Field =		        [1];
PostExposure.Trial.TargetAngle =		[1];

PostExposure.Permute = true; 
PostExposure.Block.Phase = 22;

PostExposure.Trial.Test =		[ 1];

%%
%prepare elemental blocks
A=obj.parse_trials(PreExposure);
B=obj.parse_trials(Exposure);
C=obj.parse_trials(PostExposure);


W1 = A + B; % this is a batch and iterates in fixed order
W2 = Node.sample_block([A B], 'prob', [1 1], 'max', [Inf Inf], 'replace', true); % this is a superbatch that draws one sample each time called
W3 = Node.permute_block([A B]); % Block that randomizes order  upon instantiation

Wx = Node.permute_block([A^100 B^100]);

prop = [4 1];
W2 = Node.sample_block([A B], 'prob', [30 100], 'max', prop); % this is a superbatch that draws one sample each time called
W2 = Node.sample_block([A B], 'prob', [1 100], 'max', [1 100]); % this is a superbatch that draws one sample each time called
histo = [];
for i=1:1000
    T=101*W2;
    table1 = T.parse_tree();
    histo = [histo find([table1.blockshort{:}] == 'A') ];
    xx = table1.blockshort;

    a=unique(xx,'stable');
    b=cellfun(@(x) sum(ismember(xx,x)),a,'un',0);
    b=cat(1,b{:})';
    %isequal(b,prop);
end