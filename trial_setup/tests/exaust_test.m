clear h
obj = dynamics;
cfg_name = 'trial_parse_test';

obj.cfg.TargetDistance=10;

obj.cfg.HomePosition = [0 0 0]';

%%
% specify items that can change across trials
% as indexed structure or indexed cell array

% Null field.
Field{1}.Name		= 'null';
Field{1}.FieldType		= 0;
Field{1}.FieldConstants	= [0 0];
Field{1}.FieldAngle	=	0.0;
Field{1}.FieldMatrix	=	eye(3);

% Viscous curl field
Field{2}.Name		= 'curl';
Field{2}.FieldType		= 1;
Field{2}.FieldConstants	= [0.2 0];
Field{2}.FieldAngle	=	90.0;
Field{2}.FieldMatrix	=	eye(3);

% Channel trial
Field{3}.Name		= 'channel';
Field{3}.FieldType		= 2;
Field{3}.FieldConstants	= [-30.000  -0.05];
Field{3}.FieldAngle	=	0.0;
Field{3}.FieldMatrix	=	eye(3);

TargetAngle=num2cell(linspace(0,2*pi,5));

Test{1}=eye(3);
Test{2}=eye(3);

%add these variables to the global workspace

obj.cfg.Field=Field;
obj.cfg.TargetAngle=TargetAngle;
obj.cfg.Test=Test;

%%

% create a block based on the indices above - name must match 
% Trial sub-structure are variables that change within a block
% Block sub-structure are variables that are fixed within a block

PreExposure.Trial.Field = 1;
PreExposure.Trial.TargetAngle =	1;
PreExposure.Trial.Test = 1;
PreExposure.Block.Phase = 5;
PreExposure.Permute = true; %whether to permute within block - default is no
PreExposure.Location = sparse(1,1);
PreExposure.Adjacency = sparse(1,1);

Exposure.Trial.Field = 2;
Exposure.Trial.TargetAngle = 1;
Exposure.Permute = false;
Exposure.Block.Phase = 2;
Exposure.Trial.Test = 1;

PostExposure.Trial.Field = 1;
PostExposure.Trial.TargetAngle = 1;
PostExposure.Permute = true; 
PostExposure.Block.Phase = 22;
PostExposure.Trial.Test = 1;

MidExposure.Trial.Field = 1;
MidExposure.Trial.TargetAngle =	1;
MidExposure.Permute = true; 
MidExposure.Block.Phase = 3;
MidExposure.Trial.Test = 1;

%%
% Creating the T1, ..., Tn leaves to be permuted (L1, .., L4)
L1 = obj.parse_trials(PreExposure);
L2 = obj.parse_trials(Exposure);
L3 = obj.parse_trials(PostExposure);
L4 = obj.parse_trials(MidExposure);

L = [L1 L2 L3 L4];

p = perms(1:4);
% Loop over all permutations creating a parent wl_node for each permutation
for k=1:rows(p)
   % Parent wl_node C(k) with copies of permuted leaves as children
   C(k) = wl_node.fixed( L(p(k,:)) );
end

% Variable to sandwich in between calls from A
D = obj.parse_trials(MidExposure);
% Running same experiment 10000 times to check the distribution is uniform
k = 10000;
for i=1:k

    A = wl_node.sample(C, 'replace', false, 'exhaust', true );

    % Get all permutations from A intercalated with Ds
    B = rows(p) * 4 * (A + D);
    T = B.parse_tree();
    % Reshaping to obtain the parsed 'randomised' permutation blocks 
    % (we also filter out the D's since we are just concerned with the
    % correctness of the perutations
    x = reshape( T.block_short(~strcmp(T.block_short, 'D')), 4, 24)';
    for j=1:24
        ith = cellfun(@(y) str2double(y(2)), x(j,:));
        [~, index] = ismember(ith, p, 'rows');
        h(i,j) = index;
    end
    % Mapping resulting parsing to array of strings [ 'L1L2L3L4', ...
    % 'L2L1L4L3', ...] in order to check
    s = string(horzcat(char(x(:,1)), char(x(:,2) ), char(x(:,3)), char(x(:,4))));
    % Filtering out possible duplicates (if there are duplicates thats a
    % bug which we check for)
    u = unique(s);
    % Checking that all permutations were attained
    if length(u) ~= 24
        error('Ooops Exhaust Failed since there are duplicates');
    end
end

% For debugging purposes: checking the random shuffling of permutations is
% uniformly distributed
if k > 10
   for i=1:24
       subplot(4,6,i)
       hist(h(:,i), 24)
   end
end

