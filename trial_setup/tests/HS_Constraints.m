clear all

obj = dynamics;
cfg_name = 'trial_parse_test';

obj.cfg.TargetDistance=10;

obj.cfg.HomePosition = [0 0 0]';

%%
% specify items that can change across trials
% as indexed structure or indexed cell array

% Null field.
Field{1}.Name		= 'null';
Field{1}.FieldType		= 0;
Field{1}.FieldConstants	= [0 0];
Field{1}.FieldAngle	=	0.0;
Field{1}.FieldMatrix	=	eye(3);

% Viscous curl field
Field{2}.Name		= 'curl';
Field{2}.FieldType		= 1;
Field{2}.FieldConstants	= [0.2 0];
Field{2}.FieldAngle	=	90.0;
Field{2}.FieldMatrix	=	eye(3);

% Channel trial
Field{3}.Name		= 'channel';
Field{3}.FieldType		= 2;
Field{3}.FieldConstants	= [-30.000  -0.05];
Field{3}.FieldAngle	=	0.0;
Field{3}.FieldMatrix	=	eye(3);

TargetAngle=num2cell(linspace(0,2*pi,5));

BlockVars{1}.Test1 = 1;
BlockVars{1}.Test2 = 2;
BlockVars{1}.Test3 = 3;

BlockVars{2}.Test1 = 10;
BlockVars{2}.Test2 = 20;
BlockVars{2}.Test3 = 30;

BlockVars{3}.Test1 = 100;
BlockVars{3}.Test2 = 200;
BlockVars{3}.Test3 = 300;

Test{1}=eye(3);
Test{2}=eye(3);

%add these variables to the global workspace

obj.cfg.Field=Field;
obj.cfg.TargetAngle=TargetAngle;
obj.cfg.Test=Test;

%%
FT_ProbeBlock3.Trial.FieldIndex = [ 48,48,48,48,48,48,48,48,4,4 ];
FT_ProbeBlock3.Permute = true;
FT_ProbeBlock3.Location = sparse(10,10);

FT_ProbeBlock3.Location(9,10) = 1;
FT_ProbeBlock3.Location(9,9) = 1;
FT_ProbeBlock3.Location(9,8) = 1;
FT_ProbeBlock3.Location(9,7) = 1;
FT_ProbeBlock3.Location(9,6) = 1;
FT_ProbeBlock3.Location(9,1) = 1;

FT_ProbeBlock3.Location(10,10) = 1;
FT_ProbeBlock3.Location(10,5) = 1;
FT_ProbeBlock3.Location(10,4) = 1;
FT_ProbeBlock3.Location(10,3) = 1;
FT_ProbeBlock3.Location(10,2) = 1;
FT_ProbeBlock3.Location(10,1) = 1;

FT_ProbeBlock3.Adjacency = sparse(10,10);
FT_ProbeBlock3.Adjacency(9,10) = 1;

%%

P3 = obj.parse_trials(FT_ProbeBlock3);
C = Node.fixed([P3]);
k = 10000;
h48 = [];
h4 = [];
for i=1:k
    t = C.parse_tree().FieldIndex;
    h48 = [ h48 find(t==48) ];
    h4 = [ h4 find(t==4) ];
end

subplot(2,1,1)
hist(h48(:),10);
title('48');

subplot(2,1,2)
hist(h4(:),1:10);
title('4');
