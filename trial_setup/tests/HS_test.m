clear all;

obj = dynamics;
cfg_name = 'trial_parse_test';

obj.cfg.TargetDistance=10;

obj.cfg.HomePosition = [0 0 0]';

%%
% specify items that can change across trials
% as indexed structure or indexed cell array

% Null field.
Field{1}.Name		= 'null';
Field{1}.FieldType		= 0;
Field{1}.FieldConstants	= [0 0];
Field{1}.FieldAngle	=	0.0;
Field{1}.FieldMatrix	=	eye(3);

% Viscous curl field
Field{2}.Name		= 'curl';
Field{2}.FieldType		= 1;
Field{2}.FieldConstants	= [0.2 0];
Field{2}.FieldAngle	=	90.0;
Field{2}.FieldMatrix	=	eye(3);

% Channel trial
Field{3}.Name		= 'channel';
Field{3}.FieldType		= 2;
Field{3}.FieldConstants	= [-30.000  -0.05];
Field{3}.FieldAngle	=	0.0;
Field{3}.FieldMatrix	=	eye(3);

TargetAngle=num2cell(linspace(0,2*pi,5));

Test{1}=eye(3);
Test{2}=eye(3);

%add these variables to the global workspace

obj.cfg.Field=Field;
obj.cfg.TargetAngle=TargetAngle;
obj.cfg.Test=Test;

%%

% create a block based on the indices above - name must match 
% Trial sub-structure are variables that change within a block
% Block sub-structure are variables that are fixed within a block


PreExposure.Trial.Field = 1;
PreExposure.Trial.TargetAngle =	1;
PreExposure.Trial.Test = 1;
PreExposure.Block.Phase = 5;
PreExposure.Permute = true; %whether to permute within block - default is no
PreExposure.Location = sparse(1,1);
PreExposure.Adjacency = sparse(1,1);

Exposure.Trial.Field = 2;
Exposure.Trial.TargetAngle = 1;
Exposure.Permute = false;
Exposure.Block.Phase = 2;
Exposure.Trial.Test = 1;

PostExposure.Trial.Field = 1;
PostExposure.Trial.TargetAngle = 1;
PostExposure.Permute = true; 
PostExposure.Block.Phase = 22;
PostExposure.Trial.Test = 1;

MidExposure.Trial.Field = 1;
MidExposure.Trial.TargetAngle =	1;
MidExposure.Permute = true; 
MidExposure.Block.Phase = 3;
MidExposure.Trial.Test = 1;

%%
A = obj.parse_trials(PreExposure);
B = obj.parse_trials(Exposure);
C = obj.parse_trials(PostExposure);
D = obj.parse_trials(MidExposure);

E1 = obj.parse_trials(MidExposure);
E2 = obj.parse_trials(MidExposure);
E3 = obj.parse_trials(MidExposure);
E4 = obj.parse_trials(MidExposure);
E = Node.sample_block([E1 2*E2 2*E3 E4], 'replace', false);

F1 = obj.parse_trials(MidExposure);
F2 = obj.parse_trials(MidExposure);
F3 = obj.parse_trials(MidExposure);
F4 = obj.parse_trials(MidExposure);
F5 = obj.parse_trials(MidExposure);
F6 = obj.parse_trials(MidExposure);
F = Node.sample_block([F1 F2 F3 F4 F5 F6], 'replace', false);

G1 = obj.parse_trials(MidExposure);
G2 = obj.parse_trials(MidExposure);
G3 = obj.parse_trials(MidExposure);
G4 = obj.parse_trials(MidExposure);
G5 = obj.parse_trials(MidExposure);
G6 = obj.parse_trials(MidExposure);
G = Node.sample_block([G1 G2 G3 G4 G5 G6], 'replace', false);

O1 = obj.parse_trials(MidExposure);
O2 = obj.parse_trials(MidExposure);
O3 = obj.parse_trials(MidExposure);
O4 = obj.parse_trials(MidExposure);
O5 = obj.parse_trials(MidExposure);
O6 = obj.parse_trials(MidExposure);
O = Node.sample_block([O1 O2 O3 O4 O5 O6], 'replace', false);

P1 = obj.parse_trials(MidExposure);
P2 = obj.parse_trials(MidExposure);
P3 = obj.parse_trials(MidExposure);
P4 = obj.parse_trials(MidExposure);
P5 = obj.parse_trials(MidExposure);
P6 = obj.parse_trials(MidExposure);
P = Node.sample_block([P1 P2 P3 P4 P5 P6], 'replace', false);


H = 6*B;
I = 10*C;
J = 15*C;
K = 5*D;
L = 4*D;
T = 6*(E+F);
U = 6*(E+G);
V = 6*(E+O);
W = 6*(E+P);

RT = A + H + I + 2*J + C + L + 2*K + T + U + V + W + H;

table1 = RT.parse_tree()

