 function NodeTest(iters1, iters2)
    E = Node([Node([],0,'all',[], 'fixed') Node([],1,'all',[], 'fixed')], 'X', 'one', [], 'fixed');
    leaves = [E];
    for i=2:10
        data = i;

        order = 'fixed';
        distribution = [];
        children = [] ; %[ Node([], data, order, distribution)];
        l = Node(children, data, 'all', distribution, order);
        leaves = [leaves l];
    end

    middleLayer = [];
    x = 0;
    for i=1:4
        data = 'X';
        order = 'one';
        distribution  = [];
        children = [];
        if i < 3
            for j=1:2
                tmp = x + j;
                children = [ children leaves(tmp)];
            end
        else
            for j=1:3
                tmp = x + j;
                children = [ children leaves(tmp)];
            end
        end
        x = tmp;
        middleLayer = [middleLayer Node(children, data, order, distribution, 'fixed')];
    end


    root = Node(middleLayer, 'X', 'one', [], 'fixed');
    root.returns =iters1;
    %root.bfs_intercalated2(1)
    root.parse_tree();
    if nargin > 1
        root.returns = iters2;
        root.parse_tree();
    end

end