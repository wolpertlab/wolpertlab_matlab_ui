clear all

obj = dynamics;
cfg_name = 'trial_parse_test';

obj.cfg.TargetDistance=10;

obj.cfg.HomePosition = [0 0 0]';

%% Field Cell Array Creation
% specify items that can change across trials
% as indexed structure or indexed cell array

% Fields

Field{1}.FieldIndex     = 0;
Field{1}.FieldType		= 0;
Field{1}.FieldConstants	= [ 0 0 ];
Field{1}.FieldAngle     = 0.0;
Field{1}.ContextType    = 0;
Field{1}.TargetAngle    = 45;

Field{2}.FieldIndex     = 1;
Field{2}.FieldType		= 0;
Field{2}.FieldConstants	= [ 0 0 ];
Field{2}.FieldAngle     = 0.0;
Field{2}.ContextType    = 0;
Field{2}.TargetAngle    = -45;

Field{3}.FieldIndex     = 2;
Field{3}.FieldType		= 1;
Field{3}.FieldConstants	= [ 0.150 0 ];
Field{3}.FieldAngle     = 90.0;
Field{3}.ContextType    = 0;
Field{3}.TargetAngle    = 45;

Field{4}.FieldIndex     = 3;
Field{4}.FieldType		= 1;
Field{4}.FieldConstants	= [ 0.150 0 ];
Field{4}.FieldAngle     = -90.0;
Field{4}.ContextType    = 0;
Field{4}.TargetAngle    = -45;

Field{5}.FieldIndex     = 4;
Field{5}.FieldType		= 2;
Field{5}.FieldConstants	= [ -60.000 -0.200 ];
Field{5}.FieldAngle     = 0.0;
Field{5}.ContextType    = 0;
Field{5}.TargetAngle    = 45;

Field{6}.FieldIndex     = 5;
Field{6}.FieldType		= 2;
Field{6}.FieldConstants	= [ -60.000 -0.200 ];
Field{6}.FieldAngle     = 0.0;
Field{6}.ContextType    = 0;
Field{6}.TargetAngle    = -45;

Field{7}.FieldIndex     = 48;
Field{7}.FieldType		= 2;
Field{7}.FieldConstants	= [ -60.000 -0.200 ];
Field{7}.FieldAngle     = 0.0;
Field{7}.ContextType    = 2;
Field{7}.TargetAngle    = 45;

Field{8}.FieldIndex     = 49;
Field{8}.FieldType		= 2;
Field{8}.FieldConstants	= [ -60.000 -0.200 ];
Field{8}.FieldAngle     = 0.0;
Field{8}.ContextType    = 2;
Field{8}.TargetAngle    = -45;

Field{9}.FieldIndex     = 70;
Field{9}.FieldType		= 4;
Field{9}.FieldConstants	= [ -30.000 0.000 ];
Field{9}.FieldAngle     = 0.0;
Field{9}.ContextType    = 9;
Field{9}.TargetAngle    =  0;

% add these variables to the global workspace
obj.cfg.FieldIndex = Field;

%% This scipt creates the Leaves of the experiment

% create a block based on the indices above - name must match 
% Trial sub-structure are variables that change within a block
% Block sub-structure are variables that are fixed within a block

% The following is the null blocks...
% 14 trials
FT_Null.Trial.Index.FieldIndex = [ 1,1,1,1,1,2,2,2,2,2,5,6,7,8 ];
FT_Null.Permute = true;

% The following are exposure blocks...
% 14 trials
FT_Exposure1.Trial.Index.FieldIndex = [ 3,3,3,3,3,4,4,4,4,4,5,6,7,8 ];
FT_Exposure1.Permute = true;

% 40 trials
FT_Exposure2.Trial.Index.FieldIndex = [ 3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,5,6,7,7,7,7,7,7,8,8,8,8,8,8 ];
FT_Exposure2.Permute = true;

% 26 trials
FT_Exposure26.Trial.Index.FieldIndex = [ 4,4,4,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5 ];
FT_Exposure26.Permute = true;

% 28 trials
FT_Exposure28.Trial.Index.FieldIndex = [ 3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4 ];
FT_Exposure28.Permute = true;

% 30 trials
FT_Exposure30.Trial.Index.FieldIndex = [ 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4 ];
FT_Exposure30.Permute = true;

% 32 trials
FT_Exposure32.Trial.Index.FieldIndex = [ 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4 ];
FT_Exposure32.Permute = true;

% The following are probe blocks...
% 10 trials
FT_ProbeBlock1.Trial.Index.FieldIndex = [ 5,5,5,5,5,5,5,5,5,5 ];
FT_ProbeBlock1.Permute = true;

FT_ProbeBlock2.Trial.Index.FieldIndex = [ 6,6,6,6,6,6,6,6,6,6 ];
FT_ProbeBlock2.Permute = true;

FT_ProbeBlock3.Trial.Index.FieldIndex = [ 7,7,7,7,7,7,7,7,5,5 ];
FT_ProbeBlock3.Permute = true;
FT_ProbeBlock3.Location = sparse(10,10);

FT_ProbeBlock3.Location(9,10) = 1;
FT_ProbeBlock3.Location(9,9) = 1;
FT_ProbeBlock3.Location(9,8) = 1;
FT_ProbeBlock3.Location(9,7) = 1;
FT_ProbeBlock3.Location(9,6) = 1;
FT_ProbeBlock3.Location(9,1) = 1;

FT_ProbeBlock3.Location(10,10) = 1;
FT_ProbeBlock3.Location(10,5) = 1;
FT_ProbeBlock3.Location(10,4) = 1;
FT_ProbeBlock3.Location(10,3) = 1;
FT_ProbeBlock3.Location(10,2) = 1;
FT_ProbeBlock3.Location(10,1) = 1;

FT_ProbeBlock3.Adjacency = sparse(10,10);
FT_ProbeBlock3.Location(9,10) = 1;

FT_ProbeBlock4 = FT_ProbeBlock3;
FT_ProbeBlock4.Trial.Index.FieldIndex = [ 8,8,8,8,8,8,8,8,6,6 ];

FT_ProbeBlock5 = FT_ProbeBlock3;
FT_ProbeBlock5.Trial.Index.FieldIndex = [ 9,9,9,9,9,9,9,9,5,5 ];

FT_ProbeBlock6 = FT_ProbeBlock3;
FT_ProbeBlock6.Trial.Index.FieldIndex = [ 9,9,9,9,9,9,9,9,6,6 ];

% Pre-probe and post-probe channel trials.
T1.Trial.Index.FieldIndex = [ 5,6 ];
T1.Permute = false;

T2.Trial.Index.FieldIndex = [ 6,5 ];
T2.Permute = false;

%% Trial Generation Script
B = obj.parse_trials(FT_Null);

C = obj.parse_trials(FT_Exposure1);
D = obj.parse_trials(FT_Exposure2);

E1 = obj.parse_trials(FT_Exposure26);
E2 = obj.parse_trials(FT_Exposure28);
E3 = obj.parse_trials(FT_Exposure30);
E4 = obj.parse_trials(FT_Exposure32);
E = wl_node.permute([E1 2.*E2 2.*E3 E4]);

P(1) = obj.parse_trials(FT_ProbeBlock1);
P(2) = obj.parse_trials(FT_ProbeBlock2);
P(3) = obj.parse_trials(FT_ProbeBlock3);
P(4) = obj.parse_trials(FT_ProbeBlock4);
P(5) = obj.parse_trials(FT_ProbeBlock5);
P(6) = obj.parse_trials(FT_ProbeBlock6);

Ta(1) = obj.parse_trials(T1);
Ta(2) = obj.parse_trials(T2);

cart_prod = allcomb(1:2, 1:2); % all comb is a matlab function, computes cartesian product
for i=1:length(cart_prod)
    Tchildren(i) = wl_node.fixed( Ta(cart_prod(i,:)) );
end

T4 = wl_node.sample(Tchildren, 'replace', false, 'exhaust', true );

for i=1:6
    T(i) = wl_node.copy(T4);
    PP(i) = wl_node.fixed_all([ T(i) P(i) T(i) ]);
end

A = wl_node.permute(PP);

H = 6*B;
I = 9*C;
J = 15*C;

K = 5*D;
L = 4*D;

T = 6 * (E + A);

S = H + I + (2 * ('R' + J)) + 'R' + C + L + (2 * ('R' + K)) + (4 * ('R' + T)) + 'R' + H;

TTT = S.parse_tree;

%% Testing balancing and plots of TrialData:
D = TTT;

figure(10);
clf;

pm = 5;
pn = 1;
pp = 0;

pp = pp + 1;
subplot(pm,pn,pp);
[ ~,~,y ] = unique(D.FieldIndex);
x = 1:length(y);
plot(x,y,'k.');
axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
xy = axis;
xlabel('Trials');
ylabel('Field Index');

pp = pp + 1;
subplot(pm,pn,pp);
y = D.FieldType;
x = 1:length(y);
plot(x,y,'k.');
xlabel('Trials');
ylabel('Field Type');
axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);

pp = pp + 1;
subplot(pm,pn,pp);
y = D.ContextType;
x = 1:length(y);
plot(x,y,'k.');
xlabel('Trials');
ylabel('Context Type');
axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);

pp = pp + 1;
subplot(pm,pn,pp);
y = D.TargetAngle;
x = 1:length(y);
plot(x,y,'k.');
xlabel('Trials');
ylabel('Target Angle (deg)');
axis([ (min(x)-10) (max(x)+10) (min(y)-10) (max(y)+10) ]);

pp = pp + 1;
subplot(pm,pn,pp);
y = D.FieldAngle;
x = 1:length(y);
plot(x,y,'k.');
xlabel('Trials');
ylabel('Field Angle (deg)');
axis([ (min(x)-10) (max(x)+10) (min(y)-10) (max(y)+10) ]);

%%

FileCount = 1; % One subject
n = 1; % One 'subject'

[ ~,~,FieldIndex ] = unique(D.FieldIndex);
FieldIndexList = unique(FieldIndex);
FieldIndexCount = length(FieldIndexList);

FieldType = D.FieldType;
FieldTypeList = unique(FieldType);
FieldTypeCount = length(FieldTypeList);

for i=1:FieldIndexCount
    j = find(FieldIndex == FieldIndexList(i));
    FileFieldIndexCount(n,i) = length(j);
    FileFieldIndexType(n,i) = FieldType(j(1));
end

for i=1:FieldTypeCount
    j = find(FieldType == FieldTypeList(i));
    FileFieldTypeCount(n,i) = length(j);
end

TargetAngle = D.TargetAngle;
TargetAngleList = unique(TargetAngle);
TargetAngleCount = length(TargetAngleList);

for i=1:TargetAngleCount
    j = find(TargetAngle == TargetAngleList(i));
    FileTargetAngleCount(n,i) = length(j);
end

FieldAngle = D.FieldAngle;
FieldAngleList = unique(FieldAngle);
FieldAngleCount = length(FieldAngleList);

for i=1:FieldAngleCount
    j = find(FieldAngle == FieldAngleList(i));
    FileFieldAngleCount(n,i) = length(j);
end

%%

fprintf('\nFieldIndex\n');
for n=1:FileCount
    fprintf('n=%d ',n);
    for i=1:FieldIndexCount
        fprintf('[ %d %4d ]',FieldIndexList(i),FileFieldIndexCount(n,i));
    end
    fprintf('\n');
end

fprintf('\nFieldType\n');
for n=1:FileCount
    fprintf('n=%d ',n);
    for i=1:FieldTypeCount
        fprintf('[ %d %4d ]',FieldTypeList,FileFieldTypeCount(n,i));
    end
    fprintf('\n');
end

fprintf('\nTargetAngle\n');
for n=1:FileCount
    fprintf('n=%d ',n);
    for i=1:TargetAngleCount
        fprintf('[ %3.0f� %4d ]',TargetAngleList(i),FileTargetAngleCount(n,i));
    end
    fprintf('\n');
end

fprintf('\nFieldAngle\n');
for n=1:FileCount
    fprintf('n=%d ',n);
    for i=1:FieldAngleCount
        fprintf('[ %3.0f� %4d ]',FieldAngleList(i),FileFieldAngleCount(n,i));
    end
    fprintf('\n');
end