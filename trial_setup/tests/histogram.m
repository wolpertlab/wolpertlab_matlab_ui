for rep=1:1000 %loop of repetition

    data=[zeros(100,1); 1]; % 100 A and one B for example
    weights=zeros(101,1);  %initlais weights

   for k=1:101    
       s1=sum(data==0);
       s2=sum(data==1);
       weights(data==1)=1;
       weights(data==0)=100/s1;
       weights=weights/sum(weights);

       [y(k),I]= datasample(data,1, 'Replace',false, 'Weights',weights) ;
       data(I)=[];
       weights(I)=[];
   end
   f(rep)=find(y==1);
end

hist(f)