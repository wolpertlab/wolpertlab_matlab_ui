clear all

obj = dynamics;
cfg_name = 'trial_parse_test';

obj.cfg.TargetDistance=10;

obj.cfg.HomePosition = [0 0 0]';

%%
% specify items that can change across trials
% as indexed structure or indexed cell array

% Null field.
Field{1}.Name		= 'null';
Field{1}.FieldType		= 0;
Field{1}.FieldConstants	= [0 0];
Field{1}.FieldAngle	=	0.0;
Field{1}.FieldMatrix	=	eye(3);

% Viscous curl field
Field{2}.Name		= 'curl';
Field{2}.FieldType		= 1;
Field{2}.FieldConstants	= [0.2 0];
Field{2}.FieldAngle	=	90.0;
Field{2}.FieldMatrix	=	eye(3);

% Channel trial
Field{3}.Name		= 'channel';
Field{3}.FieldType		= 2;
Field{3}.FieldConstants	= [-30.000  -0.05];
Field{3}.FieldAngle	=	0.0;
Field{3}.FieldMatrix	=	eye(3);

TargetAngle=num2cell(linspace(0,2*pi,5));

Test{1}=eye(3);
Test{2}=eye(3);

%add these variables to the global workspace

obj.cfg.Field=Field;
obj.cfg.TargetAngle=TargetAngle;
obj.cfg.Test=Test;

%%

% create a block based on the indices above - name must match 
% Trial sub-structure are variables that change within a block
% Block sub-structure are variables that are fixed within a block

PreExposure.Trial.Field = 1;
PreExposure.Trial.TargetAngle =	1;
PreExposure.Trial.Test = 1;
PreExposure.Block.Phase = 5;


%% Actual wl_node usage here:

% Creating the T1, ..., Tn leaves to be permuted (L1, .., L4)
% obj.parse_trails returns wl_leaf objects which are a special type of
% wl_node
L1 = obj.parse_trials(PreExposure);
L2 = obj.parse_trials(PreExposure);
L3 = obj.parse_trials(PreExposure);
L4 = obj.parse_trials(PreExposure);

L = [L1 L2 L3 L4];

p = perms(1:4);
% Loop over all permutations creating a parent wl_node for each permutation
for k=1:rows(p)
   % Parent wl_node C(k) with copies of permuted leaves as children
   C(k) = wl_node.fixed( L(p(k,:)) );
end

% Variable to sandwich in between calls from A
D = obj.parse_trials(PreExposure);


% Usually you would not set exhaust to true, however this trial sequence is
% quite unique and required this unusual setting to be on
A = wl_node.sample(C, 'replace', false, 'exhaust', true );

% Get all permutations from A intercalated with Ds
B = rows(p) * 4 * (A + D);
T = B.parse_tree();
% Reshaping to obtain the parsed 'randomised' permutation blocks 
% (we also filter out the D's since we are just concerned with the
% correctness of the permutations
x = reshape( T.block_short(~strcmp(T.block_short, 'D')), 4, 24)';
s = string(horzcat(char(x(:,1)), char(x(:,2) ), char(x(:,3)), char(x(:,4))));
u = unique(s)

