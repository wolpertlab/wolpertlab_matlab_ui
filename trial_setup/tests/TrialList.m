%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
close all
clear all
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
FilePath = '';
 
FileList = { 'HS_TrialSequenceA' 'HS_TrialSequenceB' 'HS_TrialSequenceC' 'HS_TrialSequenceD' 'HS_TrialSequenceE' 'HS_TrialSequenceF' };
FileCount = length(FileList);
justindex = false; 
for n=1:FileCount
    load(sprintf('%s%s',FilePath,FileList{n}));
     
    FieldIndex = D.FieldIndex(1:2:end);
    FieldType = D.FieldType(1:2:end);
     
    FieldIndexList = unique(FieldIndex);
    FieldIndexCount = length(FieldIndexList);
     
    FieldTypeList = unique(FieldType);
    FieldTypeCount = length(FieldTypeList);
     
    for i=1:FieldIndexCount
        j = find(FieldIndex == FieldIndexList(i));
        FileFieldIndexCount(n,i) = length(j);
        FileFieldIndexType(n,i) = FieldType(j(1));
    end
     
    for i=1:FieldTypeCount
        j = find(FieldType == FieldTypeList(i));
        FileFieldTypeCount(n,i) = length(j);
    end
 
    figure(n);
    clf;
 
    pm = 5;
    pn = 1;
    pp = 0;
 
    pp = pp + 1;
    if ~justindex
        subplot(pm,pn,pp);
    end
    [ ~,~,y ] = unique(D.FieldIndex(1:2:end));
    x = 1:length(y);
    plot(x,y,'k.');
    axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
    xy = axis;
    xlabel('Trials');
    ylabel('Field Index');
    [a ~] = hist(y, unique(y))
 
    pp = pp + 1;
    subplot(pm,pn,pp);
    y = D.FieldType(1:2:end);
    x = 1:length(y);
    plot(x,y,'k.');
    xlabel('Trials');
    ylabel('Field Type');
    axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
    [a ~] = hist(y, unique(y))
    
    if ~justindex
        pp = pp + 1;
        subplot(pm,pn,pp);
        y = D.ContextType(1:2:end);
        x = 1:length(y);
        plot(x,y,'k.');
        xlabel('Trials');
        ylabel('Context Type');
        axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
        [a ~] = hist(y, unique(y))
        
        pp = pp + 1;
        subplot(pm,pn,pp);
        y = D.TargetAngle(1:2:end);
        x = 1:length(y);
        plot(x,y,'k.');
        xlabel('Trials');
        ylabel('Target Angle (deg)');
        axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
        [a ~] = hist(y, unique(y))

        pp = pp + 1;
        subplot(pm,pn,pp);
        y = D.FieldAngle(1:2:end);
        x = 1:length(y);
        plot(x,y,'k.');
        xlabel('Trials');
        ylabel('Field Angle (deg)');
        axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
        [a ~] = hist(y, unique(y))
    end
end