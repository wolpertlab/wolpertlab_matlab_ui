T1 = Node([],string('T1'),[],'fixed');
T2 = Node([],string('T2'),[], 'fixed');
T3 = Node([],string('T3'),[], 'fixed');
T4 = Node([],string('T4'),[], 'fixed');

A =  Node([T1 T2], 'X', [], 'sample');
B =  Node([T3 T4], 'X', [], 'sample');

W= A + B;

%C = W + W + W + W; % This can be equal to 4*(A+B) , should it ? or should it
% be  A + B + A + B ?
C = 4*W; 
C.parse_tree(4)