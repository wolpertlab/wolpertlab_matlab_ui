clear all

obj = dynamics;
cfg_name = 'trial_parse_test';

obj.cfg.TargetDistance=10;

obj.cfg.HomePosition = [0 0 0]';

%% Field Cell Array Creation
% specify items that can change across trials
% as indexed structure or indexed cell array

% Fields

Field{1}.Name           = 0; % They probably dont have names
Field{1}.FieldType		= 0;
Field{1}.FieldConstants	= [ 0 0 ];
Field{1}.FieldAngle     = 0.0;
Field{1}.ContextType    = 0;
Field{1}.TargetAngle    = 45;

Field{2}.Name           = 1;
Field{2}.FieldType		= 0;
Field{2}.FieldConstants	= [ 0 0 ];
Field{2}.FieldAngle     = 0.0;
Field{2}.ContextType    = 0;
Field{2}.TargetAngle    = -45;

Field{3}.Name           = 2;
Field{3}.FieldType		= 1;
Field{3}.FieldConstants	= [ 0.150 0 ];
Field{3}.FieldAngle     = 90.0;
Field{3}.ContextType    = 0;
Field{3}.TargetAngle    = 45;

Field{4}.Name           = 3;
Field{4}.FieldType		= 1;
Field{4}.FieldConstants	= [ 0.150 0 ];
Field{4}.FieldAngle     = -90.0;
Field{4}.ContextType    = 0;
Field{4}.TargetAngle    = -45;

Field{5}.Name           = 4;
Field{5}.FieldType		= 2;
Field{5}.FieldConstants	= [ -60.000 -0.200 ];
Field{5}.FieldAngle     = 0.0;
Field{5}.ContextType    = 0;
Field{5}.TargetAngle    = 45;

Field{6}.Name           = 5;
Field{6}.FieldType		= 2;
Field{6}.FieldConstants	= [ -60.000 -0.200 ];
Field{6}.FieldAngle     = 0.0;
Field{6}.ContextType    = 0;
Field{6}.TargetAngle    = -45;

Field{7}.Name           = 48;
Field{7}.FieldType		= 2;
Field{7}.FieldConstants	= [ -60.000 -0.200 ];
Field{7}.FieldAngle     = 0.0;
Field{7}.ContextType    = 2;
Field{7}.TargetAngle    = 45;

Field{8}.Name           = 49;
Field{8}.FieldType		= 2;
Field{8}.FieldConstants	= [ -60.000 -0.200 ];
Field{8}.FieldAngle     = 0.0;
Field{8}.ContextType    = 2;
Field{8}.TargetAngle    = -45;

Field{9}.Name           = 70;
Field{9}.FieldType		= 4;
Field{9}.FieldConstants	= [ -30.000 0.000 ];
Field{9}.FieldAngle     = 0.0;
Field{9}.ContextType    = 9;
Field{9}.TargetAngle    =  0;

% add these variables to the global workspace
obj.cfg.FieldIndex = Field;

%% This scipt creates the Leaves of the experiment
% Rmapping indexes using hashing from Hannahs config to suitable array indices in MATLAB
map = containers.Map([0 1 2 3 4 5 48 49 70], 1:9);
mp = @(y) arrayfun(@(x) map(x), y); % NOTE: This would not be necesary if FieldIndexes had been entered with Ids going from 1 to max(unique(FieldIndexes))

% create a block based on the indices above - name must match 
% Trial sub-structure are variables that change within a block
% Block sub-structure are variables that are fixed within a block

% The following is the null blocks...
% 14 trials
FT_Null.Trial.Index.FieldIndex = mp([ 0,0,0,0,0,1,1,1,1,1,4,5,48,49 ]);
FT_Null.Permute = true;

% The following are exposure blocks...
% 14 trials
FT_Exposure1.Trial.Index.FieldIndex = mp([ 2,2,2,2,2,3,3,3,3,3,4,5,48,49 ]);
FT_Exposure1.Permute = true;

% 40 trials
FT_Exposure2.Trial.Index.FieldIndex = mp([ 2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,4,5,48,48,48,48,48,48,49,49,49,49,49,49 ]);
FT_Exposure2.Permute = true;

% 26 trials
FT_Exposure26.Trial.Index.FieldIndex = mp([ 2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3 ]);
FT_Exposure26.Permute = true;

% 28 trials
FT_Exposure28.Trial.Index.FieldIndex = mp([ 2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3 ]);
FT_Exposure28.Permute = true;

% 30 trials
FT_Exposure30.Trial.Index.FieldIndex = mp([ 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3 ]);
FT_Exposure30.Permute = true;

% 32 trials
FT_Exposure32.Trial.Index.FieldIndex = mp([ 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3 ]);
FT_Exposure32.Permute = true;

% The following are probe blocks...
% 10 trials
FT_ProbeBlock1.Trial.Index.FieldIndex = mp([ 4,4,4,4,4,4,4,4,4,4 ]);
FT_ProbeBlock1.Permute = true;

FT_ProbeBlock2.Trial.Index.FieldIndex = mp([ 5,5,5,5,5,5,5,5,5,5 ]);
FT_ProbeBlock2.Permute = true;

FT_ProbeBlock3.Trial.Index.FieldIndex = mp([ 48,48,48,48,48,48,48,48,4,4 ]);
FT_ProbeBlock3.Permute = true;
FT_ProbeBlock3.Location = sparse(10,10);

FT_ProbeBlock3.Location(9,10) = 1;
FT_ProbeBlock3.Location(9,9) = 1;
FT_ProbeBlock3.Location(9,8) = 1;
FT_ProbeBlock3.Location(9,7) = 1;
FT_ProbeBlock3.Location(9,6) = 1;
FT_ProbeBlock3.Location(9,1) = 1;

FT_ProbeBlock3.Location(10,10) = 1;
FT_ProbeBlock3.Location(10,5) = 1;
FT_ProbeBlock3.Location(10,4) = 1;
FT_ProbeBlock3.Location(10,3) = 1;
FT_ProbeBlock3.Location(10,2) = 1;
FT_ProbeBlock3.Location(10,1) = 1;

FT_ProbeBlock3.Adjacency = sparse(10,10);
FT_ProbeBlock3.Location(9,10) = 1;

FT_ProbeBlock4 = FT_ProbeBlock3;
FT_ProbeBlock4.Trial.Index.FieldIndex = mp([ 49,49,49,49,49,49,49,49,5,5 ]);

FT_ProbeBlock5 = FT_ProbeBlock3;
FT_ProbeBlock5.Trial.Index.FieldIndex = mp([ 70,70,70,70,70,70,70,70,4,4 ]);

FT_ProbeBlock6 = FT_ProbeBlock3;
FT_ProbeBlock6.Trial.Index.FieldIndex = mp([ 70,70,70,70,70,70,70,70,5,5 ]);

T1.Trial.Index.FieldIndex = mp([4, 5]);
T1.Permute = false;

T2.Trial.Index.FieldIndex = mp([5, 4]);
T2.Permute = false;

%% Trial Generation Script
B = obj.parse_trials(FT_Null);

C = obj.parse_trials(FT_Exposure1);
D = obj.parse_trials(FT_Exposure2);

E1 = obj.parse_trials(FT_Exposure26);
E2 = obj.parse_trials(FT_Exposure28);
E3 = obj.parse_trials(FT_Exposure30);
E4 = obj.parse_trials(FT_Exposure32);
E = Node.permute_block([E1 E2^2 E3^2 E4]);

P(1) = obj.parse_trials(FT_ProbeBlock1);
P(2) = obj.parse_trials(FT_ProbeBlock2);
P(3) = obj.parse_trials(FT_ProbeBlock3);
P(4) = obj.parse_trials(FT_ProbeBlock4);
P(5) = obj.parse_trials(FT_ProbeBlock5);
P(6) = obj.parse_trials(FT_ProbeBlock6);

Ta(1) = obj.parse_trials(T1);
Ta(2) = obj.parse_trials(T2);

cart_prod = allcomb(1:2, 1:2); % all comb is a matlab function, computes cartesian product
for i=1:length(cart_prod)
    Tchildren(i) = Node.fixed( Ta(cart_prod(i,:)) );
end

T4 = Node.sample_block(Tchildren, 'replace', false, 'exhaust', true );

for i=1:6
    T(i) = Node.copy(T4);
    PP(i) = Node.fixed_all([ T(i) P(i) T(i) ]);
end

A = Node.permute_block(PP);

H = 6*B;
I = 9*C;
J = 15*C;

K = 5*D;
L = 4*D;

T = 6 * (E + A);

S = H + I + (2 * ('R' + J)) + 'R' + C + L + (2 * ('R' + K)) + (4 * ('R' + T)) + 'R' + H;

TTT = S.parse_tree;
%% Testing balancing and plots of TrialData:
D = TTT;
figure(10);
clf;

pm = 5;
pn = 1;
pp = 0;

pp = pp + 1;
subplot(pm,pn,pp);
[ ~,~,y ] = unique(D.Name);
x = 1:length(y);
plot(x,y,'k.');
axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
xy = axis;
xlabel('Trials');
ylabel('Field Index');
[a ~] = hist(y, unique(y))

pp = pp + 1;
subplot(pm,pn,pp);
y = D.FieldType;
x = 1:length(y);
plot(x,y,'k.');
xlabel('Trials');
ylabel('Field Type');
axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
[a ~] = hist(y, unique(y))

pp = pp + 1;
subplot(pm,pn,pp);
y = D.ContextType;
x = 1:length(y);
plot(x,y,'k.');
xlabel('Trials');
ylabel('Context Type');
axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
[a ~] = hist(y, unique(y))

pp = pp + 1;
subplot(pm,pn,pp);
y = D.TargetAngle;
x = 1:length(y);
plot(x,y,'k.');
xlabel('Trials');
ylabel('Target Angle (deg)');
axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
[a ~] = hist(y, unique(y))

pp = pp + 1;
subplot(pm,pn,pp);
y = D.FieldAngle;
x = 1:length(y);
plot(x,y,'k.');
xlabel('Trials');
ylabel('Field Angle (deg)');
axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
[a ~] = hist(y, unique(y))



