clear all

obj = dynamics;
cfg_name = 'trial_parse_test';

obj.cfg.TargetDistance=10;

obj.cfg.HomePosition = [0 0 0]';

%%
% specify items that can change across trials
% as indexed structure or indexed cell array

% Null field.
Field{1}.Name		= 'null';
Field{1}.FieldType		= 0;
Field{1}.FieldConstants	= [0 0];
Field{1}.FieldAngle	=	0.0;
Field{1}.FieldMatrix	=	eye(3);

% Viscous curl field
Field{2}.Name		= 'curl';
Field{2}.FieldType		= 1;
Field{2}.FieldConstants	= [0.2 0];
Field{2}.FieldAngle	=	90.0;
Field{2}.FieldMatrix	=	eye(3);

% Channel trial
Field{3}.Name		= 'channel';
Field{3}.FieldType		= 2;
Field{3}.FieldConstants	= [-30.000  -0.05];
Field{3}.FieldAngle	=	0.0;
Field{3}.FieldMatrix	=	eye(3);

TargetAngle=num2cell(linspace(0,2*pi,5));

BlockVars{1}.Test1 = 1;
BlockVars{1}.Test2 = 2;
BlockVars{1}.Test3 = 3;

BlockVars{2}.Test1 = 10;
BlockVars{2}.Test2 = 20;
BlockVars{2}.Test3 = 30;

BlockVars{3}.Test1 = 100;
BlockVars{3}.Test2 = 200;
BlockVars{3}.Test3 = 300;

Test{1}=eye(3);
Test{2}=eye(3);

%add these variables to the global workspace

obj.cfg.Field=Field;
obj.cfg.BlockVars=BlockVars;
obj.cfg.TargetAngle=TargetAngle;
obj.cfg.Test=Test;

%%

% create a block based on the indices above - name must match 
% Trial sub-structure are variables that change within a block
% Block sub-structure are variables that are fixed within a block

PreExposure.Trial.Index.Field = [ 1 2 3 ];
PreExposure.Trial.FieldValue = [ 1 2 3 ];
PreExposure.Trial.Index.TargetAngle =	[ 1 2 3 ];
PreExposure.Block.Index.BlockVars = 1;

PreExposure.Block.Phase = 5;
PreExposure.Permute = true; %whether to permute within block - default is no
PreExposure.Location = sparse(3,3);
PreExposure.Adjacency = sparse(3,3);

Exposure.TrialCount = 3;
Exposure.Trial.Index.Field = 2;
Exposure.Trial.FieldValue = 3;
Exposure.Trial.Index.TargetAngle = 1;
Exposure.Permute = false;
Exposure.Block.Index.BlockVars = 2;
Exposure.Block.Phase = 2;

PostExposure.TrialCount = 6;
PostExposure.Trial.Index.Field = [ 1 2 3 ];
PostExposure.Trial.FieldValue = [ 1 2 3 ];
PostExposure.Trial.Index.TargetAngle = [ 1 2 3 ];
%PostExposure.Permute = true; 
PostExposure.Block.Index.BlockVars = 3;
PostExposure.Block.Phase = 22;
%%
A = obj.parse_trials(PreExposure);
B = obj.parse_trials(Exposure);
C = obj.parse_trials(PostExposure);

t = parse_tree(C +  B  + A)
