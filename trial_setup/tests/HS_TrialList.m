%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FilePath = 'C:\Users\jni20\Dropbox\WolpertVargasIngram\TrialSequences\HS\';

FileList = { 'HS_TrialSequenceA' 'HS_TrialSequenceB' 'HS_TrialSequenceC' 'HS_TrialSequenceD' 'HS_TrialSequenceE' 'HS_TrialSequenceF' };
FileCount = length(FileList);

for n=1:FileCount
    load(sprintf('%s%s',FilePath,FileList{n}));
    
    %FieldIndex = D.FieldIndex(1:2:end);
    [ ~,~,FieldIndex ] = unique(D.FieldIndex(1:2:end));
    FieldIndexList = unique(FieldIndex);
    FieldIndexCount = length(FieldIndexList);
    
    FieldType = D.FieldType(1:2:end);
    FieldTypeList = unique(FieldType);
    FieldTypeCount = length(FieldTypeList);
    
    for i=1:FieldIndexCount
        j = find(FieldIndex == FieldIndexList(i));
        FileFieldIndexCount(n,i) = length(j);
        FileFieldIndexType(n,i) = FieldType(j(1));
    end
    
    for i=1:FieldTypeCount
        j = find(FieldType == FieldTypeList(i));
        FileFieldTypeCount(n,i) = length(j);
    end

    TargetAngle = D.TargetAngle(1:2:end);
    TargetAngleList = unique(TargetAngle);
    TargetAngleCount = length(TargetAngleList);
    
    for i=1:TargetAngleCount
        j = find(TargetAngle == TargetAngleList(i));
        FileTargetAngleCount(n,i) = length(j);
    end
    
    FieldAngle = D.FieldAngle(1:2:end);
    FieldAngleList = unique(FieldAngle);
    FieldAngleCount = length(FieldAngleList);
    
    for i=1:FieldAngleCount
        j = find(FieldAngle == FieldAngleList(i));
        FileFieldAngleCount(n,i) = length(j);
    end
    
    figure(n);
    clf;

    pm = 5;
    pn = 1;
    pp = 0;

    pp = pp + 1;
    subplot(pm,pn,pp);
    [ ~,~,y ] = unique(D.FieldIndex(1:2:end));
    x = 1:length(y);
    plot(x,y,'k.');
    axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
    xy = axis;
    xlabel('Trials');
    ylabel('Field Index');

    pp = pp + 1;
    subplot(pm,pn,pp);
    y = D.FieldType(1:2:end);
    x = 1:length(y);
    plot(x,y,'k.');
    xlabel('Trials');
    ylabel('Field Type');
    axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
    
    pp = pp + 1;
    subplot(pm,pn,pp);
    y = D.ContextType(1:2:end);
    x = 1:length(y);
    plot(x,y,'k.');
    xlabel('Trials');
    ylabel('Context Type');
    axis([ (min(x)-10) (max(x)+10) (min(y)-1) (max(y)+1) ]);
    
    pp = pp + 1;
    subplot(pm,pn,pp);
    y = D.TargetAngle(1:2:end);
    x = 1:length(y);
    plot(x,y,'k.');
    xlabel('Trials');
    ylabel('Target Angle (deg)');
    axis([ (min(x)-10) (max(x)+10) (min(y)-10) (max(y)+10) ]);
    
    pp = pp + 1;
    subplot(pm,pn,pp);
    y = D.FieldAngle(1:2:end);
    x = 1:length(y);
    plot(x,y,'k.');
    xlabel('Trials');
    ylabel('Field Angle (deg)');
    axis([ (min(x)-10) (max(x)+10) (min(y)-10) (max(y)+10) ]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\nFieldIndex\n');
for n=1:FileCount
    fprintf('n=%d ',n);
    for i=1:FieldIndexCount
        fprintf('[ %d %4d ]',FieldIndexList(i),FileFieldIndexCount(n,i));
    end
    fprintf('\n');
end

fprintf('\nFieldType\n');
for n=1:FileCount
    fprintf('n=%d ',n);
    for i=1:FieldTypeCount
        fprintf('[ %d %4d ]',FieldTypeList,FileFieldTypeCount(n,i));
    end
    fprintf('\n');
end

fprintf('\nTargetAngle\n');
for n=1:FileCount
    fprintf('n=%d ',n);
    for i=1:TargetAngleCount
        fprintf('[ %3.0f� %4d ]',TargetAngleList(i),FileTargetAngleCount(n,i));
    end
    fprintf('\n');
end

fprintf('\nFieldAngle\n');
for n=1:FileCount
    fprintf('n=%d ',n);
    for i=1:FieldAngleCount
        fprintf('[ %3.0f� %4d ]',FieldAngleList(i),FileFieldAngleCount(n,i));
    end
    fprintf('\n');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
