function wl_trial_setup(obj)
%WL_TRIAL_SETUP    
    if( isfield(obj.GW,'Robot') )
        ok = obj.Robot.FieldNull();
    end
    
    tmp=table2struct(obj.TrialData(obj.TrialNumber,:));
    fname=fieldnames(tmp);
    for k=1:length(fname)
        if ~ischar(tmp.(fname{k}))
            if iscell(tmp.(fname{k}))
                for i=1:length(tmp.(fname{k}))
                    tmp.(fname{k}){i}=shiftdim(tmp.(fname{k}){i});
                end
            else
                tmp.(fname{k})=shiftdim(tmp.(fname{k}));
            end
        end
    end
    % obj.Trial=catstruct(obj.Trial,tmp);
    obj.Trial=tmp;
    
    %fprintf('TrialSetup() Trial=%d, Field=%d\n',obj.GW.Trial,obj.GW.FieldType);
    if( isfield(obj.GW,'Robot') )
        ok = obj.Robot.RampDown();
    end
    
    obj.GW.RestBreakRemainPercent=1;
end
