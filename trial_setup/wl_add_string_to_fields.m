function Q=wl_add_string_to_fields(MyStruct,s)
%WL_ADD_STRING_TO_FIELDS renames struct fields to end with a string
%   Q=WL_ADD_STRING_TO_FIELDS(MYSTRUCT,S) Takes in a struct MYSTRUCT and a
%   string S and renames every field in MYSTRUCT to be the concatenation of
%   its previos name and the string S
%
%   % Example
%   >> S.positionvector = [ 1, 3 ];
%   >> S.speedvector = [ 2, 4 ];
%   >> S = wl_add_string_to_fields(S, '_stiffbot')
%   S =
%
%   struct with fields:
%
%     positionvector_stiffbot: [1 3]
%        speedvector_stiffbot: [2 4]

    % Get a cell array of MyStruct's field names
    fields = fieldnames(MyStruct);
    
    % Create an empty struct
    temp = struct;
    
    % Loop through and assign each field of new struct after doing string
    % replacement. You may need more complicated (regexp) string replacement
    % if field names are not simple prefixes
    for ii = 1 : length(fields)
        temp.([fields{ii} s]) = MyStruct.(fields{ii});
    end
    
    % Replace original struct
    Q = temp;
end