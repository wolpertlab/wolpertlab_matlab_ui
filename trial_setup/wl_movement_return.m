function T=wl_movement_return(C,S,str)
%WL_MOVEMENT_RETURN calculates the movement returns for the passive trials
%
%   WL_MOVEMENT_RETURN(CFG,S,STR) CFG is the config struct of the
%   experiment namely OBJ.CFG where OBJ is a pointer to the experiment
%   object. S is the trial data table which tradiationally can be found in
%   OBJ.TrialData and S is a string indicating the nature of the trial
%   which can take on the values of 'same' , 'passive' or 'null'

    % TODO: Null field seems equivalent to passive? why?
    
    % Number of rows in table S
    r = rows(S);
    ri = ones(r-1,1);
    
    % Check if current table S has a HomePosition collumn
    % else Take from the CFG C
    if isfield(S, 'HomePosition')
        home = S.HomePosition;
    else
        home = repmat(reshape(C.HomePosition,1,3),r-1,1);
    end
    
    % Create larger table T with every even trial being a return of type
    % str
    T = table;
    T(1:2:2*r-1,:) = S;
    T(2:2:2*r-1,:) = S(1:end-1,:);
    
    % Check for movement_return type
    if strcmp(str,'same')
        % do nothing
    elseif strcmp(str, 'passive')
        % Passive return FieldType = 3
        T(2:2:2*r-1,:).FieldType = 3 * ri;
    elseif strcmp(str, 'null')
        % Null return FieldType = 0, this seems redudant
        T(2:2:2*r-1,:).FieldType = 0 * ri;
    end
    
    % No restflags for passive returns
    T(2:2:2*r-1,:).RestFlag = 0 * ri;
    
    T(2:2:2*r-1,:).TargetPosition = home;
    T.Trial = (1:(2*r-1))';
end