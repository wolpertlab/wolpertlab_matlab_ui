function r = wl_constrained_permute(N, location, adjacency)
%WL_CONSTRAINED_PERMUTE permutes the integers 1:N given a set of constraints
% Typically the integers refer to trials of a particular type
%
% r= WL_CONSTRAINED_PERMUTE (N, location, adjacency)
%
% location(i,j) = 1 means integer i cannot be at j-th position
% adjaceny(i,j) = 1 means integer j cannot directly follow integer i
% the convention for 1 meaning forbidden allows the use of sparse
% matrices and the easy display of contsraints
%
% Uses the min-conflict algorithm from the N-Queens problem
% https://en.wikipedia.org/wiki/Min-conflicts_algorithm
    
    if nargin <2 || isempty(location)
        location=zeros(N, N);
    end
    
    if nargin <3 || isempty(adjacency)
        adjacency=zeros(N, N);
    end
    
    %make full matrices in case they are sparse and flip so 1 is allowed
    location_full=full(~location);
    adjacency_full=full(~adjacency);

    maxn=1000; %max number of permutations to try
    
    for k=1:maxn
        if mod(k,100)==1 %permute randomly initially or if we get stuck
            r=randperm(N);
        end
        
        %location test
        s=sub2ind([N N],r,1:N);  %elements to test in locations matrix
        loc_test=location_full(s); %this should be all 1 if OK
        
        %adjacency test
        s=sub2ind([N N], r(1:N-1), r(2:N));  %elements to test in adjacency  matrix
        adj_test=adjacency_full(s); %this should be all 1 if OK
        
        if all(loc_test) && all(adj_test) % success - we are done
            break;
            
        elseif ~all(loc_test)  %we will do a swap with first conflict
            i=findfirst(loc_test'==0); %find first error
            poss=find(location_full(r(i),:)); %find locations it can go
            
            if isempty(poss)
                error('Permutation failed. Location constraints too restrictive')
            end
            
            k = randi(length(poss));  %choose random valid location to swap to
            r([i poss(k)])= r([poss(k) i]); %now swap positions
            
        elseif ~all(adj_test)   %we will do a swap with first conflict
            i=findfirst(adj_test'==0);   %we will do a swap with first conflict pair
            w=rand>0.5; %randomise which of the pair we swap
            poss=find(location_full(r(i+w),:));
            
            if isempty(poss)   %change to other pair if no possible locations
                w=1-w;
                poss=find(location_full(r(i+w),:));
                if isempty(poss)
                    error('Permutation failed. Adjacency with location constraints too restrictive')
                end
            end
            
            k=randi(length(poss)); %choose random valid location to swap to
            r([i+w poss(k)])= r([poss(k) i+w]);  %now swap
        end
    end    
    
    if k == maxn
        r = []; %#ok<NASGU>
        error('Number of permutation attempts exceeded. Are constraints too restrictive?')
    end
end


