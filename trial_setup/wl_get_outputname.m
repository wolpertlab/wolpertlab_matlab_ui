function outputname=wl_get_outputname
%WL_GET_OUTPUTNAME obtains the name of the variable before the equal sign
%for the last declared/set variable
     
    stDebug = dbstack;
    % We know a-priori that this function's caller info is in (stDebug)(2)
    % Get caller file name & line number of code which called this function:
    
    if length(stDebug)>3
        callerFileName = stDebug(4).file;
        callerLineNumber = stDebug(4).line;
        % Open caller file:
        fCaller = fopen(callerFileName);
        % Iterate through lines to get to desired line number:
        for iLine = 1 : callerLineNumber
            % Read current line of text:
            tmp = fgetl(fCaller);
        end
        % (currLine) now reflects calling desired code: display this code:
        
        outputname= strrep(tmp(1:strfind(tmp,'=')-1),' ','');
        % Close caller file:
        fclose(fCaller);
        
    else
        outputname='';
    end
    
    if isempty(outputname)
        outputname='';
    end
    
    
end