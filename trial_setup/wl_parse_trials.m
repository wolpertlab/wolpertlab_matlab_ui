function R = wl_parse_trials(obj, X, realname)
%WL_PARSE_TRIALS Creates Leaves/Blocks with corresponding subtables in them
% by parsing the config structure specified in the config file into small
% table blocks returning a wl_leaf data type.
%
%    OBJ: a pointer to the WL object used to read the config file (an Instance of ExperimentBase i.e. WL)
%
%    X : Struct containing a trial block (set of trials), for example:
%
%       X.Trial.Index.Field = [ 1 1 1 ];  % 3 trials pointing at field index 1
%       X.Trial.Index.TargetAngle = ones(1, 3);
%       X.Permute = true;  % whether to permute within block
%       X.Location  = sparse(3, 3);
%       X.Location(3,1) = 1;  % Trial 3 should never be shuffled to position 1    
%
%    realname : inputname(2) , this parameter is handled by ExperimentBase
%      and corresponds to the variable name of the second parameter (X)
%           
%        A = WL.parse_trails(X)  % realname = X
%
%   R  : wl_leaf with small trial block table built in 
    
    X = orderfields(X); %sort fields
    
    outputname = wl_get_outputname;  % get variable name befor equals sign
    
    % Init variables used to traverse index and non index fields
    nblockvar = 0;
    nindexvar = 0;
    nindexBlockVar = 0;
    indexBlockVarNames = [];
    indexvarnames = [];
    
    % Setting Block Init variables
    if isfield(X,'Block')
        blockvarnames = fieldnames(X.Block);
        nblockvar = length(blockvarnames);
        if isfield(X.Block,'Index')
            indexBlockVarNames = fieldnames(X.Block.Index);
            nindexBlockVar = length(indexBlockVarNames);
        end
    end
    
    % Setting Trial.Index init variables
    if isfield(X.Trial,'Index')
        indexvarnames = fieldnames(X.Trial.Index);
        nindexvar = length(indexvarnames);
    end
    
    trialvarnames = fieldnames(X.Trial);
    ntrialvar = length(trialvarnames);
    nindexes = [ nindexvar nindexBlockVar ];
    indexNames = { indexvarnames indexBlockVarNames };
    
    % Loops over all X.Y.Index variables where Y could be Block or Trial
    for m=1:length(nindexes) 
        indvar = nindexes(m);
        namevar = indexNames{m};
        % pre-allocate all variables to tables for speed of access
        for j=1:indvar % Loop over fields inside index variable
            name=namevar{j};
            for k=1:length(obj.cfg.(name)) % Loop over Trials inside field
                s = obj.cfg.(name){k};
                if ~isempty(s)
                    if isstruct(s)
                        tt.(name){k} = struct2table(s, 'AsArray', 1); % this allows us to pass on multidim array in the table
                    elseif isvector(s)
                        tt.(name){k} = array2table(s, 'VariableNames', ...
                                                   {name});
                    else % matrix
                        t = table;
                        t.(name){1} = s;
                        tt.(name){k} = t;
                    end
                end
            end
        end
    end
    
    circ = @(x, N) (1 + mod(x-1, N)); % Cycic index Z_{N}
    count = 0; %trial count
    %create table
    T = table; %null table

    % Obtain the length of the trial lists, searching for the  
    % first element which is not in the Index field
    noindex = find(~strcmp(trialvarnames, 'Index'),1);
    if ~isempty(noindex)
        m = length(X.Trial.(trialvarnames{noindex}));
    else
        m = length(X.Trial.Index.(indexvarnames{1}));
    end
    % Obtaining number of trails
    if isfield(X, 'TrialCount')
        nn = X.TrialCount;
    elseif isfield(X.Trial, 'Index')
        nn = length(X.Trial.Index.(indexvarnames{1}));
    else
        nn = m;
    end

    % Checking length of trial list is a multiple of the number of trials
    % The function would actually work even if this was not the case
    % however it would truncate the cycle at an arbitrary point i. e 
    % 1 2 3 1 2 3 1 2 3 1 2
    if mod(nn, m) ~= 0
        err = char(sprintf("length(X.Trial.('%s')) = %d is not a multiple of TrialCount = %d",...
                   trialvarnames{noindex}, m, nn));
        error(err);
    end
    
    % Looping over number of trials
    for k=1:nn
        count = count + 1;
        tmp = table;  %one row of the table
        % Some of these loops could be merged but not sure its worth the
        % trouble
        
        % Inserting Trial.Index variables into table
        for j=1:nindexvar
            kk = circ(k, length(X.Trial.Index.(indexvarnames{j})));
            tmp1 = tt.(indexvarnames{j}){X.Trial.Index.(indexvarnames{j})(kk)};
            tmp = horzcat(tmp,tmp1);
        end
        
        % Inserting Block.Index variables into table
        for j=1:nindexBlockVar
            tmp1 = tt.(indexBlockVarNames{j}){ X.Block.Index.(indexBlockVarNames{j}) };
            tmp = horzcat(tmp,tmp1);
        end
        
        % Inserting Trial variables into table
        for j=1:ntrialvar
            if strcmp(trialvarnames{j}, 'Index'); continue; end
            kk = circ(k, length(X.Trial.(trialvarnames{j})));
            tmp1 = array2table(X.Trial.(trialvarnames{j})(kk), ...
                               'VariableNames', {trialvarnames{j}});
            tmp = horzcat(tmp, tmp1);
        end
        
        % Inserting Block variables into table
        for j=1:nblockvar
            if strcmp(blockvarnames{j}, 'Index'); continue; end
            clear w
            w.(blockvarnames{j}) = X.Block.(blockvarnames{j});
            tmp = horzcat(tmp, struct2table(w));
        end
        
        % I am not sure why this is done this way
        clear w
        w.RestFlag = 0;
        w.MissTrial = 0;
        
        tmp = horzcat(tmp, struct2table(w));
        T(count,:) = tmp; % inserting row into table
    end
    
    [~, i] = sort(upper(T.Properties.VariableNames));
    T = T(:,i);
    TT = table();
    
    TT.block_name = repmat(cellstr(realname),height(T),1);
    TT.block_short = repmat(cellstr(outputname),height(T),1);
    
    T = [ TT T ];
    
    % Setting default premute flag for leaf
    if ~isfield(X, 'Permute')
        X.Permute = false;
    end
    
    % Setting default location matrix
    if ~isfield(X,'Location')
        X.Location = sparse(nn, nn);
    end
    
    % Setting default adjacency matrix
    if ~isfield(X,'Adjacency')
        X.Adjacency = sparse(nn, nn);
    end
    
    % Creating leaf with parsed table block
    R = wl_leaf(T, X.Location, X.Adjacency, X.Permute);
end
