classdef DoubleLinkedList < handle
% Similar idea to deque datastructure in C++ and python
    properties (Access = public)
        head;
        tail;
        empty;
    end
    methods (Access = public)
        
        function this = DoubleLinkedList()
            this.head = false;
            this.tail = false;
            this.empty = true;
        end
        
        function out=is_empty(this)
            out = this.empty;
        end
        
        function insert_head(this, data)
            node = DoubleLinkedListNode(this.head, false, data);
            if this.head ~= false
                this.head.parent = node;
                this.head = this.head.parent;
            else
                this.head = node;
                this.tail = node;
            end
            this.empty = false;
        end
        
        function insert_tail(this, data)
            node = DoubleLinkedListNode(false, this.tail, data);
            if this.tail ~= false
                this.tail.child = node;
                this.tail = this.tail.child;
            else
                this.tail = node;
                this.head = node;
            end
            this.empty = false;
        end
        
        function out=pop_tail(this)
            if ~this.is_empty()
                out = this.tail;
            else
                out = false;
                return;
            end
            
            if this.tail.parent ~= false
                this.tail = this.tail.parent; 
            else
                this.empty = true;
                this.tail = false;
                this.head = false;
            end
            
        end
        
        function out=pop_head(this)
            if ~this.is_empty()
                out = this.head;
            else
                out = false;
                return;
            end
            
            if this.head.child ~= false
                this.head = this.head.child; 
            else
                this.empty = true;
                this.tail = false;
                this.head = false;
            end
            
        end
 
    end

end