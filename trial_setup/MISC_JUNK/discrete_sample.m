function samp =  discrete_sample(A, p)
    if length(p) == 1
        samp = A(1);
        return
    end
    p = p / sum(p);
    samp = A(find(mnrnd(1,p)));
    samp = samp(1);
    return;
    p = sort(p);
    cumulative_distribution = [0 p];
    for i=2:(length(p) + 1)
        cumulative_distribution(i) = cumulative_distribution(i-1) + cumulative_distribution(i);
    end
    x = rand();
    
    for i=1:length(p)
        if x < cumulative_distribution(i+1)
            samp = A(min(i,length(p)));
            break;
        end
    end
end
