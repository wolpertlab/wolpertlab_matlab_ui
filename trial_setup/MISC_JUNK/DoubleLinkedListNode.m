classdef DoubleLinkedListNode < handle
% Simple node node used as a building block of double linked list
    properties (Access = public)
        child;
        parent;
        data;
    end
    methods (Access = public)
        
        function this = DoubleLinkedListNode(child, parent, data)
            this.child = child;
            this.parent = parent;
            this.data = data;
        end
    end
end