classdef wl_leaf < wl_node
    properties
        position % Position constraint sparse matrix 1's indicate item i should not be shuffled to position j
        adjacency % Adjecency constraint matrix 1's indicate item i should never be next to item j
        permute % Boolean true or false (permutes rows of table if true)
        table % empty table plce holder fillef in by parse_tree
        id % Internal id to keep track of number of leave instancs
        trial_data % initial trial_data table filled in by wl_parse_trials
    end
    
    methods

        function this = wl_leaf(trial_data, position, adjacency, permute)
        % wl_leave is the constructor for a subclass of Node (wl_node) that is
        % specifically designated to hold the trial batch tables
        % directly. This is a special type of node that has no children.
        %
        % a = wl_leaf(table({'a'}), [], [], 0);
        % t = parse_tree( 2*a )
        %
        % block_count    block_index    block_index_count    block_trial    block_original_trial    Var1
        % ___________    ___________    _________________    ___________    ____________________    ____
        % 1              1              1                    1              1                       'a' 
        % 2              1              2                    1              1                       'a'
        %
        % (please type wl_node for details on its superclass and
        % supermethods)
            
            % Caling super class constructor
            this@wl_node([], [], 'fixed', []); % Creating leaf node
            this.position = position;
            this.table = table();
            this.trial_data = trial_data;
            this.adjacency = adjacency;
            this.permute = permute;
            this.id = this.len(this.len + 1);
        end
        
    end
    
    methods (Static)

      function out = len(data)
      %LEN keeps track of the leave instances in RAM and assigns a
      % unique identifier to each leave
         persistent instances;
         if isempty(instances) && ~nargin
             out = 0;
             return;
         end
         if nargin
            instances = data;
         end
         out = instances;
      end

    end

end