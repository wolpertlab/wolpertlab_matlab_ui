classdef wl_gui < matlab.apps.AppBase
    
    % Properties that correspond to app components
    properties (Access = public)
        UIFigure                  matlab.ui.Figure
        TabGroup                  matlab.ui.container.TabGroup
        FixedParametersTab        matlab.ui.container.Tab
        savefileEditFieldLabel    matlab.ui.control.Label
        savefileEditField         matlab.ui.control.EditField
        configfileEditFieldLabel  matlab.ui.control.Label
        configfileEditField       matlab.ui.control.EditField
        configtypeEditFieldLabel  matlab.ui.control.Label
        dateLabel                 matlab.ui.control.Label
        configtypeEditField       matlab.ui.control.EditField
        RunButton                 matlab.ui.control.Button
        Before                    matlab.ui.control.Button
        Default                   matlab.ui.control.Button
        Forward                   matlab.ui.control.Button
        SmallScreen               matlab.ui.control.CheckBox
        AddParametersTab          matlab.ui.container.Tab
        MouseFlag                 matlab.ui.control.CheckBox
        Exit                      matlab.ui.control.Button
        cacheName;
        EditFieldList;
        EditFieldStruct;
        ParamNames;
        ParamTypes;
        TypeMapper;
        pSet;
        gridStack;
        colInd;
        paramStep;
        cacheLength;
        defaultCache;
        running;
        Name;
        Timer;
        commandLineData;
    end
    
    
    % App initialization and construction
    methods (Access = private)
     
        function ExitButtonPushed(app, event)
            delete(app.UIFigure);
        end
        
        %RunButton Callback
        function RunButtonPushed(app, event)
            % Loads the parameters into app.pSet and releases the program
            % lock from the GUI (by closing it)
            for i=1:length(app.ParamNames)
                caster = app.TypeMapper.(char(app.ParamTypes(i)));
                param_name = char(app.ParamNames(i,1));
                cache_el.(param_name) = caster(app.EditFieldStruct.(param_name).Value);
                cache_el.dt = datetime;
                cache_el.pos = app.UIFigure.Position;
            end
            if exist(app.cacheName, 'file')
                load(app.cacheName);
                cache = wl_gui.unique_insert(cache, cache_el);
            else
                cache = {cache_el};
            end
            app.cacheLength = length(cache);
            save(app.cacheName, 'cache');
            
            uiresume(app.UIFigure);
            app.pSet = cache_el;
        end
        
        function BeforeButtonPushed(app, event)
            % BeforeButton steps through the cache file loading previous
            % parameters to the GUI
            min(max(app.paramStep + 1, 1), app.cacheLength-1);
            app.paramStep = min(max(app.paramStep + 1, 1), app.cacheLength-1);
            app.load_cache(0);
        end
        
        function DefaultButtonPushed(app, event)
            % app event when default button pushed, loads default parameters
            % to the GUI display
            app.paramStep = 0;
            app.load_cache(1);
            app.dateLabel.Text = '0 back';
        end
        
        function ForwardButtonPushed(app, event)
            % app event moves app.ParamStep a step forward in time (happens
            % when forward button in the GUI is pushed)
            if app.paramStep >= 1
                app.paramStep = min(max(app.paramStep - 1, 1), app.cacheLength-1);
                app.paramStep;
                app.load_cache(0);
            else
                % DefaultButtonPushed(app, event);
            end
        end
        
        % Create UIFigure and components
        function createComponents(app)
            
            % Create UIFigure
            app.UIFigure = uifigure;
            app.UIFigure.Position = [100 100 640 480];
            app.UIFigure.Name = 'UI Figure';
            
            % Create TabGroup
            app.TabGroup = uitabgroup(app.UIFigure);
            app.TabGroup.Position = [1 1 640 480];
            
            % Create FixedParametersTab
            app.FixedParametersTab = uitab(app.TabGroup);
            app.FixedParametersTab.Title = 'Parameters';
            
            % Create savefileEditFieldLabel
            app.savefileEditFieldLabel = uilabel(app.FixedParametersTab);
            app.savefileEditFieldLabel.HorizontalAlignment = 'left';
            app.savefileEditFieldLabel.Position = [79 377+16 100 22];
            app.savefileEditFieldLabel.FontWeight = 'bold';
            app.savefileEditFieldLabel.Text = 'save file';
            
            % Create savefileEditField
            app.savefileEditField = uieditfield(app.FixedParametersTab, 'text');
            app.savefileEditField.Position = [79 377 100 22];
            
            % Create configfileEditFieldLabel
            app.configfileEditFieldLabel = uilabel(app.FixedParametersTab);
            app.configfileEditFieldLabel.HorizontalAlignment = 'left';
            app.configfileEditFieldLabel.Position = [272 377+16 100 22];
            app.configfileEditFieldLabel.FontWeight = 'bold';
            app.configfileEditFieldLabel.Text = 'config file';
            
            % Create configfileEditField
            app.configfileEditField = uieditfield(app.FixedParametersTab, 'text');
            app.configfileEditField.Position = [272 377 100 22];
            
            % Create configtypeEditFieldLabel
            app.configtypeEditFieldLabel = uilabel(app.FixedParametersTab);
            app.configtypeEditFieldLabel.HorizontalAlignment = 'left';
            app.configtypeEditFieldLabel.Position = [477 376+16 100 22];
            app.configtypeEditFieldLabel.FontWeight = 'bold';
            app.configtypeEditFieldLabel.Text = 'config type';
            
            % Create configtypeEditField
            app.configtypeEditField = uieditfield(app.FixedParametersTab, 'text');
            app.configtypeEditField.Position = [477 376 100 22];
            
            % Create ReloadtableCheckBox
            app.SmallScreen = uicheckbox(app.FixedParametersTab);
            app.SmallScreen.Text = 'Small Screen';
            app.SmallScreen.FontWeight = 'bold';
            app.SmallScreen.Position = [79 377-50 100 22];
            
            app.MouseFlag = uicheckbox(app.FixedParametersTab);
            app.MouseFlag.Text = 'Mouse Flag';
            app.MouseFlag.FontWeight = 'bold';
            app.MouseFlag.Position = [272 377-50 100 22];
            
            % Create RunButton
            app.RunButton = uibutton(app.FixedParametersTab, 'push');
            app.RunButton.Position = [477+50 29 100 22];
            app.RunButton.Text = 'Run';
            app.RunButton.ButtonPushedFcn = createCallbackFcn(app, @RunButtonPushed, true);
            
            % Create Exit Button
            app.RunButton = uibutton(app.FixedParametersTab, 'push');
            app.RunButton.Position = [477-60 29 100 22];
            app.RunButton.Text = 'Exit';
            app.RunButton.ButtonPushedFcn = createCallbackFcn(app, @ExitButtonPushed, true);
            
            % Create Before Button
            app.Before = uibutton(app.FixedParametersTab, 'push');
            app.Before.Position =  [29 33 100 22];
            app.Before.Text = 'Backwards';
            app.Before.ButtonPushedFcn = createCallbackFcn(app, @BeforeButtonPushed, true);
            
            % Create Before Default
            app.Default = uibutton(app.FixedParametersTab, 'push');
            app.Default.Position =  [149 33 100 22];
            app.Default.Text = 'Default';
            app.Default.ButtonPushedFcn = createCallbackFcn(app, @DefaultButtonPushed, true);
            
            % Create Before Forward
            app.Forward = uibutton(app.FixedParametersTab, 'push');
            app.Forward.Position =  [269 33 100 22];
            app.Forward.Text = 'Forward';
            app.Forward.ButtonPushedFcn = createCallbackFcn(app, @ForwardButtonPushed, true);
            
            app.dateLabel = uilabel(app.FixedParametersTab);
            app.dateLabel.HorizontalAlignment = 'left';
            app.dateLabel.Position = [29 03 500 22];
            app.dateLabel.Text = '';
            
        end
    end
    
    methods(Static)
        
        function out= str2array(str)
            n = length(str);
            if n < 1
                out = [];
            else
                out = str2double(split(str, ' ')');
            end
        end
        
        function out=str2double2(str)
            if isa(str,'logical') || isa(str,'double') || isa(str,'single')
                out = str;
            else
                out = str2double(str);
            end
        end
        
        function out=parse_value(val)
            if isempty(val)
                out = '';
            else
                out = char(string(val));
            end
        end
        
        function out = unique_struct_array(strct)
            %UNIQUE_STRUCT_ARRY Cleans up struct with duplicates keeping the most recent
            % date of a particular struct array
            % Only needed to be used once to clean up my database for debugging
            % purposes, Can be used later on if someone wishes to clean their
            % parameter space
            % Not sure if in use
            out = [];
            dups = zeros(length(strct)); % deduplicator;
            for i=1:length(strct)
                if dups(i)
                    continue;
                end
                x = strct(i);
                dt = x.dt;
                for j=1:length(strct)
                    if i ~= j
                        y = strct(j);
                        if wl_gui.struct_eq(x,y)
                            dups(j) = 1;
                            dt = max(dt, y.dt);
                        end
                    end
                end
                x.dt = dt;
                out = [out; x];
            end
        end
        
        function out = unique_insert(strct_arr, strct)
            %UNIQUE_INSERTS Inserts a struct into a cell array of structs and removes
            % duplicates (identical structs that were already in the array)
            %
            % strct_arr   - cell array of structs
            % strct       - struct to be inserted into struct_arr
            
            out = {};
            
            % Loop over struct cell array and filter entries which are the
            % same as strct
            for j=1:length(strct_arr)
                y = strct_arr{j};
                % Insert struct if not already in array
                if ~wl_gui.struct_eq(strct, y)
                    out = [out y];
                end
            end
            % Insert strct at the end of the array as the most recent item
            out = [out strct];
            
        end
        
        function out = struct_eq(x,y)
            % compares two structs for equality ignoring the unique key dt
            % (corresponding to its date)
            % Used to not store the same parameter configurations mltiple
            % times
            %
            % x, y - structs to compare fields and corresponding values over
            
            if numel(fields(x)) ~= numel(fields(y)) || ~isequal(fields(y),fields(x))
                out = false;
            else
                keys = fields(x);
                for i=1:numel(keys)
                    k = char(keys(i));
                    if ~isequal(k, 'dt') && ~isequal(x.(k), y.(k))
                        out = false;
                        return;
                    end
                end
                out = true;
            end
        end
    end
    
    methods (Access = public)
        
        % Construct app
        function app = wl_gui(exp_name, save_file, ...
                config_file, config_type, varargin)
            %wl_gui Constructor for the GUI. Sets up default location of fields
            % as well as default variables.
            %
            % exp_name    - string with the name of the experiment (must
            %               match name of experiment file i.e. dynamics.m)
            % save_file   - string with name of the file with experiment
            %               data will be saved
            % config_file - string with the cfg file for the experiment
            %               (i.e. cfg_dynamics)
            % config_type - string parameter passed to the cfg file
            %
            % varargin    - are commandline parameters to be passed only
            %               when the graphical interface is not wanted
            
            % If args passed ignore GUI and run commandline mode
            if ~isempty(varargin)
                app.pSet = inputParser;
                addParameter(app.pSet,'save_file', save_file);
                addParameter(app.pSet,'config_file', config_file);
                addParameter(app.pSet,'config_type', config_type);
                app.commandLineData = varargin;
                return;
            end
            
            app.Timer = timer('StartDelay', 0, 'Period', 0.001, ...
                'ExecutionMode', 'fixedRate');
            
            app.Name = exp_name;
            app.cacheName = sprintf('cache_%s.mat', exp_name);
            % Create and configure components
            % createComponents(app)
            
            % Register the app with App Designer
            % registerApp(app, app.UIFigure)
            
            % Relative position of the default fields in the GUI.
            app.gridStack(1).pos1 = [79 377-34 100 22];
            app.gridStack(1).pos2 = [79 377-50 100 22];
            app.gridStack(2).pos1 = [272 377-34 100 22];
            app.gridStack(2).pos2 = [272 377-50 100 22];
            app.gridStack(3).pos1 = [477 376+16 100 22];
            app.gridStack(3).pos2 = [477 376 100 22];
            
            app.colInd = 3;
            app.paramStep = 0;
            
            app.ParamTypes = {'char', 'char', 'char', 'numeric', 'numeric'};
            app.ParamNames = [{'save_file', 'save file'}; ...
                {'config_file', 'config file'};  ...
                {'config_type', 'config type'}; ...
                {'SmallScreen', 'Small Screen'}; ...
                {'MouseFlag', 'Mouse Flag'}];
            
            app.defaultCache.save_file = string(save_file);
            app.defaultCache.config_file  = string(config_file);
            app.defaultCache.config_type  = string(config_type);
            app.defaultCache.SmallScreen  = 0;
            app.defaultCache.MouseFlag  = 0;
            
            app.TypeMapper.string = @string;
            app.TypeMapper.numeric = @app.str2double2;
            app.TypeMapper.array = @app.str2array;
            app.TypeMapper.char = @char;
            
            createComponents(app);
            registerApp(app, app.UIFigure);
            
            app.UIFigure.Name = app.Name;
            app.EditFieldStruct.save_file = app.savefileEditField;
            app.EditFieldStruct.config_file = app.configfileEditField;
            app.EditFieldStruct.config_type = app.configtypeEditField;
            app.EditFieldStruct.MouseFlag = app.MouseFlag;
            app.EditFieldStruct.SmallScreen = app.SmallScreen;
            app.load_cache(1);
            
            if nargout == 0
                clear app
            end
        end
        
        function display(app)
            % Override display so it does not automatically call
            % Deliberately does nothing
        end
        
        function out = process_params(app)
            %PROCESS_PARAMS is to be run once parameters are submited (run
            % button of the GUI is pressed). This function unpacks the GUI
            % parameters into a struct with their corresponding param_names
            % and values storing this in the variable app.pSet.
            %
            % When running in command line mode it knows to read the
            % parameters from the command line.
            
            % If arguments were passed to the GUI constructor it means we
            % are running in commandline mode:
            if ~isempty(app.commandLineData)
                parse(app.pSet, app.commandLineData{:});
                app.pSet = app.pSet.Results;
                out = 0;
                return;
            end
            uiwait(app.UIFigure);
            out=0;
            if ~isprop(app, 'UIFigure')
                out = 1;
            end
        end
        
        function addParam(app, paramType, paramName, defaultValue)
            % The add param function adds parameters specifiying their
            % type, name and default value. Usage:
            %
            %   WL.GUI = wl_gui('dynamics', 'test', 'cfg_dynamic', 'exp3');
            %   WL.GUI.addParam('array', 'trials_to_run', []);
            %
            %  paramType    - string specifying the type of the parameter can
            %                 be : numeric, array, char, string
            %  paramName    - string specifying the name the GUI param will
            %                 have
            %  defaultValue - default value of parameter when one specified
            
            if ~isempty(app.commandLineData)
                addParameter(app.pSet, paramName, defaultValue);
                return;
            end
            
            % addParameter(app.pSet, paramName, defaultValue);
            % struct with 3 positions and just shift downwards and right
            app.gridStack(app.colInd).pos1(2) = app.gridStack(app.colInd).pos1(2) - 50;
            app.gridStack(app.colInd).pos2(2) = app.gridStack(app.colInd).pos2(2) - 50;
            
            % Create <paramName>eEditFieldLabel
            EditFieldLabel = uilabel(app.FixedParametersTab);
            EditFieldLabel.HorizontalAlignment = 'left';
            EditFieldLabel.Position = app.gridStack(app.colInd).pos1;
            
            
            % Create <paramName>EditField
            EditField = uieditfield(app.FixedParametersTab, 'text');
            EditField.Position = app.gridStack(app.colInd).pos2;
            EditField.Value = app.parse_value(defaultValue);
            
            if ~iscell(paramName)
                paramName = {paramName, paramName};
            end
            EditFieldLabel.Text = char(paramName(2));
            
            app.EditFieldStruct.(char(paramName(1))) = EditField;
            
            app.ParamNames = [app.ParamNames; paramName];
            app.ParamTypes = [app.ParamTypes paramType];
            
            % Attempt to load latest parameters
            if exist(app.cacheName, 'file') && app.paramStep ~= 0
                load(app.cacheName);
                pos = app.cacheLength - app.paramStep + 1;
                if isfield(cache(pos), char(paramName(1)))
                    app.EditFieldStruct.(char(paramName(1))).Value = app.parse_value(cache(pos).(char(paramName(1))) );
                end
                % else load default parameters
            else
                app.EditFieldStruct.(char(paramName(1))).Value = app.parse_value(defaultValue);
            end
            
            app.colInd = max(mod(app.colInd + 1, 4),1);
            app.defaultCache.(char(paramName(1))) = defaultValue;
        end
        
        function load_cache(app, init)
            %LOAD_CACHE opens the mat file with previous prameter settings
            % and loads the parameters specified by (the backwards, fowards
            % and default buttons) app.paramStep into the GUI.
            %
            % The file being searched will have the name
            % 'cache_<experiment_name>.mat
            
            % if file exist and a previous version has been asked
            % store those parameters in variable cache
            if exist(app.cacheName, 'file') && app.paramStep ~= 0
                load(app.cacheName);
                app.cacheLength = length(cache);
                pos = app.cacheLength - app.paramStep + 1;
                cache = cache{pos};
                % else if file exists and no navigation has been specified
                % load the latest version into variable cache
            elseif exist(app.cacheName, 'file')
                load(app.cacheName);
                posistion = cache{end}.pos;
                app.cacheLength = length(cache);
                cache = app.defaultCache;
                if init
                    app.UIFigure.Position = posistion;
                end
                % if cile does not exist load default parameters into variable
                % cache
            else
                cache = app.defaultCache;
            end
            
            % Once loaded cache, loop over fields in gui and populate
            % parameters:
            keys = fields(app.EditFieldStruct);
            for i = 1:numel(keys)
                if isfield(cache, keys{i})
                    if isa(app.EditFieldStruct.(keys{i}), 'matlab.ui.control.EditField')
                        % If invalid NaN stored in previous parameters warn
                        % and skip
                        if all(isa(cache.(keys{i}), 'double')) && all(isnan(cache.(keys{i})))
                            warning('%s isnan thus ignored\n', keys{i});
                            continue;
                        end
                        app.EditFieldStruct.(keys{i}).Value = char(strjoin(string(cache.(keys{i}))));
                    else
                        app.EditFieldStruct.(keys{i}).Value = cache.(keys{i});
                    end
                end
            end
            
            % Set datetime when parameters were saved
            if isfield(cache, 'dt')
                app.dateLabel.Text = sprintf('%s, %d back' ,char(cache.dt), app.paramStep);
            end
        end
        
    end
end