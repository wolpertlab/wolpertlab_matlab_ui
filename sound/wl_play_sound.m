function wl_play_sound(obj, P,vol)
%WL_PLAY_SOUND plays a sound unsing psychtoolbox.
%   WL_PLAY_SOUND(OBJ,P,VOL) plays sound  corresponding to PsychPortAudio  
%   handle at volume VOL if specified otherwise looks for the volume in the
%   experiment objects global workspace.

    if nargin==2
        vol=obj.GW.vol;
    end

    PsychPortAudio('Stop',obj.GW.AudioHandle); % Stop current playback.
    PsychPortAudio('Volume',obj.GW.AudioHandle,vol);
    PsychPortAudio('FillBuffer',obj.GW.AudioHandle,P);

    PsychPortAudio('Start',obj.GW.AudioHandle);
end
