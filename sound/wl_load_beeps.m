function P=wl_load_beeps(freqs,dur)
%WL_LOAD_BEEPS 
%   P=WL_LOAD_BEEPS(FREQS,DUR) takes in a vector of  frequencies (FREQS) 
%   and a vector of time durations (DUR) synthesiing a beep and returning
%   it as a psychtoolbox audio handle (P)

    freq=48000;
    myBeep=[];

    for k=1:length(freqs)
        if  freqs(k)>0
            beep = sin(2*pi*freqs(k)*(0:dur(k)*freq)/freq);
            myBeep = [myBeep beep];
        else
            myBeep = [myBeep 0*(0:dur(k)*freq)];
        end
    end

    P = PsychPortAudio('CreateBuffer', [],  ones(2,1)*myBeep);
end


