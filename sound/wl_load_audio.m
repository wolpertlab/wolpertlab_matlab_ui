function P=wl_load_audio(obj, fname)
%WL_LOAD_AUDIO loads audio wave data into the experiment class object to be
%used for playing later.
%   P=WL_LOAD_AUDIO(OBJ, FNAME) takes in a pointer to the experiment object
%   (TODO: remove or use OBJ pointer) and a path FNAME for an audio file
%   returning a psychtoolbox handle to the loaded audio file.

    [y, freq] =audioread(fname);
    wavedata = y';
    nrchannels = size(wavedata, 1); % Number of rows == number

    if nrchannels ==1
        wavedata = resample(wavedata, 48000, freq);
        swavedata = [wavedata ; wavedata];
    else
        swavedata(1,:) = resample(wavedata(1,:), 48000, freq);
        swavedata(2,:) = resample(wavedata(2,:), 48000, freq);
    end

    P = PsychPortAudio('CreateBuffer', [], swavedata);
end
