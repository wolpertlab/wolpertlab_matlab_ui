function out=wl_cm2pix(obj, in)
%WL_CM2PIX converts centimeters to pixels
%   OUT = WL_CM2PIX(OBJ,IN) takes in a pointer OBJ  to the experiment
%   object in order to obtain the screen dimensions and an array of
%   distances in centimeters which get mapped to pixel space and stored in
%   OUT
    for k=1:length(in)
        if  rem(k,2)==1
            out(k)=obj.Screen.windowRect(3)*(in(k)-min(obj.Screen.xcm))/range(obj.Screen.xcm);
        else
            out(k)=obj.Screen.windowRect(4)*((in(k)-min(obj.Screen.ycm))/range(obj.Screen.ycm));
        end
    end
end
