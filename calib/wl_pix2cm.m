function out=wl_pix2cm(obj, in)
%WL_PIX2CM converts an array of distances from pixel space to centimeter
%space (returns relative distances)
%   OUT = WL_PIX2CM(OBJ,IN) takes in a pointer to the experiment object OBJ
%   and an array of distances in pixels IN converting them to an array of
%   distances in centimeters OUT.
    for k=1:length(in)
        if  rem(k,2)==1
            out(k,1)= range(obj.Screen.xcm)*(in(k)/obj.Screen.windowRect(3))+min(obj.Screen.xcm);
        else
            out(k,1)= range(obj.Screen.ycm)*(in(k)/obj.Screen.windowRect(4))+min(obj.Screen.ycm);
        end
    end
end
