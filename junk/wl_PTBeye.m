classdef wl_PTBeye<handle
    
    properties
        FrameVariableList = { 'TimeStamp','State','StateTime','Position','Velocity','Speed' };
        FrameVariableSize = [ 1,1,1,3,3,1];
        FrameVariableCount=0;
        FrameData
        
        Data
        DataCount=0
        TimeStamp
        State
        StartTime
        StateTime
        LastTime=0
        Collecting=false
        CircBuffer
        RobotCount
        Simulate=true;
        Position=[0 0 0]
        Velocity=[0 0 0]
        Speed=0
        bcount=0
        k1
        k2
        Active
        bsize=50
        N=3000
        ActualType='eye';
        edfFile;
        eye_used
        el;
    end
    
    methods
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function this=wl_PTBeye()
            this.FrameVariableCount = length(this.FrameVariableList);
            this.Data=zeros(this.N,sum(this.FrameVariableSize));
            this.CircBuffer=zeros(this.bsize,4); %for pos and time
            this.StartTime=GetSecs;
            this.k1=cumsum([0 this.FrameVariableSize(1:end-1)])+1;
            this.k2=cumsum(this.FrameVariableSize);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function ok = GetLatest(this, obj)

            ok=1;
            
            if Eyelink( 'NewFloatSampleAvailable') == 0
                return;
            end
            
            evt = Eyelink( 'NewestFloatSample');
            pos(1,1) = evt.gx(this.eye_used+1);
            pos(2,1) = evt.gy(this.eye_used+1);
            pos(3,1)=0;
           % pos=wl_eye2cm(pos);

            
            this.TimeStamp=GetSecs-this.StartTime;
            this.State=obj.State.Current;
            this.StateTime=obj.State.Timer.GetTime;
            this.Position=pos;
            this.Active=ones(1,this.RobotCount);
            
            this.LastTime=this.TimeStamp;
            this.bcount=this.bcount+1;
            this.CircBuffer(mod(this.bcount-1, this.bsize)+1,:) = [this.Position' this.TimeStamp];
            
            tmp=this.CircBuffer(mod(this.bcount-1, this.bsize)+1,:)- this.CircBuffer(mod(this.bcount-this.bsize, this.bsize)+1,:);
            this.Velocity=tmp(1:3)/tmp(4);
            
            this.Speed=norm(this.Velocity);
            
            if  this.Collecting
                this.DataCount=this.DataCount+1;
                for k=1:this.FrameVariableCount
                    this.Data(this.DataCount,this.k1(k):this.k2(k))=this.(this.FrameVariableList{k});
                end
            end
            
            if this.RobotCount==2
                this.Velocity=[this.Velocity  this.Velocity];
                this.Speed=[this.Speed  this.Speed];
                this.Position=[this.Position  [-1 0 0;  0 -1 0 ; 0 0 1]*(this.Position)];
            end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function ok = DataStart(this)
            this.Data=zeros(this.N,sum(this.FrameVariableSize));
            this.StartTime=GetSecs;
            this.DataCount=0;
            this.Collecting=1;
            this.LastTime=0;
            
            ok = 1;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function ok = DataStop(this)
            this.Collecting=0;
            ok = 1;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function [ ok,FrameData ] = DataGet(this)
            FrameData.Frames = this.DataCount;
            
            for i=1:this.FrameVariableCount
                FrameData.(this.FrameVariableList{i})= this.Data(:,this.k1(i):this.k2(i));
            end
            
            ok=1;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function ok = Start(this,obj,Robot1,Robot2)
            ok=1;
            this.RobotCount=nargin-1;
            this.Velocity=zeros(3, this.RobotCount);
            this.Speed=zeros(1, this.RobotCount);
            
            this.el=EyelinkInitDefaults(obj.GW.Screen.window);
            if ~EyelinkInit(0, 1)
                ok = false;
                fprintf('Eyelink Init aborted.\n');
                return;
            end
            Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA');
            Eyelink('StartRecording');
            Eyelink('Message', 'SYNCTIME');
            error=Eyelink('CheckRecording');
            
            if(error~=0)
                ok = false;
                fprintf('Eyelink Init aborted.\n');
                return;
            end
            this.eye_used = Eyelink('EyeAvailable'); % get eye that's tracked
            if this.eye_used == this.el.BINOCULAR; % if both eyes are tracked
                this.eye_used = this.el.LEFT_EYE; % use left eye
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ok = Stop(this)
            Eyelink('StopRecording');
            Eyelink('Shutdown');
            ok = 1;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function ok = StateSet(varargin)
            ok =1;
        end
        
        function ok = RampUp(this)
            ok = 1;
        end
        
        function ok = RampDown(this)
            ok =1;
        end
        
        function ok = FieldNull(this)
            ok = 1;
        end
        
        function ok = FieldViscous(varargin)
            ok = 1;
        end
        
        function ok = FieldSpring(varargin)
            ok = 1;
        end
        
        function ok = FieldChannel(varargin)
            ok = 1;
        end
        
        function ok = FieldPMove(varargin)
            ok =1;
        end
        
        function ok =FieldBimanualSpring(varargin)
            ok =1;
        end
    end
end