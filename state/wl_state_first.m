function out = wl_state_first(obj)
%WL_STATE_FIRST checks if this is the first state and updates the first
%flag
%   out = WL_STATE_FIRST(OBJ) returns true if this is the first time that
%   this function is called and this is the first state in the experiment
    out=obj.State.FirstFlag;
    obj.State.FirstFlag = false;
end