function wl_state_init(obj, varargin)
%WL_STATE_INIT initialises the states of the finite state machine used by
%experiments
%   WL_STATE_INIT(OBJ, varargin) OBJ is a pointer to the experiment object
%   and varargin is an arbitrary number of arguments each of which is a
%   string representing a state
%
%   % Example:
%   % ... OBJ = experiment(); 
%   wl_state_init(OBJ,'INITIALIZE','SETUP','HOME','START','GO','MOVEWAIT',...
%                 'MOVING','FINISH','NEXT','INTERTRIAL','EXIT','ERROR')

    for k=1:length(varargin)

        obj.State.(upper(varargin{k})) = k;
        obj.State.Name{k} = varargin{k};
    end

    obj.State.Timer = wl_timer;
    obj.State.FirstFlag = false;
    obj.State.Last = 1;
    obj.State.Current = 1;
    obj.State.Max = k;
    obj.State.To = 0;
    obj.State.From = 0;
end