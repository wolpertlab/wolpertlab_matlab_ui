function ok = wl_automatic(obj, delay)
%WL_AUTOMATIC returns true somehow and for some reason and is not in use
    ok = obj.GW.automatic && (obj.State.Timer.GetTime > delay);
end
