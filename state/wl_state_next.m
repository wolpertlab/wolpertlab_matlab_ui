function wl_state_next(obj, state)
%WL_STATE_NEXT transitions the current state of the FSM to the specified
%state.
%   WL_STATE_NEXT(OBJ,STATE) takes in a pointer to the experiment object.
%   OBJ and the string STATE representing the specified state. The function changes the current
%   state in OBJ to specified STATE.
%
    %   [WARNING]: don't change state if you are in error mode - as the error will
%   determine where you go next.


    if obj.State.Current == state || ~isempty(obj.GW.error_msg)
        return;
    end

    if isfield(obj.cfg, 'Debug')  && obj.cfg.Debug
        fprintf(1,'obj.State: %s[%d] > %s[%d] %.3fs\n',...
            obj.State.Name{obj.State.Current}, obj.State.Current, ...
            obj.State.Name{state}, state, obj.State.Timer.GetTime());
    end

    obj.State.Timer.Reset;

    obj.State.FirstFlag = true;
    obj.State.Last = obj.State.Current;
    obj.State.From =   obj.State.Last;

    obj.State.Current = state;
    obj.State.To = state;

    if( isfield(obj.GW,'Hardware') )
        ok = obj.Hardware.StateSet(obj.State.Current,obj.GW.TrialTimer.GetTime);
    end

end
