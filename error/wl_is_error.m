function out = wl_is_error(obj)
%WL_IS_ERROR returns a boolean specifying if there is an error message or
%more in the error message buffer of the experiment.
%   OUT = WL_IS_ERROR(OBJ) takes in a pointer to the expriment object OBJ
%   returns OUT=1 if there is at least one error  message waiting to be
%   displayed.
    out = 0;

    if isfield(obj.GW,'error_msg')
        out = ~isempty(obj.GW.error_msg) ;
    end
end

