function wl_error_state(obj, msg, state)
%WL_ERROR_STATE plays sounds corresponding to error state along with
%visual display before moving onto the specified state.
%   WL_ERROR_STATE(OBJ, MSG, STATE) takes in a pointer to the experiment
%   object OBJ displays the error before moving onto state STATE.
    if ~isempty(msg)
        wl_play_sound(obj, obj.cfg.lowbeep,obj.cfg.vol);
    end

    obj.State.ErrorResume = state;
    wl_state_next(obj, obj.State.ERROR);
    obj.GW.error_msg = msg;
end
