function wl_error_resume(obj)
%WL_ERROR_RESUME clears the error message and proceeds to the error resume
%state.
% WL_ERROR_RESUME(OBJ) Takes in a pointer OBJ to the experiment object.
% Calling WL.error_resume() or WL_ERROR_RESUME(WL) (in an instance of ExperimentBase)
% will execute this function

    obj.GW.error_msg = '';
    wl_state_next(obj, obj.State.ErrorResume);
end
