function ok = wl_initialise(obj)
%WL_INITIALISE sets up the I\O for the the experiments
%   WL_INITIALISE(OBJ) where OBJ is a pointer to the main experiment class.
%   Note the passing of OBJ is handled inplictly by the ExperimentBase
%   class thus you do not need to wory about passing this parameter.
%
%   Calling WL.initialise inside your experiment class
%   equates to wl_initialise(WL) where WL is the
%   first argument of every method in the class also known as an internal
%   pointer to the object. this and obj are more commonly used.
%
%   Usage (within instance of ExperimenBase):
%       WL.initialise

    ok = ~obj.GUI.process_params();
    if ~ok
        return;
    end
    obj.GW = obj.GUI.pSet;
    delete(obj.GUI.UIFigure);

    obj.GW.TrialRunning = false;
    obj.GW.ExitFlag = false;
    
    obj.Timer.Paradigm.ExperimentTimer = wl_timer;  %total experiment time
    obj.Timer.Paradigm.TrialTimer = wl_timer; % from the start of a trial
    obj.Timer.Paradigm.InterTrialDelayTimer = wl_timer; %from the end of the trial
    obj.State.Timer = wl_timer;
    
    
    obj.Timer.System.idle_loop = wl_timer(); %timer of the idle function
    obj.Timer.System.idle_func_2_idle_func = wl_timer(); %timer of the idle function
    obj.Timer.System.idle_func = wl_timer(); %timer of the idle function
    obj.Timer.System.flip_check = wl_timer();
    
    obj.Timer.System.flip_2_flip=wl_timer;
    obj.Timer.System.flip_2_display_func=wl_timer;
    obj.Timer.System.flip_request_2_flip=wl_timer;
    obj.Timer.System.TrialSave = wl_timer;
    obj.Timer.System.display_func = wl_timer;
    obj.StimulusTime = wl_flip_timer;
    %  obj.GW.StimulusTime=wl_flip_timer;
    obj.Timer.System.flip_request=wl_timer;
    
    
    obj.Timer.MovementDurationTimer=wl_timer;
    obj.Timer.MovementReactionTimer=wl_timer;
    obj.Screen.StimulusTime=wl_timer;
    %obj.Screen.StimulusStarted=1;
    
    obj.GW.error_msg='';
    obj.GW.MissTrial=0;
    
    InitializePsychSound(1);
    dev=PsychPortAudio('GetDevices');
    
    obj.GW.AudioDevice=[];
    
    for k=1:length(dev)
        if strfind(dev(k).DeviceName,'AISO')==1
            obj.GW.AudioDevice=dev(k).DeviceIndex;
        end
    end
    
    obj.GW.AudioHandle = PsychPortAudio('Open', obj.GW.AudioDevice, 1, 1, 48000,2);
    oldlevel = PsychPortAudio('Verbosity' ,1);
    oldLevel = Screen('Preference', 'Verbosity', 1);     %usuallly 3
    
    
    %this is to rename the WL functions
    d=dbstack;
    tmp=(d(2).name);
    u= strfind(tmp,'.');
    
    
    feval(obj.GW.config_file, obj, obj.GW.config_type);  %runs configuration
    
    obj.GW.table_file=['_table_' strrep(num2str(GetSecs),'.','_') '.mat'];
    obj.GW.save_file=[obj.GW.save_file '.mat'];
    obj.GW.log_file=['_log_' strrep(num2str(GetSecs),'.','_') '.txt'];
    
    if ispc
        obj.GW.save_file=['T:\' obj.GW.save_file];
        obj.GW.table_file=['T:\' obj.GW.table_file];
        obj.GW.log_file=['T:\' obj.GW.log_file];
    end
    
    if exist(obj.GW.save_file,'file')
        delete(obj.GW.save_file);
    end
    
    
    
    if isfield(obj.cfg, 'Debug') && obj.cfg.Debug
        diary(obj.GW.log_file);
        % oldLevel = Screen('Preference', 'Verbosity', 10);     %usuallly 3
    end
    
    if obj.GW.reload_table
        load TrialData
        obj.TrialData=TrialData;
    else
        TrialData = obj.TrialData;
        save('TrialData','TrialData');
    end
    
    
    %choose trials to run
    if length(obj.GW.trials_to_run)==1
        obj.TrialData=obj.TrialData(obj.GW.trials_to_run:end,:);
    elseif length(obj.GW.trials_to_run )==2
        obj.TrialData=obj.TrialData(obj.GW.trials_to_run(1):obj.GW.trials_to_run(2),:);
    end
    
    wl_start_screen(obj,obj.cfg.ScreenSize);  %possibly put this in initialise
end
