
classdef wl_flip_timer<handle
  % Summary of wl_timer class
  % General timer
  %
  % wl_timer Properties:
  %     N - size of array to store times
  %     StartTime - time of creation or reset
  %
  %   Loop Properties i.e. where we time between the same point in code
  %     Loops - times stored by loop timer
  %     loop_count - counter for loop times
  %
  %   Interval Properties i.e. where we time between different points in code
  %     Tics  - tic array - time from here
  %     Tocs  - toc array- time to here
  %     toc_count  - counter for tic/toc time
  %
  % wl_timer Methods:
  %   Creation
  %     wl_timer(N) - creates a timer or array size N
  %     wl_timer    - creates a timer or array size 1
  %     Reset    - reset timer
  %
  %     GetTime  - time since timer created or reset
  %
  %   Loop Functions
  %     loop - saves times
  %     [mean_dt,sd_dt,dt,t] = GetLoopInterval  - Loop interval
  %     [mean_freq,sd_freq,freq,t] = GetLoopFreq - Loop freq
  %
  %   Interval Functions
  %     Tic - start a tic and save time
  %     Toc - start a toc and save time  - returns iti
  %     [mean_dt,sd_dt,dt,t] = GetTocInterval  - Toc intervals
  %     [mean_freq,sd_freq,freq,t] = GetTocFreq - Toc freq
  
  properties
    StartTime;
    Loops;
    N=10000;
    loop_count=0;
    trigger=0;
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  
  methods (Static, Access = private)
    function out = set_instances(this)
    % Private function to manage the container for instances
        persistent val
        if isempty(val)
            val = [];
        end
        if nargin
            val = [val this];
        end
        out = val;
    end
  end

  methods (Static)
    function value = get_instances()
    % Public access to read only from container 
        value = wl_flip_timer.set_instances();
    end
  end

  methods
    
    function this=wl_flip_timer()  %creates a timer
      this.Loops=zeros(this.N,1);
      this.trigger=0;
      this.StartTime=GetSecs;
      wl_flip_timer.set_instances(this);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    
    function r = GetTime(this)
      r = GetSecs-this.StartTime;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    function r = GetDur(this)
      if this.loop_count > 0
        if this.loop_count<=this.N
          w=1:this.loop_count;
          n = this.loop_count-1;
        else
          w=[circ_buffer(this.loop_count+1,this.N):this.N  1:circ_buffer(this.loop_count,this.N)];
          n = this.N-1;
        end
        
        t=this.Loops(w);
        
        r=t(end)-t(1);
      else
        r=0;
      end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function r = GetFrames(this)
      r =this.loop_count-1;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function Reset(this)
      this.loop_count=0;
      this.trigger=0;
      this.StartTime= GetSecs;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function Loop(this)
      if this.trigger
        this.loop_count=this.loop_count+1;
        
        w=circ_buffer(this.loop_count,this.N);
        this.Loops(w)= this.GetTime;
      end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function Trigger(this)
      this.trigger=1;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function S = GetSummary(this)
      S=[];
      
      
      if this.loop_count > 0
        if this.loop_count<=this.N
          w=1:this.loop_count;
          S.n = this.loop_count-1;
        else
          w=[circ_buffer(this.loop_count+1,this.N):this.N  1:circ_buffer(this.loop_count,this.N)];
          S.n = this.N-1;
        end
        
        S.t=this.Loops(w);
        S.dt=diff(S.t);
        
        S.t=S.t(end)-S.t(1);
        
        S.dtmean =mean(S.dt);
        S.dtmax=  max(S.dt');
        S.dtsd = std(S.dt);
        
        S.freq=1./S.dt;
        S.fmean = mean(S.freq);
        S.fsd = std(S.freq);
        S.ft= S.t(1:end-1)  ;
      end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function Results(this,name)
      S = this.GetSummary;
      
      if( this.loop_count > 0 )
        fprintf(1,'%s \tLoop Frequency=%0.3f%c%.3f Hz (n=%d)\n',name,S.fmean,177,S.fsd,S.n);
      end
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function k=circ_buffer(count,N)
  k= mod(count-1,N)+1;
end