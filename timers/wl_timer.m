classdef wl_timer<handle
  % Summary of wl_timer class
  % General timer
  %
  % wl_timer Properties:
  %     N - size of array to store times
  %     StartTime - time of creation or reset
  %
  %   Loop Properties i.e. where we time between the same point in code
  %     Loops - times stored by loop timer
  %     loop_count - counter for loop times
  %
  %   Interval Properties i.e. where we time between different points in code
  %     Tics  - tic array - time from here
  %     Tocs  - toc array- time to here
  %     toc_count  - counter for tic/toc time
  %
  % wl_timer Methods:
  %   Creation
  %     wl_timer(N) - creates a timer of array size N
  %     wl_timer    - creates a timer of array size 1
  %     Reset    - reset timer
  %
  %     GetTime  - time since timer created or reset
  %
  %   Loop Functions
  %     loop - saves times
  %
  %   Interval Functions
  %     Tic - start a tic and save time
  %     Toc - start a toc and save time  - returns iti

  
  properties
    StartTime;
    Loops;
    Tics;
    Tocs;
    tic_time=nan;
    N=10000;
    loop_count=0;
    toc_count=0;
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function this=wl_timer(val)  %creates a timer
      if nargin >= 1
        this.N = val;
      end
      
      this.Loops = zeros(this.N,1);
      this.Tics = zeros(this.N,1);
      this.Tocs = zeros(this.N,1);
      
      this.StartTime = GetSecs();
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function r = GetTime(this)
      % Time since timer created or reset
      r = GetSecs() - this.StartTime;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function Reset(this, count)
      if nargin == 2
        this.loop_count = count;
        this.toc_count = count;
      else
        this.loop_count = 0;
        this.toc_count = 0; 
      end
      this.tic_time = nan;
      this.StartTime = GetSecs();
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function Loop(this)
      this.loop_count = this.loop_count + 1;
      w = circ_buffer(this.loop_count, this.N);
      this.Loops(w) = this.GetTime();
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function Tic(this)
      this.tic_time= this.GetTime();
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function iti=Toc(this)
      if  ~isnan(this.tic_time) %save tic time only after a toc
        this.toc_count = this.toc_count + 1;
        w = circ_buffer(this.toc_count, this.N);
        
        this.Tics(w) = this.tic_time;
        this.Tocs(w) = this.GetTime();
        
        this.tic_time = nan;
        
        iti = this.Tocs(w) - this.Tics(w);
      elseif this.toc_count > 0
        w = circ_buffer(this.toc_count, this.N);
        
        this.Tocs(w) = this.GetTime;
        iti = this.Tocs(w) - this.Tics(w);
      else
        iti = this.GetTime();
      end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function S=GetSummary(this)
      S = [];
      if this.toc_count > 0
        if this.toc_count <= this.N
          w = 1:this.toc_count;
          S.toc.n = this.toc_count;
        else
          w = [circ_buffer(this.loop_count+1,this.N):this.N  1:circ_buffer(this.loop_count,this.N)];
          S.toc.n = this.N;
        end
        S.toc.t = this.Tics(w);
        
        S.toc.dt = this.Tocs(w) - this.Tics(w);
        S.toc.dtmean = mean(S.toc.dt);
        S.toc.dtmax =  max(S.toc.dt');
        S.toc.dtsd = std(S.toc.dt);
        
        S.toc.freq = 1./S.toc.dt;
        S.toc.fmean = mean(S.toc.freq);
        S.toc.fsd = std(S.toc.freq);
        S.toc.ft= S.toc.t(1:end-1)  ;
        
      end
      
      
      if this.loop_count > 0
        if this.loop_count <= this.N
          w = 1:this.loop_count;
          S.loop.n = this.loop_count;
        else
          w=[circ_buffer(this.loop_count+1,this.N):this.N  1:circ_buffer(this.loop_count,this.N)];
          S.loop.n = this.N;
        end
        
        S.loop.t = this.Loops(w);
        S.loop.sumt = S.loop.t(end) - S.loop.t(1);
        
        S.loop.dt = diff(S.loop.t);
        S.loop.t = S.loop.t(1:end-1);
        S.loop.dtmean = mean(S.loop.dt);
        S.loop.dtmax = max(S.loop.dt');
        S.loop.dtsd = std(S.loop.dt);
        
        S.loop.freq = 1./S.loop.dt;
        S.loop.fmean = mean(S.loop.freq);
        S.loop.fsd = std(S.loop.freq);
        S.loop.ft = S.loop.t(1:end-1);
      end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function Results(this,name)
      S = this.GetSummary;
      if( this.toc_count > 0 )
        fprintf(1,'%s \tToc Interval=%0.3f%c%.3f ms (n=%d)\n',name,S.toc.dtmean*1000,177,S.toc.dtsd*1000,S.toc.n);
      end
      
      if( this.loop_count > 0 )
        fprintf(1,'%s \tLoop Interval=%0.3f%c%.3f ms Frequency=%0.3f%c%.3f Hz (n=%d)\n',name,S.loop.dtmean*1000,177,S.loop.dtsd*1000,S.loop.fmean,177,S.loop.fsd,S.loop.n);
      end
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function k=circ_buffer(count,N)
  k= mod(count-1,N)+1;
end