function out=wl_everyhz(freq)
%WL_EVERYHZ acts as a timer returning true whenever the period has been
%carried out
%   out=WL_EVERYHZ(FREQ) takes in a scalar value for frequency and returns
%   true if 1/FREQ seconds have passed since it was last called, ideal
%   usage should be within a while loop to time schedule an event
    persistent line_number last_time names
    persistent nt

    if isempty(nt)
        nt=0;
    end

    S = dbstack(1);
    S=S(1);
    exist=0;
    out=false;

    for k=1:nt
        if  strcmp(names{k},S.name) && line_number(k)==S.line
            exist=1;
            break
        end
    end

    if exist
        if GetSecs-last_time(k)>1/freq
            last_time(k)=GetSecs;
            out=true;
        end
    else
        nt=nt+1;
        names{nt}=S.name;
        line_number(nt)=S.line;
        last_time(nt)=GetSecs;
        out=true;
    end

end


