function S=wl_test_timings(obj, test_duration)
%WL_TEST_TIMINGS for a given pointer to an Experiment object OBJ and a
% double TEST_DURATION WL_TEST_TIMINGS(OBJ, TEST_DURATION) draws a simple
% cursor on a black screeen for TEST_DURATION seconds with different
% flip_request_2_flip delays and collects/plots stats for the VBL latencies
% associated to this test

if nargin < 2
    test_duration = 2;
end

test_freq = 1 / test_duration;

% Set the mouse to the middle of the screen
if ismac
    SetMouse(obj.Screen.cent(1),obj.Screen.cent(2), obj.Screen.window);
end

HideCursor()

topPriorityLevel = MaxPriority(obj.Screen.window);
Priority(topPriorityLevel);

obj.GW.TrialCount = rows(obj.TrialData);
obj.Trial = 1;

[id names info] = GetKeyboardIndices();

if exist('kbd_default.mat','file')
    load kbd_default
    fprintf(1,'Loaded keyboard id =%i from file \n', key_id)
else
    key_id=id(1);    
    for k=1:length(names)
        if strfind(names{k},'Apple Keyboard') == 1
            key_id=id(k);
        end
    end
end

KbQueueCreate(key_id);
KbQueueStart(key_id);

obj.Screen.flipped = 0;

%delays=0.001:0.0005:0.010;
delays = linspace(0.000,0.003,30);

Exit = 0;
vbl = Screen('Flip',  obj.Screen.window);

count = 1;
display_delay = 0;

act_delay = zeros(1, length(delays));
act_delay_se = zeros(1, length(delays));
while count <= length(delays) && ~Exit
    t_adjust=delays(count);
    obj.Timer.System.idle_loop.Loop;
    
    if wl_everyhz(1000)
      obj.Timer.System.idle_func_2_idle_func.Loop;
      obj.Timer.System.idle_func.Tic;
      obj.idle_func()
      obj.Timer.System.idle_func.Toc;
      obj.State.From = 0;
      obj.State.To = 0;
      
      [pressed, firstPress] = KbQueueCheck(key_id);
      
      if  obj.GW.ExitFlag || (pressed && firstPress(KbName('ESCAPE')))
        Exit=1;
      end
    end
    
    if wl_everyhz(test_freq) && obj.Timer.System.flip_request_2_flip.toc_count > 2

      S3= obj.Timer.System.flip_request_2_flip.GetSummary;
      act_delay(count)=S3.toc.dtmean;
      act_delay_se(count)= S3.toc.dtsd;
      count=count+1;
      obj.Timer.System.flip_request_2_flip.Reset;
    end
    
    if obj.Screen.flipped==1 && (obj.Screen.ifi-(GetSecs-vbl) < t_adjust+display_delay)
             obj.Timer.System.flip_2_display_func.Toc;
      
      obj.Timer.System.display_func.Tic();
      obj.display_func()
      display_delay=obj.Timer.System.display_func.Toc();
      
      
      obj.Timer.System.flip_request.Tic;
      Screen('AsyncFlipBegin', obj.Screen.window);
      obj.Timer.System.flip_request.Toc;
      
      obj.Timer.System.flip_request_2_flip.Tic;
      obj.Screen.flipped=0;
      
      obj.StimulusTime.Trigger
    end
    
    
    [vbl StimulusOnsetTime FlipTimestamp] = Screen('AsyncFlipCheckEnd', obj.Screen.window);
    %test whether screen has flipped
    if  obj.Screen.flipped == 0 && vbl > 0
               
        obj.Screen.flipped = 1;
        
        obj.StimulusTime.Loop
        
        obj.Timer.System.flip_request_2_flip.Toc;
        obj.Timer.System.flip_2_display_func.Tic;
        
        obj.Timer.System.flip_2_flip.Loop;
    end
end
ShowCursor()

wl_timer_results(obj);
KbQueueStop(key_id)
sca;

if obj.cfg.Debug
  diary off
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

high_con_upper_bound = act_delay + 2 * act_delay_se;
[best, argmin] = min(high_con_upper_bound);

fprintf('Best time to delay flip request to : %f\n', 1000 * delays(argmin));
fprintf('Smallest latency until flip: %f +/- %f\n', 1000 * best, 1000 * act_delay_se(argmin));

S.best = best;
S.best_std = act_delay_se(argmin);
S.delays = delays;
S.act_delay = act_delay;
S.act_delay_se = act_delay_se;

clf
errorbar(delays * 1000, 1000 * act_delay, 1000 * act_delay_se, 'ko-');
hold on
plot(delays(argmin) * 1000, act_delay(argmin) * 1000, 'ro', 'MarkerFaceColor', 'r');
legend('\mu_y \pm \sigma_y', 'min(\mu_{y} + 2\sigma_{y})');
xlabel('time of flip requesty before flip-time (ms)')
ylabel('Delay until flip (ms)')
shg
