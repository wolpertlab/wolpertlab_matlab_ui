function wl_timer_results(obj)
%WL_TIMER_RESULTS displays the results of system level timers
%   WL_TIMER_RESULTS(OBJ) Takes in a pointer to the experiment object OBJ
%   and prints out the results for each of the system level timers in the
%   experiment
    tmp = fieldnames(obj.Timer.System);
    xn = length(tmp);
    for i=1:xn
        obj.Timer.System.(tmp{i}).Results(tmp{i});
    end
end

