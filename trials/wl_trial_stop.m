function wl_trial_stop(obj)
%WL_TRIAL_STOP Stops the current running trial and its hardware data
%collection feed
%   WL_TRIAL_STOP(OBJ) Takes in a pointer to the experiment object and
%   stops the current trial

    if( isfield(obj.GW,'Robot') )
        ok = obj.Robot.FieldNull();
    end

    ok = obj.Hardware.DataStop();

    obj.GW.TrialRunning = false;
end
