function wl_trial_start(obj)
%WL_TRIAL_START resets the trial timer and the hardware data collection in
%order to reflect the start of a trial
%   WL_TRIAL_START(OBJ) takes in a pointer to the experiment object and
%   initialises the current trial
    obj.GW.TrialTimer.Reset();
    ok = obj.Hardware.DataStart();
    obj.GW.TrialRunning = true;
end
