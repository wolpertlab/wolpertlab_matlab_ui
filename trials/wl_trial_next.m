function out=wl_trial_next(obj)
%WL_TRIAL_NEXT returns true if the trial count is offset from the current
%trial number by one (and updates it accordingly).
%   OUT=WL_TRIAL_NEXT(OBJ) takes in a pointer to the experiment object and 
%   progresses to the next trial if it is time to do so. Then returns true in
%   OUT if progressed successfully to the next trial.

    if( obj.TrialNumber == obj.GW.TrialCount )
        out = false;
    else
        obj.TrialNumber=obj.TrialNumber+1;
        out=true;
    end
end

