function wl_trial_abort(obj)
%WL_TRIAL_ABORT aborts current trial stopping data collection and updating
%relevant flags
%   WL_TRIAL_ABORT(OBJ) takes in a pointer to the experiment class object
%   and aborts the curent trial

    if( isfield(obj.GW,'Robot') )
        ok = obj.Robot.FieldNull();
    end

    ok = obj.Hardware.DataStop();

    obj.GW.TrialRunning = false;

    obj.MissTrial();  % Generate miss trial.
end