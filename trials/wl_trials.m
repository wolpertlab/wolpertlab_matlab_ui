classdef wl_trials <handle
    properties
        X
        RestFlag
        ReverseFlag
    end
    
    methods
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function this=wl_trials(val)  %creates a trial block;
            
            if nargin==1
                N=rows(val);
                this.X{1}.Table=val;
                this.X{1}.Table.permuted=false(N,1);           % has this trial been permuted
                this.X{1}.Table.original_order=nan(N,1);       % original order of permiuted trials
                this.X{1}.Location=sparse(N,N);        % forbidden locations, location(i,j)=1 means ith trial cannot be at jth position
                this.X{1}.Adjacency=sparse(N,N);                % forbidden adjacency, adjaceny(i,j)=  1 means j cannot directly follow i
                this.X{1}.Permute=false;          % flag as to whether to permute trials in block
              %  this.RestFlag=0;          %rest trial 0=none  1=befor blockl  2=after block
             %   this.ReverseFlag=0;       %whether to reverse the order of trials within a block
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function r = plus(obj1,obj2)
            
            r=wl_trials;  %create empty trial table
            
            % this does rest break if eithert obj is a string
            if ischar(obj1)
                n2=length(obj2.X);
                for k=1:n2
                    r.X{k}=obj2.X{k};
                end
                r.ReverseFlag=obj2.ReverseFlag;
                r.RestFlag=obj2.RestFlag;
                r.RestFlag(1)=1;  %put rest before block
                return
            elseif ischar(obj2)
                n1=length(obj1.X);
                for k=1:n1
                    r.X{k}=obj1.X{k};
                end
                r.RestFlag=obj1.RestFlag;
                r.ReverseFlag=obj1.ReverseFlag;
                r.RestFlag(end)=2; %put rest after block
                return
            end
            
            n1=length(obj1.X);
            n2=length(obj2.X);
            
            for k=1:n1
                r.X{k}=obj1.X{k};
                r.RestFlag(k)=obj1.RestFlag(k);
                r.ReverseFlag(k)=obj1.ReverseFlag(k);
            end
                        
            for k=1:n2
                r.X{n1+k}=obj2.X{k};
                r.RestFlag(n1+k)=obj2.RestFlag(k);
                r.ReverseFlag(n1+k)=obj2.ReverseFlag(k);
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function r = mtimes(obj1,obj2)
            if isfloat(obj1)  %number can be first or second argument
                n=obj1;
                obj=obj2;
            else
                n=obj2;
                obj=obj1;
            end
            
            r=obj;
            for k=1:n-1
                r=r+obj;
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function r = ctranspose(obj)  %reverse order
            r=wl_trials;  %create empty trial table
            
            n=length(obj.X);
            
            for k=1:n
                r.X{k}=obj.X{n-k+1};
                r.RestFlag(k)=0;
                r.ReverseFlag(k)=1-obj.ReverseFlag(n-k+1);
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function r = horzcat(varargin)
            % this does [A B] and randomizes order
            
            r=wl_trials;
             r.RestFlag=0;
    r.ReverseFlag=0;

            r.X{1}=wl_trials;
            n=nargin;
            
            for k=1:n
                r.X{1}.X{k}=varargin{k}.X{1}.X{1};
            end
            r.X{1}.X{1}.k=1;

           %XXX r.X{1}.k=1;
            r.X{1}.X{1}.order=[];
            for k=1:500
                r.X{1}.X{1}.order=[r.X{1}.X{1}.order ;randperm(n)'];
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function r = vertcat(varargin)
            % this does [A ;B] and has a fixed order
            r=[varargin{:}];
            n=nargin;
            r.X{1}.X{1}.order=repmat((1:n)',500,1);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function T = parse(obj)
            T=table; %empty table
            
            for k=1:length(obj.X)  %loop over all blocks
                
                kk=obj.X{k}.X{1}.k;   %counter for superblock - will be zero if not a superblock
                
                if  kk==0 %  not a  superblock
                    tt=obj.X{k}.X{1};
                else %  a  superblock
                    order=obj.X{k}.X{1}.order(kk); %element to choose
                    tt=obj.X{k}.X{order};
                    obj.X{k}.X{1}.k= obj.X{k}.X{1}.k+1; %increment counter
                end
                
                if tt.Permute %permute with constraints
                    j= wl_constrained_permute(rows(tt.Table),tt.Location,tt.Adjacency);
                    tt.Table.permuted(:)=true;
                elseif obj.ReverseFlag(k)
                    j=rows(tt.Table):-1:1;  %reverse order
                else
                    j=1:rows(tt.Table);  %not perturbed so original order
                end
                
                tt.Table.block_count(:)=k;  %block counter
                
                tt.Table=tt.Table(j,:);  % permute block
                tt.Table.original_order(:)=j; % save original order
                
                if  obj.RestFlag(k)==1
                    T.RestFlag(end)=1;  %rest after previous block
                elseif  obj.RestFlag(k)==2
                    tt.Table.RestFlag(end)=1;  %rest after this block
                end
                tt.Table.block_trial=(1:rows(tt.Table))';
                
                T=vertcat(T,tt.Table);  %append
            end
            
            T.RestFlag(end)=0; %make sure we do not have rest at end of experiment
            T.TrialNumber(:)=1:rows(T);
            
            %get block_index
            [~,~,T.block_index]=unique(T.blockname,'stable');
            
            %get element count and index
            [a,~,c]=unique([T.block_index T.block_count],'rows');
            for k=1:max(a(:,1))
                n=sum(a(:,1)==k);
                a(a(:,1)==k,2)=1:n;
            end
            T.block_elcount=a(c,2);
            
            %sort by column names
            [~,i]=sort(upper(T.Properties.VariableNames));
            T=T(:,i);
        end
    end
end

